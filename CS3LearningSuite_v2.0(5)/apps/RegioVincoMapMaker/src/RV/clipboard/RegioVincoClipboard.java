package RV.clipboard;

import djf.components.AppClipboardComponent;
import java.util.ArrayList;
import javafx.collections.ObservableList;
import RV.RegioVincoListMakerApp;
import RV.data.RegioVincoData;
import RV.data.RegioVincoItemPrototype;
//import RV.transactions.CutItems_Transaction;
//import RV.transactions.PasteItems_Transaction;

/**
 *
 * @author McKillaGorilla
 */
public class RegioVincoClipboard implements AppClipboardComponent {
    RegioVincoListMakerApp app;
    ArrayList<RegioVincoItemPrototype> clipboardCutItems;
    ArrayList<RegioVincoItemPrototype> clipboardCopiedItems;
    
    public RegioVincoClipboard(RegioVincoListMakerApp initApp) {
        app = initApp;
        clipboardCutItems = null;
        clipboardCopiedItems = null;
    }
    
    @Override
    public void cut() {
//        RegioVincoData data = (RegioVincoData)app.getDataComponent();
//        if (data.isItemSelected() || data.areItemsSelected()) {
//            clipboardCutItems = new ArrayList(data.getSelectedItems());
//            clipboardCopiedItems = null;
//            CutItems_Transaction transaction = new CutItems_Transaction((RegioVincoListMakerApp)app, clipboardCutItems);
//            app.processTransaction(transaction);
//        }
    }

    @Override
    public void copy() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        if (data.isItemSelected() || data.areItemsSelected()) {
            ArrayList<RegioVincoItemPrototype> tempItems = new ArrayList(data.getSelectedItems());
            copyToCopiedClipboard(tempItems);
        }
    }
    
    private void copyToCutClipboard(ArrayList<RegioVincoItemPrototype> itemsToCopy) {
        clipboardCutItems = copyItems(itemsToCopy);
        clipboardCopiedItems = null;        
        app.getFoolproofModule().updateAll();        
    }
    
    private void copyToCopiedClipboard(ArrayList<RegioVincoItemPrototype> itemsToCopy) {
        clipboardCutItems = null;
        clipboardCopiedItems = copyItems(itemsToCopy);
        app.getFoolproofModule().updateAll();        
    }
    
    private ArrayList<RegioVincoItemPrototype> copyItems(ArrayList<RegioVincoItemPrototype> itemsToCopy) {
        ArrayList<RegioVincoItemPrototype> tempCopy = new ArrayList();         
        for (RegioVincoItemPrototype itemToCopy : itemsToCopy) {
            RegioVincoItemPrototype copiedItem = (RegioVincoItemPrototype)itemToCopy.clone();
            tempCopy.add(copiedItem);
        }        
        return tempCopy;
    }

    @Override
    public void paste() {
//        RegioVincoData data = (RegioVincoData)app.getDataComponent();
//        if (data.isItemSelected()) {
//            int selectedIndex = data.getItemIndex(data.getSelectedItem());  
//            ArrayList<RegioVincoItemPrototype> pasteItems = clipboardCutItems;
//            if ((clipboardCutItems != null)
//                    && (!clipboardCutItems.isEmpty())) {
//                PasteItems_Transaction transaction = new PasteItems_Transaction((RegioVincoListMakerApp)app, clipboardCutItems, selectedIndex);
//                app.processTransaction(transaction);
//                
//                // NOW WE HAVE TO RE-COPY THE CUT ITEMS TO MAKE
//                // SURE IF WE PASTE THEM AGAIN THEY ARE BRAND NEW OBJECTS
//                copyToCutClipboard(clipboardCopiedItems);
//            }
//            else if ((clipboardCopiedItems != null)
//                    && (!clipboardCopiedItems.isEmpty())) {
//                PasteItems_Transaction transaction = new PasteItems_Transaction((RegioVincoListMakerApp)app, clipboardCopiedItems, selectedIndex);
//                app.processTransaction(transaction);
//            
//                // NOW WE HAVE TO RE-COPY THE COPIED ITEMS TO MAKE
//                // SURE IF WE PASTE THEM AGAIN THEY ARE BRAND NEW OBJECTS
//                copyToCopiedClipboard(clipboardCopiedItems);
//            }
//        }
    }    


    @Override
    public boolean hasSomethingToCut() {
        return ((RegioVincoData)app.getDataComponent()).isItemSelected()
                || ((RegioVincoData)app.getDataComponent()).areItemsSelected();
    }

    @Override
    public boolean hasSomethingToCopy() {
        return ((RegioVincoData)app.getDataComponent()).isItemSelected()
                || ((RegioVincoData)app.getDataComponent()).areItemsSelected();
    }

    @Override
    public boolean hasSomethingToPaste() {
        if ((clipboardCutItems != null) && (!clipboardCutItems.isEmpty()))
            return true;
        else if ((clipboardCopiedItems != null) && (!clipboardCopiedItems.isEmpty()))
            return true;
        else
            return false;
    }
}