/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RV.transactions;

import RV.RegioVincoListMakerApp;
import RV.data.RegioVincoData;
import java.util.ArrayList;
import javafx.scene.image.ImageView;
import jtps.jTPS_Transaction;

/**
 *
 * @author harry
 */
public class RemoveImage_Transaction implements jTPS_Transaction{
    RegioVincoListMakerApp app;
    ImageView imageToRemove;
    ArrayList<Integer> removedItemLocations;

    public RemoveImage_Transaction(RegioVincoListMakerApp initApp, ImageView initiv) {
 app = initApp;
 imageToRemove= initiv ;
    }

    
    @Override
    public void doTransaction() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.getouterpane().getChildren().remove(imageToRemove);
        
    }

    @Override
    public void undoTransaction() {
 RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.getouterpane().getChildren().add(imageToRemove) ;
    }
    

}