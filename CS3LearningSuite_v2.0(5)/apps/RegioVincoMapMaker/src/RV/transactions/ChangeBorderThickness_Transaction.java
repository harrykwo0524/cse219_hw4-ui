/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RV.transactions;

import RV.data.RegioVincoData;
import jtps.jTPS_Transaction;

/**
 *
 * @author harry
 */
public class ChangeBorderThickness_Transaction implements jTPS_Transaction {
RegioVincoData data;
double newthick;
   double oldthick; 
    public ChangeBorderThickness_Transaction(RegioVincoData initdata, double initnew, double initold) {
        data = initdata;
        oldthick = initold;
        newthick = initnew;
    }
    @Override
    public void doTransaction() {
       data.adjustBorder(newthick);
    }

    @Override
    public void undoTransaction() {
        data.adjustBorder(oldthick);
    }
    
}
