/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RV.transactions;

import RV.data.RegioVincoData;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;

/**
 *
 * @author harry
 */
public class ChangeBorderColor_Transaction implements jTPS_Transaction {
RegioVincoData data;
Color newcolor;
   Color oldcolor; 

   public ChangeBorderColor_Transaction(RegioVincoData initdata, Color initnew, Color initold) {
       data= initdata;
       newcolor = initnew;
       oldcolor= initold;
   }
    @Override
    public void doTransaction() {
      data.setColor(newcolor);
    }

    @Override
    public void undoTransaction() {
        data.setColor(oldcolor);
    }
}
