package RV.workspace.foolproof;

import djf.modules.AppGUIModule;
import djf.ui.foolproof.FoolproofDesign;
import javafx.scene.control.TextField;
import RV.RegioVincoListMakerApp;
import static RV.RegioVincoPropertyType.TDLM_DOWN_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_EDIT_ITEM_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_IMAGE_REMOVE_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_MOVE_DOWN_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_MOVE_UP_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_NAME_TEXT_FIELD;
import static RV.RegioVincoPropertyType.TDLM_OWNER_TEXT_FIELD;
import static RV.RegioVincoPropertyType.TDLM_REGION_NAME_TEXT_FIELD;
import static RV.RegioVincoPropertyType.TDLM_REMOVE_ITEM_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_SNAP_IMAGE_BOTTOMLEFT_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_SNAP_IMAGE_TOPLEFT_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_UP_BUTTON;
import RV.data.RegioVincoData;
import RV.data.RegioVincoItemPrototype;
import static djf.AppPropertyType.REDO_BUTTON;
import static djf.AppPropertyType.UNDO_BUTTON;
import javafx.scene.control.Button;
import jtps.jTPS;

/**
 *
 * @author McKillaGorilla
 */
public class RegioVincoSelectionFoolproofDesign implements FoolproofDesign {
    RegioVincoListMakerApp app;
    
    public RegioVincoSelectionFoolproofDesign(RegioVincoListMakerApp initApp) {
        app = initApp;
    }

    @Override
    public void updateControls() {
           AppGUIModule gui = app.getGUIModule        ();
           RegioVincoData data = (RegioVincoData)app.getDataComponent();
            boolean itemIsSelected = data.isItemSelected();
        if (itemIsSelected) {
            RegioVincoItemPrototype selectedItem = data.getSelectedItem();
            int index = data.getItemIndex(selectedItem);
            gui.getGUINode(TDLM_UP_BUTTON).setDisable(index == 0);
            int numItems = data.getNumItems();
            System.out.println("numItems: " + numItems);
            System.out.println("index: " + index);
            System.out.println("index == (data.getNumItems() - 1): " + (index == (data.getNumItems()-1)));
            gui.getGUINode(TDLM_DOWN_BUTTON).setDisable(index == (data.getNumItems()-1));
        }
        else {
            gui.getGUINode(TDLM_UP_BUTTON).setDisable(!itemIsSelected);
            gui.getGUINode(TDLM_DOWN_BUTTON).setDisable(!itemIsSelected);            
        }
           jTPS tps = app.getTPS();
           boolean imageIsSelected = data.imageselect();
           gui.getGUINode(TDLM_IMAGE_REMOVE_BUTTON).setDisable(!imageIsSelected);
            gui.getGUINode(TDLM_SNAP_IMAGE_TOPLEFT_BUTTON).setDisable(!imageIsSelected);
             gui.getGUINode(TDLM_SNAP_IMAGE_BOTTOMLEFT_BUTTON).setDisable(!imageIsSelected);
     Button undoButton = (Button)gui.getGUINode(UNDO_BUTTON);
        undoButton.setDisable(!tps.hasTransactionToUndo());
        Button redoButton = (Button)gui.getGUINode(REDO_BUTTON);
        redoButton.setDisable(!tps.hasTransactionToRedo());
        
    }
}