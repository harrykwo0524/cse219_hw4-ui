package RV.workspace.dialogs;

import djf.modules.AppLanguageModule;
import java.time.LocalDate;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import RV.RegioVincoListMakerApp;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_ADD_HEADER_TEXT;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_ASSIGNED_TO_PROMPT;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_ASSIGNED_TO_TEXT_FIELD;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_CANCEL_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_CATEGORY_PROMPT;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_COMPLETED_CHECK_BOX;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_COMPLETED_PROMPT;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_DESCRIPTION_PROMPT;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_EDIT_HEADER_TEXT;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_END_DATE_PROMPT;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_HEADER;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_OK_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_START_DATE_PROMPT;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_BUTTON;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_CHECK_BOX;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_DATE_PICKER;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_GRID;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_HEADER;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_PANE;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_PROMPT;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_TEXT_FIELD;
import RV.data.RegioVincoData;
import RV.data.RegioVincoItemPrototype;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_BUTTON2;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_HEADER;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_PANE;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_PROMPT2;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_TEXT_FIELD;
import static djf.AppPropertyType.APP_BANNER;
import static djf.AppPropertyType.APP_PATH_IMAGES;
import static djf.AppPropertyType.NEW_ERROR_TITLE;
import djf.ui.dialogs.AppDialogsFacade;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.ImageView;
import javax.imageio.ImageIO;

/**
 *
 * @author McKillaGorilla
 */
public class RegioVincoListItemDialog extends Stage {
    RegioVincoListMakerApp app;
    GridPane gridPane;
    
 Button prev = new Button();   
    Button next = new Button();   
    Label headerLabel = new Label();    
    Label subregionlabel = new Label();   
   Label capitalLabel = new Label();    
    Label leaderlabel = new Label();   
    Label flaglabel = new Label(); 
   TextField subregionTextField = new TextField();
    TextField capitalTextField = new TextField();
    TextField leaderTextField = new TextField();
    ImageView welcomeDialogImageView = new ImageView();
    HBox okCancelPane = new HBox();
    Button okButton = new Button();
    Button cancelButton = new Button();
    HBox flagpane = new HBox();

    RegioVincoItemPrototype itemToEdit;
    RegioVincoItemPrototype editItem;
    boolean editing;

    EventHandler cancelHandler;
    EventHandler addItemOkHandler;
    EventHandler editItemOkHandler;
    
    public RegioVincoListItemDialog(RegioVincoListMakerApp initApp) {
        // KEEP THIS FOR WHEN THE WORK IS ENTERED
        app = initApp;

        // EVERYTHING GOES IN HERE
        gridPane = new GridPane();
        gridPane.getStyleClass().add(CLASS_TDLM_DIALOG_GRID);
        initDialog();

        // NOW PUT THE GRID IN THE SCENE AND THE SCENE IN THE DIALOG
        Scene scene = new Scene(gridPane);
        this.setScene(scene);
        
        // SETUP THE STYLESHEET
        app.getGUIModule().initStylesheet(this);
        scene.getStylesheets().add(CLASS_TDLM_DIALOG_GRID);                             
        
        // MAKE IT MODAL
        this.initOwner(app.getGUIModule().getWindow());
        this.initModality(Modality.APPLICATION_MODAL);
    }
       
    protected void initGridNode(Node node, Object nodeId, String styleClass, int col, int row, int colSpan, int rowSpan, boolean isLanguageDependent) {
        // GET THE LANGUAGE SETTINGS
        AppLanguageModule languageSettings = app.getLanguageModule();
        
        // TAKE CARE OF THE TEXT
        if (isLanguageDependent) {
            languageSettings.addLabeledControlProperty(nodeId + "_TEXT", ((Labeled)node).textProperty());
            ((Labeled)node).setTooltip(new Tooltip(""));
            languageSettings.addLabeledControlProperty(nodeId + "_TOOLTIP", ((Labeled)node).tooltipProperty().get().textProperty());
        }
        
        // PUT IT IN THE UI
        if (col >= 0)
            gridPane.add(node, col, row, colSpan, rowSpan);

        // SETUP IT'S STYLE SHEET
        node.getStyleClass().add(styleClass);
    }

    private void initDialog() {
        // THE NODES ABOVE GO DIRECTLY INSIDE THE GRID
         // THE NODES ABOVE GO DIRECTLY INSIDE THE GRID
        initGridNode(headerLabel,           TDLM_ITEM_DIALOG_HEADER,                CLASS_TDLM_DIALOG_HEADER,       0, 0, 3, 1, true);
        initGridNode(prev, null, CLASS_TDLM_DIALOG_BUTTON2, 0, 1, 3, 1, true);
        initGridNode(next, null, CLASS_TDLM_DIALOG_BUTTON2, 1, 1, 3, 1, true);
        initGridNode(subregionlabel, CLASS_TDLM_DIALOG_PROMPT2, CLASS_TDLM_DIALOG_PROMPT2, 0,2,1,1, true);
        initGridNode(subregionTextField, null, CLASS_TDLM_DIALOG_TEXT_FIELD,  1,2,1,1, false);
         initGridNode(capitalLabel, CLASS_TDLM_DIALOG_PROMPT2, CLASS_TDLM_DIALOG_PROMPT2, 0,3,1,1, true);
        initGridNode(capitalTextField, null, CLASS_TDLM_DIALOG_TEXT_FIELD,  1,3,1,1, false);
         initGridNode(leaderlabel, CLASS_TDLM_DIALOG_PROMPT2, CLASS_TDLM_DIALOG_PROMPT2, 0,4,1,1, true);
        initGridNode(leaderTextField, null, CLASS_TDLM_DIALOG_TEXT_FIELD,  1,4,1,1, false);
        initGridNode(flaglabel, CLASS_TDLM_DIALOG_PROMPT2, CLASS_TDLM_DIALOG_PROMPT2,  0,5,2,1, true);
        initGridNode(flagpane, null, null, 1,6,2,1, false);
        initGridNode(okCancelPane,          null,                                   CLASS_TDLM_DIALOG_PANE,         0, 10, 2, 1, false);

        okButton = new Button();
        cancelButton = new Button();
        app.getGUIModule().addGUINode(TDLM_ITEM_DIALOG_OK_BUTTON, okButton);
        app.getGUIModule().addGUINode(TDLM_ITEM_DIALOG_CANCEL_BUTTON, cancelButton);
        okButton.getStyleClass().add(CLASS_TDLM_DIALOG_BUTTON);
        cancelButton.getStyleClass().add(CLASS_TDLM_DIALOG_BUTTON);
        okCancelPane.getChildren().add(okButton);
        okCancelPane.getChildren().add(cancelButton);
        okCancelPane.setAlignment(Pos.CENTER);
        okButton.setText("OK");
cancelButton.setText("NO");
  String title = "View|Edit Subregion";
              headerLabel.setText(title);
              prev.setText("prev");
        next.setText("Next");
subregionlabel.setText("Subregion");
capitalLabel.setText("Capital");
leaderlabel.setText("Leader's Name");
flaglabel.setText("Flag");
         welcomeDialogImageView = new javafx.scene.image.ImageView();
        
     
        
        AppLanguageModule languageSettings = app.getLanguageModule();
        languageSettings.addLabeledControlProperty(TDLM_ITEM_DIALOG_OK_BUTTON + "_TEXT",    okButton.textProperty());
        languageSettings.addLabeledControlProperty(TDLM_ITEM_DIALOG_CANCEL_BUTTON + "_TEXT",    cancelButton.textProperty());
    
        // AND SETUP THE EVENT HANDLERS
        subregionTextField.setOnAction(e->{
            processCompleteWork();
        });
        capitalTextField.setOnAction(e->{
            processCompleteWork();
        });
        leaderTextField.setOnAction(e->{
            processCompleteWork();
        });
        okButton.setOnAction(e->{
            processCompleteWork();
        });
        cancelButton.setOnAction(e->{
            RegioVincoData data = (RegioVincoData)app.getDataComponent();
            
          
          
            editItem = null;
            this.hide();
//              for(int i = 1; i < data.getmapPane().getChildren().size(); i++) {
//                  data.getmapPane().getChildren().get(i).setStyle(null);
//            }
        });   
    }
    
//    private void makeNewItem() {
//        String subregion = subregionTextField.getText();
//        String capital =  capitalTextField.getText();
//        String leader = leaderTextField.getText();
//        newItem = new RegioVincoItemPrototype(subregion, capital, leader);
//        this.hide();
//    }
    
    private void processCompleteWork() {
        // GET THE SETTINGS
        String subregion = subregionTextField.getText();
        String capital = capitalTextField.getText();
       String leader = leaderTextField.getText();
        
        // IF WE ARE EDITING
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        if (editing) {
            if (data.isValidToDoItemEdit(itemToEdit, subregion, capital, leader)) {
                editItem = new RegioVincoItemPrototype(subregion, capital, leader);
                            }
            else {
                AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), NEW_ERROR_TITLE, NEW_ERROR_TITLE);
            }
        }
        // IF WE ARE ADDING
//        else {
//            if (data.isValidNewToDoItem(subregion, capital, leader)) {
////                this.makeNewItem();    
//            } 
//            else if(category.trim().length() == 0 || description.trim().length() == 0 || assignedTo.trim().length() == 0) {
//                newItem = null;
//                  AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), ADD_ERROR_TITLE, ADD_ERROR_CONTENT);
//            }
//            else if(startDate.isAfter(endDate) || endDate.isBefore(startDate)){
//                // OPEN MESSAGE DIALOG EXPLAINING WHAT WENT WRONG
//                // @RegioVinco
//                newItem = null;
//               AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), DATE_ERROR_TITLE, DATE_ERROR_CONTENT);
//            }
//        }
        
        // CLOSE THE DIALOG
        this.hide();
    }



 public void showViewEditDialog(RegioVincoItemPrototype initItemToEdit) throws IOException {
 itemToEdit = initItemToEdit;

   editing = true;
        editItem = null;
subregionTextField.setText(itemToEdit.getSubregion());
capitalTextField.setText(itemToEdit.getCapital());
leaderTextField.setText(itemToEdit.getLeader());
PropertiesManager props = PropertiesManager.getPropertiesManager();
    try {
        
            String bannerFileName = subregionTextField.getText() + ".png";
            String bannerPath = props.getProperty(APP_PATH_IMAGES) + "/" + bannerFileName;
            File bannerFile = new File(bannerPath);
            if(bannerFile.exists()){    
                BufferedImage bufferedImage = ImageIO.read(bannerFile);
            javafx.scene.image.Image bannerImage = SwingFXUtils.toFXImage(bufferedImage, null);
            welcomeDialogImageView.setImage(bannerImage);
            welcomeDialogImageView.setFitHeight(200);
            welcomeDialogImageView.setFitWidth(200);
                
            }
            else {
                String notFileName = "forbidden" + ".png";
            String notbannerPath = props.getProperty(APP_PATH_IMAGES) + "/" + notFileName;
            File notbannerFile = new File(notbannerPath);
             BufferedImage bufferedImage = ImageIO.read(notbannerFile);
            javafx.scene.image.Image bannerImage = SwingFXUtils.toFXImage(bufferedImage, null);
            welcomeDialogImageView.setImage(bannerImage);
            welcomeDialogImageView.setFitHeight(200);
            welcomeDialogImageView.setFitWidth(200);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }    
        flagpane.getChildren().clear();
       flagpane.getChildren().add(welcomeDialogImageView);
         showAndWait();
         
    }
    

    
    public RegioVincoItemPrototype getEditItem() {
        return editItem;
    }
}