package RV.files;

import static djf.AppPropertyType.APP_EXPORT_PAGE;
import static djf.AppPropertyType.APP_PATH_EXPORT;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.scene.web.WebEngine;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import javax.swing.text.html.HTML;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import properties_manager.PropertiesManager;
import static RV.RegioVincoPropertyType.TDLM_EXPORT_TEMPLATE_FILE_NAME;
import RV.data.RegioVincoData;
import RV.data.RegioVincoItemPrototype;
import static RV.workspace.controllers.ItemsController.filepath;
import static RV.workspace.controllers.ItemsController.paths;
import static RV.workspace.controllers.ItemsController.relative;
import static djf.AppPropertyType.SAVE_SUCCESS_CONTENT;
import static djf.AppPropertyType.SAVE_SUCCESS_TITLE;
import static djf.AppTemplate.PATH_IMAGE;
import djf.ui.dialogs.AppDialogsFacade;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Polygon;
import javax.json.JsonObjectBuilder;
import javafx.scene.layout.Pane;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javax.imageio.ImageIO;
import java.io.BufferedWriter;
import java.io.FileWriter;
import javafx.embed.swing.SwingFXUtils;
import java.awt.image.BufferedImage;
/**
 *
 * @author McKillaGorilla
 */
public class RegioVincoFiles implements AppFileComponent {
    // FOR JSON SAVING AND LOADING
    static final String JSON_SUBREGION = "SUBREGION";
    static final String JSON_CAPITAL = "CAPITAL";
    static final String JSON_LEADER = "LEADER";
    
    static final String JSON_ITEMS = "items";
   static final String JSON_MAP_NAME = "MAP_NAME";
    
    static final String JSON_NUMBER_OF_SUBREGIONS = "NUMBER_OF_SUBREGIONS";
    static final String JSON_SUBREGIONS = "SUBREGIONS";
    static final String JSON_SUBREGION_INDEX = "SUBREGION_INDEX";
    static final String JSON_NUMBER_OF_SUBREGION_POLYGONS = "NUMBER_OF_SUBREGION_POLYGONS";
    static final String JSON_SUBREGION_POLYGONS = "SUBREGION_POLYGONS";
    static final String JSON_SUBREGION_POLYGON = "SUBREGION_POLYGON";
    static final String JSON_POLYGON_POINTS = "VERTICES";
    static final String JSON_POLYGON_POINT_X = "X";
    static final String JSON_POLYGON_POINT_Y= "Y";
    
     static final String JSON_IMAGE_X = "image_x";
    static final String JSON_IMAGE_Y = "image_y";
    static final String JSON_IMAGE_PATH = "image_path";
    static final String JSON_IMAGE_ARRAY = "IMAGE_ARRAY";

    // FOR EXPORTING TO HTML
    static final String TITLE_TAG = "title";
//    static final String NAME_TAG = "list_name_td";
//    static final String OWNER_TAG = "list_owner_td";
    static final String TABLE_DATA_TAG = "to_do_list_table_data";
    
    /**
     * This method is for saving user work.
     * 
     * @param data The data management component for this application.
     * 
     * @param filePath Path (including file name/extension) to where
     * to save the data to.
     * 
     * @throws IOException Thrown should there be an error writing 
     * out data to the file.
     */
   @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
   // GET THE DATA
   RegioVincoData mapViewerData = (RegioVincoData)data;
  
        
   // NOW BUILD THE JSON ARRAY FOR THE LIST
        JsonArrayBuilder subregions = Json.createArrayBuilder();
        JsonObjectBuilder subregionsObject = Json.createObjectBuilder();
        JsonArrayBuilder subregionPolygons = Json.createArrayBuilder();
        JsonArrayBuilder subregionPolygons2 = Json.createArrayBuilder();
        JsonArrayBuilder subregionPolygons3 = Json.createArrayBuilder();
        JsonObjectBuilder xyCoordinates = Json.createObjectBuilder();
        Iterator<RegioVincoItemPrototype> regioit = mapViewerData.itemsIterator();
//        RegioVincoItemPrototype next = regioit.next();
        
        
        for(int i = 0; i < mapViewerData.polygonsize(); i++){
            int numPolygons = mapViewerData.getSubregion(i).size();
            
            for(int j = 0; j < numPolygons; j ++){
                Polygon p = (Polygon)mapViewerData.getSubregion(i).get(j);
//                JsonObject itemjson = Json.createObjectBuilder()
//                        .add(JSON_SUBREGION, next.getSubregion())
//                         .add(JSON_CAPITAL, next.getCapital())
//                         .add(JSON_LEADER, next.getLeader())
//                        .build();
//                 subregionPolygons2.add(itemjson);
                for(int k = 0; k < p.getPoints().size(); k+=2){
                    xyCoordinates.add("X", mapViewerData.returnxtolong(p.getPoints().get(k)));
                    xyCoordinates.add("Y", mapViewerData.returnytolong(p.getPoints().get(k+1)));
                    subregionPolygons2.add(xyCoordinates);
                } 
                subregionPolygons.add(subregionPolygons2);
                
            }
//            JsonObject namejson = Json.createObjectBuilder().add(JSON_MAP_NAME, mapViewerData.getMapName()).build();
//            subregionPolygons.add(namejson);
//            subregionsObject.add(JSON_IMAGE_ARRAY, subregionPolygons3);
            subregionsObject.add(JSON_NUMBER_OF_SUBREGION_POLYGONS, numPolygons);
            subregionsObject.add(JSON_SUBREGION_POLYGONS, subregionPolygons);
            subregions.add(subregionsObject);
            
        }
        for(int l = 1; l < mapViewerData.getouterpane().getChildren().size(); l++) {
                  JsonObject imagejson = Json.createObjectBuilder()
                        .add(JSON_IMAGE_X, mapViewerData.getouterpane().getChildren().get(l).getTranslateX())
                                .add(JSON_IMAGE_Y,  mapViewerData.getouterpane().getChildren().get(l).getTranslateY())
                          .add(JSON_IMAGE_PATH, mapViewerData.getItemsController().managePath().get(l-1)).build();
                  subregionPolygons3.add(imagejson);
//                  subregionPolygons2.add(imagejson);
            }
        
   // THEN PUT IT ALL TOGETHER IN A JsonObject
   JsonObject mapViewerDataJSO = Json.createObjectBuilder()
           .add(JSON_IMAGE_ARRAY, subregionPolygons3)
           .add(JSON_NUMBER_OF_SUBREGIONS, mapViewerData.polygonsize())
                .add(JSON_SUBREGIONS, subregions)
                .build();
        
   // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
   Map<String, Object> properties = new HashMap<>(1);
   properties.put(JsonGenerator.PRETTY_PRINTING, true);
   JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
   StringWriter sw = new StringWriter();
   JsonWriter jsonWriter = writerFactory.createWriter(sw);
   jsonWriter.writeObject(mapViewerDataJSO);
   jsonWriter.close();

   // INIT THE WRITER
   OutputStream os = new FileOutputStream(filePath);
   JsonWriter jsonFileWriter = Json.createWriter(os);
   jsonFileWriter.writeObject(mapViewerDataJSO);
   String prettyPrinted = sw.toString();
   PrintWriter pw = new PrintWriter(filePath);
   pw.write(prettyPrinted);
   pw.close();

    }
    
    /**
     * This method loads data from a JSON formatted file into the data 
     * management component and then forces the updating of the workspace
     * such that the user may edit the data.
     * 
     * @param data Data management component where we'll load the file into.
     * 
     * @param filePath Path (including file name/extension) to where
     * to load the data from.
     * 
     * @throws IOException Thrown should there be an error
     * reading
     * in data from the file.
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
	 // NOTE THAT WE ARE USING THE SIZE OF THE MAP
        RegioVincoData mapData = (RegioVincoData)data;
        mapData.reset();
        
        // LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
	
        // THIS IS THE TOTAL NUMBER OF SUBREGIONS, EACH WITH
        // SOME NUMBER OF POLYGONS
        int numSubregions = getDataAsInt(json, JSON_NUMBER_OF_SUBREGIONS);
        JsonArray jsonSubregionsArray = json.getJsonArray(JSON_SUBREGIONS);
        JsonArray imageArray = json.getJsonArray(JSON_IMAGE_ARRAY);
        int numimage = imageArray.size();
        
       for(int j = 1; j < numimage+1; j++) {
           JsonObject imagejson = imageArray.getJsonObject(j-1);
            double px = imagejson.getJsonNumber(JSON_IMAGE_X).doubleValue();
            double py = imagejson.getJsonNumber(JSON_IMAGE_Y).doubleValue();
            String path = imagejson.getJsonString(JSON_IMAGE_PATH).getString();
            String direc = new File(".").getCanonicalPath();
            String full = "file:" + direc + "\\" + path;
            Image img = new Image(full);
            ImageView iv = new ImageView();
            iv.setImage(img);
            mapData.getouterpane().getChildren().add(iv);
            mapData.getouterpane().getChildren().get(j).setTranslateX(px);
               mapData.getouterpane().getChildren().get(j).setTranslateY(py);
        }
        // GO THROUGH ALL THE SUBREGIONS
        for (int subregionIndex = 0; subregionIndex < numSubregions; subregionIndex++) {
            JsonObject jsonSubregion = jsonSubregionsArray.getJsonObject(subregionIndex);
            int numSubregionPolygons = getDataAsInt(jsonSubregion, JSON_NUMBER_OF_SUBREGION_POLYGONS);
            ArrayList<ArrayList<Double>> subregionPointsList = new ArrayList();
            
         
            for (int polygonIndex = 0; polygonIndex < numSubregionPolygons; polygonIndex++) {

                JsonArray Polygon = jsonSubregion.getJsonArray(JSON_SUBREGION_POLYGONS);
                JsonArray pointsarray = Polygon.getJsonArray(polygonIndex);
                 ArrayList<Double> polygonPointsList = new ArrayList();
             
//                 JsonObject item = pointsarray.getJsonObject(0);
//                RegioVincoItemPrototype pro = loadItem(item);
//                mapData.addItem(pro);
//                for(int k = 0; k <  mapData.getouterpane().getChildren().size(); k++) {
//                     JsonObject pointobject = pointsarray.getJsonObject(k);
//                    double px = pointobject.getJsonNumber(JSON_IMAGE_X).doubleValue();
//                    double py = pointobject.getJsonNumber(JSON_IMAGE_Y).doubleValue();
//                    String path = pointobject.getString(JSON_IMAGE_PATH).toString();
//                    polygonPointsList.add(px);
//                      polygonPointsList.add(py);
//                        polygonPointsList.
//                }
//                    for(int k = 0; k < mapData.getouterpane().getChildren().size(); k++) {
//                        
//                    }
               
                for(int p = 1; p < pointsarray.size(); p++) {
                    JsonObject pointobject = pointsarray.getJsonObject(p);
                    double px = pointobject.getJsonNumber(JSON_POLYGON_POINT_X).doubleValue();
                    double py = pointobject.getJsonNumber(JSON_POLYGON_POINT_Y).doubleValue();
                    polygonPointsList.add(px);
                     polygonPointsList.add(py);
                }
                subregionPointsList.add(polygonPointsList);
               
             
        }
            mapData.addSubregion(subregionPointsList);
    }
        
    }
   
    
    public double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    public int getDataAsInt(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber)value;
        return number.bigIntegerValue().intValue();
    }
    
//    public RegioVincoItemPrototype loadItem(JsonObject jsonItem) {
//	// GET THE DATA
//	String subregion = jsonItem.getString(JSON_SUBREGION);
//	String capital = jsonItem.getString(JSON_CAPITAL);
//        String leader = jsonItem.getString(JSON_LEADER);
//        
//	// THEN USE THE DATA TO BUILD AN ITEM
//        RegioVincoItemPrototype item = new RegioVincoItemPrototype(subregion, capital, leader);
//        
//	// ALL DONE, RETURN IT
//	return item;
//    }
//    public ImageView loadImageView(JsonObject jsonitem) {
//        double image_X = jsonitem.getInt(JSON_IMAGE_X);
//        double image_Y = jsonitem.getInt(JSON_IMAGE_Y);
//        
//    }
 
    

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
    /**
     * This method would be used to export data to another format,
     * which we're not doing in this assignment.
     */
    @Override
    public void exportData(AppDataComponent data, String savedFileName) throws IOException {
         String mapViewerName = savedFileName.substring(0, savedFileName.indexOf("."));
        String fileToExport = mapViewerName + ".rvm";
        RegioVincoData mapViewerData = (RegioVincoData) data;
        Pane outermap =mapViewerData.getouterpane();
        try {
            // GET THE ACTUAL DATA

            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String exportDirPath = props.getProperty(APP_PATH_EXPORT) + mapViewerName + "/";
            File exportDir = new File(exportDirPath);
            if (!exportDir.exists()) {
                exportDir.mkdir();
            }
//            File json = new File(exportDirPath + relative + ".json");
            File img = new File(exportDirPath + mapViewerName + ".png");
            this.saveData(data, exportDirPath+ mapViewerName + ".json");
            //make image
            SnapshotParameters snapimage = new SnapshotParameters();
            WritableImage wI = outermap.snapshot(snapimage, null);
              BufferedImage bw = new BufferedImage(802, 536, BufferedImage.TYPE_INT_ARGB);
            ImageIO.write(SwingFXUtils.fromFXImage( wI , bw), "png", img);
            props.addProperty(APP_EXPORT_PAGE, exportDirPath + fileToExport);
            
//            saveDocument(exportDoc, props.getProperty(APP_EXPORT_PAGE));

        } catch (Exception exc) {
            throw new IOException("Error loading " + fileToExport);
        }
    }
    private void addCellToRow(Document doc, Node rowNode, String text) {
        Element tdElement = doc.createElement(HTML.Tag.TD.toString());
        tdElement.setTextContent(text);
        rowNode.appendChild(tdElement);
    }
    private Node getNodeWithId(Document doc, String tagType, String searchID) {
        NodeList testNodes = doc.getElementsByTagName(tagType);
        for (int i = 0; i < testNodes.getLength(); i++) {
            Node testNode = testNodes.item(i);
            Node testAttr = testNode.getAttributes().getNamedItem(HTML.Attribute.ID.toString());
            if ((testAttr != null) && testAttr.getNodeValue().equals(searchID)) {
                return testNode;
            }
        }
        return null;
    }
    private void saveDocument(Document doc, String outputFilePath)
            throws TransformerException, TransformerConfigurationException {
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        Result result = new StreamResult(new File(outputFilePath));
        Source source = new DOMSource(doc);
        transformer.transform(source, result);
    }

    /**
     * This method is provided to satisfy the compiler, but it
     * is not used by this application.
     */
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
         // NOTE THAT WE ARE USING THE SIZE OF THE MAP
        RegioVincoData mapData = (RegioVincoData)data;
        mapData.reset();
        
        // LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
	
        // THIS IS THE TOTAL NUMBER OF SUBREGIONS, EACH WITH
        // SOME NUMBER OF POLYGONS
        int numSubregions = getDataAsInt(json, JSON_NUMBER_OF_SUBREGIONS);
        JsonArray jsonSubregionsArray = json.getJsonArray(JSON_SUBREGIONS);

        // GO THROUGH ALL THE SUBREGIONS
        for (int subregionIndex = 0; subregionIndex < numSubregions; subregionIndex++) {
            // MAKE A POLYGON LIST FOR THIS SUBREGION
            JsonObject jsonSubregion = jsonSubregionsArray.getJsonObject(subregionIndex);
            int numSubregionPolygons = getDataAsInt(jsonSubregion, JSON_NUMBER_OF_SUBREGION_POLYGONS);
            ArrayList<ArrayList<Double>> subregionPolygonPoints = new ArrayList();
            // GO THROUGH ALL OF THIS SUBREGION'S POLYGONS
            for (int polygonIndex = 0; polygonIndex < numSubregionPolygons; polygonIndex++) {
                // GET EACH POLYGON (IN LONG/LAT GEOGRAPHIC COORDINATES)
                JsonArray jsonPolygon = jsonSubregion.getJsonArray(JSON_SUBREGION_POLYGONS);
                JsonArray pointsArray = jsonPolygon.getJsonArray(polygonIndex);
                ArrayList<Double> polygonPointsList = new ArrayList();
                for (int pointIndex = 0; pointIndex < pointsArray.size(); pointIndex++) {
                    JsonObject point = pointsArray.getJsonObject(pointIndex);
                    double pointX = point.getJsonNumber(JSON_POLYGON_POINT_X).doubleValue();
                    double pointY = point.getJsonNumber(JSON_POLYGON_POINT_Y).doubleValue();
                    polygonPointsList.add(pointX);
                    polygonPointsList.add(pointY);
                }
                subregionPolygonPoints.add(polygonPointsList);
            }
            mapData.addSubregion(subregionPolygonPoints);
        }
    }
}
