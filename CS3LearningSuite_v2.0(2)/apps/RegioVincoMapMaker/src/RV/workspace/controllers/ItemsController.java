package RV.workspace.controllers;

import java.io.IOException;
import java.util.ArrayList;
import RV.RegioVincoListMakerApp;
import RV.data.RegioVincoData;
import RV.data.RegioVincoItemPrototype;
import RV.workspace.dialogs.RegioVincoListItemDialog;
import RV.transactions.AddItem_Transaction;
//import RV.transactions.EditItem_Transaction;
import RV.transactions.MoveItem_Transaction;
import RV.transactions.RemoveItems_Transaction;
import RV.workspace.dialogs.ChangeDimensionDialog;
import RV.workspace.dialogs.View_EditSubreionDialog;
import static djf.AppPropertyType.LOAD_ERROR_CONTENT;
import static djf.AppPropertyType.LOAD_ERROR_TITLE;
import static djf.AppPropertyType.LOAD_WORK_TITLE;
import static djf.AppPropertyType.SAVE_VERIFY_CONTENT;
import static djf.AppPropertyType.SAVE_VERIFY_TITLE;
import static djf.AppPropertyType.SAVE_WORK_TITLE;
import djf.AppTemplate;
import static djf.AppTemplate.PATH_IMAGE;
import djf.modules.AppFileModule;
import djf.ui.dialogs.AppDialogsFacade;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URI;
import java.nio.file.Paths;
import javafx.beans.property.StringProperty;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Insets;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Path;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import properties_manager.PropertiesManager;

/**
 *
 * @author McKillaGorilla
 */
public class ItemsController {
    RegioVincoListMakerApp app;
    RegioVincoListItemDialog itemDialog;
    ChangeDimensionDialog changedialog;
    ArrayList<Double> oldDragValues;
    ArrayList<Double> newDragValues;
    View_EditSubreionDialog vieweditdialog;
    public static String filepath;
    public static String[] paths;
    public static ArrayList<String> relatives;
    public static String relative;
    public static int length;
    public ItemsController(RegioVincoListMakerApp initApp) {
        app = initApp;
        
        itemDialog = new RegioVincoListItemDialog(app);
        changedialog = new ChangeDimensionDialog(app);
        vieweditdialog = new View_EditSubreionDialog(app);
        
        oldDragValues = new ArrayList();
        newDragValues = new ArrayList();
        relatives= new ArrayList();
    }
    
        public void processMapMousePress(int x, int y) {
        RegioVincoData  data = (RegioVincoData )app.getDataComponent();
        
        data.startMapDrag(x, y);
    }
    
    /**
     * Respond to mouse button release on the rendering surface, which we call canvas,
     * but is actually a Pane.
     */
    public void processMapMouseRelease(int x, int y) {
        RegioVincoData  data = (RegioVincoData )app.getDataComponent();
        data.endMapDrag(x, y);
    }


    public void processMapMouseClicked(boolean leftButton, int x, int y) {
        RegioVincoData  data = (RegioVincoData )app.getDataComponent();
        if (leftButton)
            data.zoomInOnPoint(x, y);
        else
            data.zoomOutOnPoint(x, y);
    }

    public void processMapMouseScroll(boolean zoomIn, int x, int y) {
       RegioVincoData data = (RegioVincoData )app.getDataComponent();
        if (zoomIn)
            data.zoomInOnPoint(x, y);
        else
            data.zoomOutOnPoint(x, y);
    }

    /**
     * Respond to mouse dragging on the rendering surface, which we call canvas,
     * but is actually a Pane.
     */
    public void processMapMouseMoved(int x, int y) {
        RegioVincoData  data = (RegioVincoData )app.getDataComponent();
        data.update(x, y);
    }

    public void processMapMouseDragged(double x, double y) {
        RegioVincoData  data = (RegioVincoData )app.getDataComponent();
        data.updateMapDrag(x, y);
    }
    
//    public void processAddItem() {
//        itemDialog.showAddDialog();
//        RegioVincoItemPrototype newItem = itemDialog.getNewItem();        
//        if (newItem != null) {
//            // IF IT HAS A UNIQUE NAME AND COLOR
//            // THEN CREATE A TRANSACTION FOR IT
//            // AND ADD IT
//            RegioVincoData data = (RegioVincoData)app.getDataComponent();
//            AddItem_Transaction transaction = new AddItem_Transaction(data, newItem);
//            app.processTransaction(transaction);
//        }    
//        // OTHERWISE TELL THE USER WHAT THEY
//        // HAVE DONE WRONG
//        else {
//            djf.ui.dialogs.AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), "Invalid Line", "Invalid data for a new line");
//        }
//    }
    
    public void processRemoveItems() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        if (data.isItemSelected() || data.areItemsSelected()) {
            ArrayList<RegioVincoItemPrototype> itemsToRemove = new ArrayList(data.getSelectedItems());
            RemoveItems_Transaction transaction = new RemoveItems_Transaction(app, itemsToRemove);
            app.processTransaction(transaction);
        }
    }

//    public void processEditItem() {
//        RegioVincoData data = (RegioVincoData)app.getDataComponent();
//        if (data.isItemSelected()) {
//            RegioVincoItemPrototype itemToEdit = data.getSelectedItem();
//            itemDialog.showEditDialog(itemToEdit);
//            RegioVincoItemPrototype editItem = itemDialog.getEditItem();
//            if (editItem != null) {
//                EditItem_Transaction transaction = new EditItem_Transaction(itemToEdit, editItem.getSubregion(), editItem.getCapital(), editItem.getLeader());
//                app.processTransaction(transaction);
//            }
//        }
//    }
    
    public void processMoveItemUp() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        if (data.isItemSelected()) {
            RegioVincoItemPrototype itemToMove = data.getSelectedItem();
            int oldIndex = data.getItemIndex(itemToMove);
            if (oldIndex > 0) {
                MoveItem_Transaction transaction = new MoveItem_Transaction(data, oldIndex, oldIndex-1);
                app.processTransaction(transaction);
                
                // DESELECT THE OLD INDEX
                data.clearSelected();
                
                // AND SELECT THE MOVED ONE
                data.selectItem(itemToMove);
                
                // UPDATE BUTTONS
                app.getFoolproofModule().updateAll();
            }
        }
    }
    
    public void processMoveItemDown() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        if (data.isItemSelected()) {
            RegioVincoItemPrototype itemToMove = data.getSelectedItem();
            int oldIndex = data.getItemIndex(itemToMove);
            if (oldIndex < (data.getNumItems()-1)) {
                MoveItem_Transaction transaction = new MoveItem_Transaction(data, oldIndex, oldIndex+1);
                app.processTransaction(transaction);
                
                // DESELECT THE OLD INDEX
                data.clearSelected();
                
                // AND SELECT THE MOVED ONE
                data.selectItem(itemToMove);
                
                // UPDATE BUTTONS
                app.getFoolproofModule().updateAll();
            }
        }
    }
    
     public void processImageLoadRequest() {
        
}
    
    
    public void processchange() {
        changedialog.showDimensionChangeDialog();
    }
    
    public void processviewedit() throws IOException {
        vieweditdialog.showViewEditDialog();
    }
 

    /**
     * This helper method asks the user for a file to open. The user-selected
     * file is then loaded and the GUI updated. Note that if the user cancels
     * the open process, nothing is done. If an error occurs loading the file, a
     * message is displayed, but nothing changes.
     * @return 
     */
//    private void promptToLoadImage() {
//	       
//               BufferedImage img = null;
//               try {
//                   img = ImageIO.read(new File(AppTemplate.PATH_IMAGE));
//               } catch(IOException e) {
//                   e.printStackTrace();
//               }
//               JLabel lbl = new JLabel();
//               lbl.setIcon(new ImageIcon(img));
//               
//    }
    
    
    
//    public String getPath() {
//        String filepath = null;
//             final JFileChooser fc = new JFileChooser();
//    int retre = fc.showOpenDialog(fc);
//    
//        if(retre == JFileChooser.APPROVE_OPTION) {
//        filepath = fc.getSelectedFile().getAbsolutePath();
//        }
//        fc.hide();
//        return filepath;
//    }
    public ImageView returnimage() {
//         RegioVincoData data = (RegioVincoData)app.getDataComponent();
        GridPane gridpane = new GridPane();
        gridpane.setPadding(new Insets(5));
        gridpane.setHgap(10);
        gridpane.setVgap(10);
        ImageView imv = new ImageView();
        BufferedImage img = null;
             final JFileChooser fc = new JFileChooser();
    int retre = fc.showOpenDialog(fc);
    
        if(retre == JFileChooser.APPROVE_OPTION) {
        filepath = fc.getSelectedFile().getAbsolutePath();

        }
         try {
             
                   img = ImageIO.read(new File(filepath));
               } catch(IOException e) {
                   e.printStackTrace();
               }
              
Image image = SwingFXUtils.toFXImage(img, null);
imv.setImage(image);
   
     filepath =  fc.getSelectedFile().toURI().toString();
    paths = filepath.split("/");
        relative = paths[paths.length-2] + "\\" + paths[paths.length-1];
        relatives.add(relative);
       return imv;
    }
    
    
//    public String getPath(File file) {
//        String base = PATH_IMAGE;
//        filepath = new File(base).toURI().relativize(new File(filepath).toURI()).getPath();
//        return filepath;
//    }
    
    public ArrayList<String> managePath() {
        return relatives;
      
    }
//   public void processfilepath() {
//       
//   }
// 
    
}
