package RV.workspace.dialogs;

import djf.modules.AppLanguageModule;
import java.time.LocalDate;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import RV.RegioVincoListMakerApp;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_ADD_HEADER_TEXT;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_ASSIGNED_TO_PROMPT;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_ASSIGNED_TO_TEXT_FIELD;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_CANCEL_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_CATEGORY_PROMPT;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_COMPLETED_CHECK_BOX;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_COMPLETED_PROMPT;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_DESCRIPTION_PROMPT;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_EDIT_HEADER_TEXT;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_END_DATE_PROMPT;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_HEADER;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_OK_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_START_DATE_PROMPT;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_BUTTON;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_CHECK_BOX;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_DATE_PICKER;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_GRID;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_HEADER;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_PANE;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_PROMPT;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_TEXT_FIELD;
import RV.data.RegioVincoData;
import RV.data.RegioVincoItemPrototype;

/**
 *
 * @author McKillaGorilla
 */
public class RegioVincoListItemDialog extends Stage {
    RegioVincoListMakerApp app;
    GridPane gridPane;
    
    Label headerLabel = new Label();    
    Label subregionLabel = new Label();
    TextField subregionTextField = new TextField();
    Label capitalLabel = new Label();
    TextField capitalTextField = new TextField();
//    Label startDateLabel = new Label();
//    DatePicker startDatePicker = new DatePicker();    
//    Label endDateLabel = new Label();
//    DatePicker endDatePicker = new DatePicker();  
    Label leaderLabel = new Label();
    TextField leaderTextField = new TextField();
//    Label completedLabel = new Label();
//    CheckBox completedCheckBox = new CheckBox();
    HBox okCancelPane = new HBox();
    Button okButton = new Button();
    Button cancelButton = new Button();

    RegioVincoItemPrototype itemToEdit;
    RegioVincoItemPrototype newItem;
    RegioVincoItemPrototype editItem;
    boolean editing;

    EventHandler cancelHandler;
    EventHandler addItemOkHandler;
    EventHandler editItemOkHandler;
    
    public RegioVincoListItemDialog(RegioVincoListMakerApp initApp) {
        // KEEP THIS FOR WHEN THE WORK IS ENTERED
        app = initApp;

        // EVERYTHING GOES IN HERE
        gridPane = new GridPane();
        gridPane.getStyleClass().add(CLASS_TDLM_DIALOG_GRID);
        initDialog();

        // NOW PUT THE GRID IN THE SCENE AND THE SCENE IN THE DIALOG
        Scene scene = new Scene(gridPane);
        this.setScene(scene);
        
        // SETUP THE STYLESHEET
        app.getGUIModule().initStylesheet(this);
        scene.getStylesheets().add(CLASS_TDLM_DIALOG_GRID);                             
        
        // MAKE IT MODAL
        this.initOwner(app.getGUIModule().getWindow());
        this.initModality(Modality.APPLICATION_MODAL);
    }
       
    protected void initGridNode(Node node, Object nodeId, String styleClass, int col, int row, int colSpan, int rowSpan, boolean isLanguageDependent) {
        // GET THE LANGUAGE SETTINGS
        AppLanguageModule languageSettings = app.getLanguageModule();
        
        // TAKE CARE OF THE TEXT
        if (isLanguageDependent) {
            languageSettings.addLabeledControlProperty(nodeId + "_TEXT", ((Labeled)node).textProperty());
            ((Labeled)node).setTooltip(new Tooltip(""));
            languageSettings.addLabeledControlProperty(nodeId + "_TOOLTIP", ((Labeled)node).tooltipProperty().get().textProperty());
        }
        
        // PUT IT IN THE UI
        if (col >= 0)
            gridPane.add(node, col, row, colSpan, rowSpan);

        // SETUP IT'S STYLE SHEET
        node.getStyleClass().add(styleClass);
    }

    private void initDialog() {
        // THE NODES ABOVE GO DIRECTLY INSIDE THE GRID
        initGridNode(headerLabel,           TDLM_ITEM_DIALOG_HEADER,                CLASS_TDLM_DIALOG_HEADER,       0, 0, 3, 1, true);
        initGridNode(subregionLabel,         TDLM_ITEM_DIALOG_CATEGORY_PROMPT,       CLASS_TDLM_DIALOG_PROMPT,       0, 1, 1, 1, true);
        initGridNode(subregionTextField,     null,                                   CLASS_TDLM_DIALOG_TEXT_FIELD,   1, 1, 1, 1, false);
        initGridNode(capitalLabel,      TDLM_ITEM_DIALOG_DESCRIPTION_PROMPT,    CLASS_TDLM_DIALOG_PROMPT,       0, 2, 1, 1, true);
        initGridNode(capitalTextField,  null,                                   CLASS_TDLM_DIALOG_TEXT_FIELD,   1, 2, 1, 1, false);
        initGridNode(leaderLabel,       TDLM_ITEM_DIALOG_ASSIGNED_TO_PROMPT,    CLASS_TDLM_DIALOG_PROMPT,       0, 5, 1, 1, true);
        initGridNode(leaderTextField,   TDLM_ITEM_DIALOG_ASSIGNED_TO_TEXT_FIELD,CLASS_TDLM_DIALOG_TEXT_FIELD,   1, 5, 1, 1, false);
        initGridNode(okCancelPane,          null,                                   CLASS_TDLM_DIALOG_PANE,         0, 7, 3, 1, false);

        okButton = new Button();
        cancelButton = new Button();
        app.getGUIModule().addGUINode(TDLM_ITEM_DIALOG_OK_BUTTON, okButton);
        app.getGUIModule().addGUINode(TDLM_ITEM_DIALOG_CANCEL_BUTTON, cancelButton);
        okButton.getStyleClass().add(CLASS_TDLM_DIALOG_BUTTON);
        cancelButton.getStyleClass().add(CLASS_TDLM_DIALOG_BUTTON);
        okCancelPane.getChildren().add(okButton);
        okCancelPane.getChildren().add(cancelButton);
        okCancelPane.setAlignment(Pos.CENTER);

        AppLanguageModule languageSettings = app.getLanguageModule();
        languageSettings.addLabeledControlProperty(TDLM_ITEM_DIALOG_OK_BUTTON + "_TEXT",    okButton.textProperty());
        languageSettings.addLabeledControlProperty(TDLM_ITEM_DIALOG_CANCEL_BUTTON + "_TEXT",    cancelButton.textProperty());
       
        // AND SETUP THE EVENT HANDLERS
        subregionTextField.setOnAction(e->{
            processCompleteWork();
        });
        capitalTextField.setOnAction(e->{
            processCompleteWork();
        });
        okButton.setOnAction(e->{
            processCompleteWork();
        });
        cancelButton.setOnAction(e->{
            newItem = null;
            editItem = null;
            this.hide();
        });   
    }
    
    private void makeNewItem() {
        String subregion = subregionTextField.getText();
        String capital =  capitalTextField.getText();
        String leader = leaderTextField.getText();
        newItem = new RegioVincoItemPrototype(subregion, capital, leader);
        this.hide();
    }
    
    public void processCompleteWork() {
        // GET THE SETTINGS
        String subregion = subregionTextField.getText();
        String capital = capitalTextField.getText();
        String leader = leaderTextField.getText();
        
        // IF WE ARE EDITING
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        if (editing) {
            if (data.isValidToDoItemEdit(itemToEdit, subregion, capital, leader)) {
                itemToEdit.setSubregion(subregion);
                itemToEdit.setCapital(capital);
                itemToEdit.setLeader(leader);
            }
            else {
                // OPEN MESSAGE DIALOG EXPLAINING WHAT WENT WRONG
                // @todo
            }
        }
        // IF WE ARE ADDING
        else {
            if (data.isValidNewToDoItem(subregion, capital, leader)) {
                this.makeNewItem();
            }
            else {
                // OPEN MESSAGE DIALOG EXPLAINING WHAT WENT WRONG
                // @todo
            }
        }
        
        // CLOSE THE DIALOG
        this.hide();
    }

//    public void showAddDialog() {        
//        // USE THE TEXT IN THE HEADER FOR ADD
//        PropertiesManager props = PropertiesManager.getPropertiesManager();
//        String headerText = props.getProperty(TDLM_ITEM_DIALOG_ADD_HEADER_TEXT);
//        headerLabel.setText(headerText);
//        setTitle(headerText);
//
//        // USE THE TEXT IN THE HEADER FOR ADD
//        categoryTextField.setText("");
//        descriptionTextField.setText("");
//        startDatePicker.setValue(LocalDate.now());
//        endDatePicker.setValue(LocalDate.now());
//        assignedToTextField.setText("");
//        completedCheckBox.selectedProperty().setValue(false);
//        
//        // WE ARE ADDING A NEW ONE, NOT EDITING
//        editing = false;
//        editItem = null;
//        
//        // AND OPEN THE DIALOG
//        showAndWait();
//    }

//    public void showEditDialog(RegioVincoItemPrototype initItemToEdit) {
//        // WE'LL NEED THIS FOR VALIDATION
//        itemToEdit = initItemToEdit;
//        
//        // USE THE TEXT IN THE HEADER FOR EDIT
//        PropertiesManager props = PropertiesManager.getPropertiesManager();
//        String headerText = props.getProperty(TDLM_ITEM_DIALOG_EDIT_HEADER_TEXT);
//        headerLabel.setText(headerText);
//        setTitle(headerText);
//        
//        // WE'LL ONLY PROCEED IF THERE IS A LINE TO EDIT
//        editing = true;
//        editItem = null;
//        
//        // USE THE TEXT IN THE HEADER FOR EDIT
//        categoryTextField.setText(itemToEdit.getCategory());
//        descriptionTextField.setText(itemToEdit.getDescription());
//        startDatePicker.setValue(itemToEdit.getStartDate());
//        endDatePicker.setValue(itemToEdit.getEndDate());
//        assignedToTextField.setText(itemToEdit.getAssignedTo());
//        completedCheckBox.selectedProperty().setValue(itemToEdit.isCompleted());
//               
//        // AND OPEN THE DIALOG
//        showAndWait();
//    }
    
    public RegioVincoItemPrototype getNewItem() {
        return newItem;
    }
    
    public RegioVincoItemPrototype getEditItem() {
        return editItem;
    }
}