package RV.workspace.foolproof;

import djf.modules.AppGUIModule;
import djf.ui.foolproof.FoolproofDesign;
import javafx.scene.control.TextField;
import RV.RegioVincoListMakerApp;
import static RV.RegioVincoPropertyType.TDLM_EDIT_ITEM_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_MOVE_DOWN_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_MOVE_UP_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_NAME_TEXT_FIELD;
import static RV.RegioVincoPropertyType.TDLM_OWNER_TEXT_FIELD;
import static RV.RegioVincoPropertyType.TDLM_REMOVE_ITEM_BUTTON;
import RV.data.RegioVincoData;
import RV.data.RegioVincoItemPrototype;

/**
 *
 * @author McKillaGorilla
 */
public class RegioVincoSelectionFoolproofDesign implements FoolproofDesign {
    RegioVincoListMakerApp app;
    
    public RegioVincoSelectionFoolproofDesign(RegioVincoListMakerApp initApp) {
        app = initApp;
    }

    @Override
    public void updateControls() {
        AppGUIModule gui = app.getGUIModule        ();
       
        // CHECK AND SEE IF A TABLE ITEM IS SELECTED
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        boolean itemIsSelected = data.isItemSelected();
        boolean itemsAreSelected = data.areItemsSelected();
//        gui.getGUINode(TDLM_EDIT_ITEM_BUTTON).setDisable(!itemIsSelected);
//        gui.getGUINode(TDLM_REMOVE_ITEM_BUTTON).setDisable(!(itemIsSelected || itemsAreSelected));
        if (itemIsSelected) {
            RegioVincoItemPrototype selectedItem = data.getSelectedItem();
            int index = data.getItemIndex(selectedItem);
//            gui.getGUINode(TDLM_MOVE_UP_BUTTON).setDisable(index == 0);
            int numItems = data.getNumItems();
            System.out.println("numItems: " + numItems);
            System.out.println("index: " + index);
            System.out.println("index == (data.getNumItems() - 1): " + (index == (data.getNumItems()-1)));
//            gui.getGUINode(TDLM_MOVE_DOWN_BUTTON).setDisable(index == (data.getNumItems()-1));
        }
        else {
//            gui.getGUINode(TDLM_MOVE_UP_BUTTON).setDisable(!itemIsSelected);
//            gui.getGUINode(TDLM_MOVE_DOWN_BUTTON).setDisable(!itemIsSelected);            
        }
//        ((TextField)gui.getGUINode(TDLM_NAME_TEXT_FIELD)).setEditable(true);
//        ((TextField)gui.getGUINode(TDLM_OWNER_TEXT_FIELD)).setEditable(true);
    }
}