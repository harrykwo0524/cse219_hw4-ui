package RV.transactions;

import java.util.ArrayList;
import jtps.jTPS_Transaction;
import RV.RegioVincoListMakerApp;
import RV.data.RegioVincoData;
import RV.data.RegioVincoItemPrototype;

/**
 *
 * @author McKillaGorilla
 */
public class PasteItems_Transaction implements jTPS_Transaction {
    RegioVincoListMakerApp app;
    ArrayList<RegioVincoItemPrototype> itemsToPaste;
    int pasteIndex;
    
    public PasteItems_Transaction(  RegioVincoListMakerApp initApp, 
                                    ArrayList<RegioVincoItemPrototype> initItemsToPaste,
                                    int initPasteIndex) {
        app = initApp;
        itemsToPaste = initItemsToPaste;
        pasteIndex = initPasteIndex;
    }

    @Override
    public void doTransaction() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        int index = pasteIndex+1;
        for (RegioVincoItemPrototype itemToPaste : itemsToPaste) {
            data.addItemAt(itemToPaste, index);
            index++;
        }
    }

    @Override
    public void undoTransaction() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        for (RegioVincoItemPrototype itemToPaste : itemsToPaste) {
            data.removeItem(itemToPaste);
        }
    }   
}