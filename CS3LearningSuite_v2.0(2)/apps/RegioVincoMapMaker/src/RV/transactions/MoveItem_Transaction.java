package RV.transactions;

import jtps.jTPS_Transaction;
import RV.data.RegioVincoData;
import RV.data.RegioVincoItemPrototype;

/**
 *
 * @author McKillaGorilla
 */
public class MoveItem_Transaction implements jTPS_Transaction {
    RegioVincoData data;
    int oldIndex;
    int newIndex;
    
    public MoveItem_Transaction(RegioVincoData initData, int initOldIndex, int initNewIndex) {
        data = initData;
        oldIndex = initOldIndex;
        newIndex = initNewIndex;
    }
    
    @Override
    public void doTransaction() {
        data.moveItem(oldIndex, newIndex);
    }

    @Override
    public void undoTransaction() {
        data.moveItem(newIndex, oldIndex);
    }   
}