package RV.transactions;

import jtps.jTPS_Transaction;
import RV.data.RegioVincoData;
import RV.data.RegioVincoItemPrototype;

/**
 *
 * @author McKillaGorilla
 */
public class AddItem_Transaction implements jTPS_Transaction {
    RegioVincoData data;
    RegioVincoItemPrototype itemToAdd;
    
    public AddItem_Transaction(RegioVincoData initData, RegioVincoItemPrototype initNewItem) {
        data = initData;
        itemToAdd = initNewItem;
    }

    @Override
    public void doTransaction() {
        data.addItem(itemToAdd);        
    }

    @Override
    public void undoTransaction() {
        data.removeItem(itemToAdd);
    }
}
