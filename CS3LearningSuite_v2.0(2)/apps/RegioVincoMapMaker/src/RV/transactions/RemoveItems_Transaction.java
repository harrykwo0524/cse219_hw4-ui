package RV.transactions;

import static djf.AppPropertyType.APP_CLIPBOARD_FOOLPROOF_SETTINGS;
import java.util.ArrayList;
import jtps.jTPS_Transaction;
import RV.RegioVincoListMakerApp;
import RV.data.RegioVincoData;
import RV.data.RegioVincoItemPrototype;

/**
 *
 * @author McKillaGorilla
 */
public class RemoveItems_Transaction implements jTPS_Transaction {
    RegioVincoListMakerApp app;
    ArrayList<RegioVincoItemPrototype> itemsToRemove;
    ArrayList<Integer> removedItemLocations;
    
    public RemoveItems_Transaction(RegioVincoListMakerApp initApp, ArrayList<RegioVincoItemPrototype> initItems) {
        app = initApp;
        itemsToRemove = initItems;
    }

    @Override
    public void doTransaction() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        removedItemLocations = data.removeAll(itemsToRemove);
    }

    @Override
    public void undoTransaction() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.addAll(itemsToRemove, removedItemLocations);
    }
}