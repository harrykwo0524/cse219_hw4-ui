package RV.workspace.controllers;

import djf.AppTemplate;
import djf.modules.AppGUIModule;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import RV.RegioVincoListMakerApp;
import static RV.RegioVincoPropertyType.TDLM_ITEMS_TABLE_VIEW;
import RV.data.RegioVincoItemPrototype;

/**
 *
 * @author McKillaGorilla
 */
public class ItemsTableController {
    RegioVincoListMakerApp app;

    public ItemsTableController(AppTemplate initApp) {
        app = (RegioVincoListMakerApp)initApp;
    }

    public void processChangeTableSize() {
        AppGUIModule gui = app.getGUIModule();
        TableView<RegioVincoItemPrototype> itemsTable = (TableView)gui.getGUINode(TDLM_ITEMS_TABLE_VIEW);
        ObservableList columns = itemsTable.getColumns();
        for (int i = 0; i < columns.size(); i++) {
            TableColumn column = (TableColumn)columns.get(i);
            column.setMinWidth(itemsTable.widthProperty().getValue()/columns.size());
            column.setMaxWidth(itemsTable.widthProperty().getValue()/columns.size());
        }
    }    
}
