package RV.transactions;

import jtps.jTPS_Transaction;
import static djf.AppPropertyType.APP_CLIPBOARD_FOOLPROOF_SETTINGS;
import java.util.ArrayList;
import javafx.collections.ObservableList;
import RV.RegioVincoListMakerApp;
import RV.data.RegioVincoData;
import RV.data.RegioVincoItemPrototype;

/**
 *
 * @author McKillaGorilla
 */
public class CutItems_Transaction implements jTPS_Transaction {
    RegioVincoListMakerApp app;
    ArrayList<RegioVincoItemPrototype> itemsToCut;
    ArrayList<Integer> cutItemLocations;
    
    public CutItems_Transaction(RegioVincoListMakerApp initApp, ArrayList<RegioVincoItemPrototype> initItemsToCut) {
        app = initApp;
        itemsToCut = initItemsToCut;
    }

    @Override
    public void doTransaction() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        cutItemLocations = data.removeAll(itemsToCut);
        app.getFoolproofModule().updateControls(APP_CLIPBOARD_FOOLPROOF_SETTINGS);
    }

    @Override
    public void undoTransaction() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.addAll(itemsToCut, cutItemLocations);
        app.getFoolproofModule().updateControls(APP_CLIPBOARD_FOOLPROOF_SETTINGS);
    }   
}