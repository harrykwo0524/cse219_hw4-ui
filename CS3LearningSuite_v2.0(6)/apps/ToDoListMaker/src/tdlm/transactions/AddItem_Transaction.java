package tdlm.transactions;

import jtps.jTPS_Transaction;
import tdlm.ToDoListMakerApp;
import tdlm.data.ToDoData;
import tdlm.data.ToDoItemPrototype;

/**
 *
 * @author McKillaGorilla
 */
public class AddItem_Transaction implements jTPS_Transaction {
    ToDoListMakerApp app;
    ToDoData data;
    ToDoItemPrototype itemToAdd;
    
    public AddItem_Transaction(ToDoListMakerApp initApp, ToDoData initData, ToDoItemPrototype initNewItem) {
        app = initApp;
        data = initData;
        itemToAdd = initNewItem;
    }

    @Override
    public void doTransaction() {
        data.addItem(itemToAdd); 
        app.getFileModule().markAsEdited(true);
    }

    @Override
    public void undoTransaction() {
        data.removeItem(itemToAdd);
         app.getFileModule().markAsEdited(true);
    }
}
