/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tdlm.transactions;

import java.util.ArrayList;
import jtps.jTPS_Transaction;
import tdlm.ToDoListMakerApp;
import tdlm.data.ToDoData;
import tdlm.data.ToDoItemPrototype;

/**
 *
 * @author harry
 */
public class EditItem_Transaction implements jTPS_Transaction {
    
ToDoListMakerApp app;
    ToDoItemPrototype olditem;
    ToDoItemPrototype itemToEdit;
    
     public EditItem_Transaction(ToDoListMakerApp initApp, ToDoItemPrototype initItem, ToDoItemPrototype EditItem) {
                 app = initApp;
                olditem = initItem;
                itemToEdit = EditItem;
                
    }
    @Override
    public void doTransaction() {
       ToDoData data = (ToDoData)app.getDataComponent();   
      int index = data.getItemIndex(olditem);
      data.addItemAt(itemToEdit, index);
      data.removeItem(olditem);
       app.getFileModule().markAsEdited(true);
    }

    @Override
    public void undoTransaction() {
      ToDoData data = (ToDoData)app.getDataComponent();   
      int index = data.getItemIndex(itemToEdit);
      data.addItemAt(olditem, index);
      data.removeItem(itemToEdit);
       app.getFileModule().markAsEdited(true);
    }
    
}
