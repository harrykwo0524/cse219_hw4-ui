/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tdlm.transactions;

import java.util.ArrayList;
import jtps.jTPS_Transaction;
import tdlm.ToDoListMakerApp;
import tdlm.data.ToDoData;
import tdlm.data.ToDoItemPrototype;

/**
 *
 * @author harry
 */
public class MoveUp_Transaction implements  jTPS_Transaction{
     ToDoListMakerApp app;
    ArrayList<ToDoItemPrototype> itemsToMoveUp;
    ArrayList<Integer> movedItemLocations;
    
    public MoveUp_Transaction(ToDoListMakerApp initApp, ArrayList<ToDoItemPrototype> initItems) {
        app = initApp;
        itemsToMoveUp = initItems;
    }

    public void doTransaction() {
        ToDoData data = (ToDoData)app.getDataComponent();
        int oldposition = data.getItemIndex(data.getSelectedItem())-1;
         int newposition = data.getItemIndex(data.getSelectedItem());
        data.moveItem(oldposition, newposition);
         app.getFileModule().markAsEdited(true);
    }

    public void undoTransaction() {
        ToDoData data = (ToDoData)app.getDataComponent();
          int oldposition = data.getItemIndex(data.getSelectedItem());
         int newposition = data.getItemIndex(data.getSelectedItem())+1;
        data.moveItem(newposition, oldposition);
         app.getFileModule().markAsEdited(true);
        
    }
}

