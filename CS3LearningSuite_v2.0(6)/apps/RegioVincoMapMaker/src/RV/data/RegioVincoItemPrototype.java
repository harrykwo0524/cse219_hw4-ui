package RV.data;

import java.time.LocalDate;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author McKillaGorilla
 */
public class RegioVincoItemPrototype implements Cloneable {
    public static final String DEFAULT_SUBREGION = "";
    public static final String DEFAULT_CAPITAL = "";
    public static final String DEFAULT_LEADER= "";

    final StringProperty subregion;
    final StringProperty capital;
    final StringProperty leader;
       
    public RegioVincoItemPrototype() {
        subregion = new SimpleStringProperty(DEFAULT_SUBREGION);
        capital = new SimpleStringProperty(DEFAULT_CAPITAL);
        leader = new SimpleStringProperty(DEFAULT_LEADER);
     
    }

    public RegioVincoItemPrototype(String initSubregion, String initCapital, String initLeader) {
        this();
        subregion.set(initSubregion);
        capital.set(initCapital);
        leader.set(initLeader);
    
    }

    public String getSubregion() {
        return subregion.get();
    }

    public void setSubregion(String value) {
        subregion.set(value);
    }

    public StringProperty subregionProperty() {
        return subregion;
    }

    public String getCapital() {
        return capital.get();
    }

    public void setCapital(String value) {
        capital.set(value);
    }

    public StringProperty capitalProperty() {
        return capital;
    }

  
    public String getLeader() {
        return leader.get();
    }
    
    public void setLeader(String initAssignedTo) {
        leader.setValue(initAssignedTo);
    }
    
    public StringProperty LeaderProperty() {
        return leader;
    }

  
    
    public void reset() {
        setSubregion(DEFAULT_SUBREGION);
        setCapital(DEFAULT_CAPITAL);
        setLeader(DEFAULT_LEADER);
       
    }

    public Object clone() {
        return new RegioVincoItemPrototype(   subregion.getValue(), 
                                        capital.getValue(), 
                                        leader.getValue());
    }
    
    public boolean equals(Object obj) {
        return this == obj;
    }
}