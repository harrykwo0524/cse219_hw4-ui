/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RV.transactions;

import RV.data.RegioVincoData;
import jtps.jTPS_Transaction;

/**
 *
 * @author harry
 */
public class ResetViewport_Transaction implements jTPS_Transaction {
RegioVincoData data;
double sx;
double sy;
  double ax;
double ay;    

public ResetViewport_Transaction(RegioVincoData initData, double initsx, double initsy, double initax, double initay) {
    data = initData;
    sx = initsx;
    sy = initsy;
    ax = initax;
    ay = initay;
}

    @Override
    public void doTransaction() {
        data.resetViewport();
    }

    @Override
    public void undoTransaction() {
         data.getmapPane().setTranslateX(ax);
      data.getmapPane().setTranslateY(ay);
      data.getmapPane().setScaleX(sx);
        data.getmapPane().setScaleY(sy);
    }
}
