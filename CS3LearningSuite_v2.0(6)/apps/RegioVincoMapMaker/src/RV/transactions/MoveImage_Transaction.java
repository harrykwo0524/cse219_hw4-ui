/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RV.transactions;

import RV.data.RegioVincoData;
import javafx.scene.image.ImageView;
import jtps.jTPS_Transaction;

/**
 *
 * @author harry
 */
public class MoveImage_Transaction implements jTPS_Transaction {
RegioVincoData data;
double cx;
double cy;
double sx;
double sy;
ImageView imv;
double diffx;
double diffy;
    
    public MoveImage_Transaction(RegioVincoData initData, double initx, double inity, double oldx, double oldy, ImageView initimv) {
    data = initData;
    sx = initx;
    sy = inity;
    cx = oldx;
    cy = oldy;
    imv = initimv;
    }

    
    @Override
    public void doTransaction() {
       diffx = sx - cx;
      diffy = sy-cy;
//       double tx = imv.getTranslateX()+ diffx;
//        double ty = imv.getTranslateY() + diffy;
       imv.setTranslateX(diffx);
         imv.setTranslateY(diffy);
//        data.clamp();
    }

    @Override
    public void undoTransaction() {
//   diffx = sx - cx;
//      diffy = sy-cy;
       imv.setTranslateX(imv.getTranslateX() - sx-cx);
         imv.setTranslateY(imv.getTranslateY() - sy-cy);
//            imv.setTranslateX(sx);
//         imv.setTranslateY(sy);
//        data.clamp();
    }
    
}
