/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RV.transactions;

import RV.data.RegioVincoData;
import jtps.jTPS_Transaction;

/**
 *
 * @author harry
 */
public class Fit_Transaction implements jTPS_Transaction {
RegioVincoData data;

    public Fit_Transaction(RegioVincoData initdata) {
        data = initdata;
    }
    @Override
    public void doTransaction() {
        data.fitToRegion();
    }

    @Override
    public void undoTransaction() {
    
    }
    
}
