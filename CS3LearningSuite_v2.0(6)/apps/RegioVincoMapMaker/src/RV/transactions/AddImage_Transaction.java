/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RV.transactions;

import RV.data.RegioVincoData;
import javafx.scene.image.ImageView;
import jtps.jTPS_Transaction;

/**
 *
 * @author harry
 */
public class AddImage_Transaction implements jTPS_Transaction{
    RegioVincoData initData;
    ImageView initiv;
    
    public AddImage_Transaction(RegioVincoData data, ImageView iv) {
        initData = data;
        initiv = iv;
    }
    @Override
    public void doTransaction() {
       initData.getouterpane().getChildren().add(initiv);
       initiv.setFitHeight(200);
       initiv.setFitWidth(200);
    }

    @Override
    public void undoTransaction() {
          initData.getouterpane().getChildren().remove(initiv);
           initiv.setFitHeight(200);
       initiv.setFitWidth(200);
    }
    
}
