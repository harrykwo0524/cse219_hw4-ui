package RV.transactions;

import RV.data.RegioVincoData;
import jtps.jTPS_Transaction;
import RV.data.RegioVincoData;
import RV.data.RegioVincoItemPrototype;
import javafx.collections.ObservableList;
import javafx.scene.shape.Polygon;

/**
 *
 * @author McKillaGorilla
 */
public class MoveItem_Transaction implements jTPS_Transaction {
    RegioVincoData data;
    int oldIndex;
    int newIndex;
    
    public MoveItem_Transaction(RegioVincoData initData, int initOldIndex, int initNewIndex) {
        data = initData;
        oldIndex = initOldIndex;
        newIndex = initNewIndex;
    }
    
    @Override
    public void doTransaction() {
        data.moveItem(oldIndex, newIndex);
         ObservableList<Polygon> oldp = data.getSubregion(oldIndex);
                                       ObservableList<Polygon> newp = data.getSubregion(newIndex);
                                       data.returnSubregion().replace(newp, oldp);
                                        data.returnSubregion().replace(oldp, newp);
    }

    @Override
    public void undoTransaction() {
        data.moveItem(newIndex, oldIndex);
         ObservableList<Polygon> oldp = data.getSubregion(oldIndex);
                                       ObservableList<Polygon> newp = data.getSubregion(newIndex);
                                       data.returnSubregion().replace(newp, oldp);
                                        data.returnSubregion().replace(oldp, newp);
    }   
}