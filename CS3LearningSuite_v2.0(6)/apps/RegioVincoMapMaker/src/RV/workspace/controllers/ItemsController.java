package RV.workspace.controllers;

import java.io.IOException;
import java.util.ArrayList;
import RV.RegioVincoListMakerApp;
import static RV.RegioVincoPropertyType.TDLM_ITEMS_TABLE_VIEW;
import RV.data.RegioVincoData;
import RV.data.RegioVincoItemPrototype;
import RV.transactions.AddImage_Transaction;
import RV.transactions.ChangeBorderColor_Transaction;
import RV.transactions.ChangeBorderThickness_Transaction;
import RV.transactions.Fit_Transaction;
import RV.transactions.MoveImage_Transaction;
import RV.transactions.RandomColor_Transaction;
import RV.transactions.RemoveImage_Transaction;
import RV.transactions.SnapLeftBottom_Transaction;
import RV.workspace.dialogs.RegioVincoListItemDialog;
import RV.workspace.dialogs.ChangeDimensionDialog;
import RV.workspace.dialogs.CreateNewDialog;
import static djf.AppPropertyType.LOAD_ERROR_CONTENT;
import static djf.AppPropertyType.LOAD_ERROR_TITLE;
import static djf.AppPropertyType.LOAD_WORK_TITLE;
import static djf.AppPropertyType.SAVE_VERIFY_CONTENT;
import static djf.AppPropertyType.SAVE_VERIFY_TITLE;
import static djf.AppPropertyType.SAVE_WORK_TITLE;
import djf.AppTemplate;
import static djf.AppTemplate.PATH_IMAGE;
import djf.modules.AppFileModule;
import djf.ui.dialogs.AppDialogsFacade;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URI;
import java.nio.file.Paths;
import java.util.Collection;
import javafx.beans.property.StringProperty;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Insets;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Path;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import properties_manager.PropertiesManager;
import RV.transactions.EditItem_Transaction;
import RV.transactions.FitToRegions_Transaction;
import RV.transactions.MoveItem_Transaction;
import RV.transactions.RG_Transaction;
import RV.transactions.ResetViewport_Transaction;
import RV.transactions.SnapTopLeft_Transaction;
import javafx.collections.ObservableList;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableView;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author McKillaGorilla
 */
public class ItemsController {
    RegioVincoListMakerApp app;
    RegioVincoListItemDialog itemDialog;
    ChangeDimensionDialog changedialog;
    ArrayList<Double> oldDragValues;
    ArrayList<Double> newDragValues;
    public static String filepath;
    public static String[] paths;
    public static ArrayList<String> relatives;
    public static String relative;
    public static int length;
    double moveInc = 25.0;
    public ImageView imv;
    public ItemsController(RegioVincoListMakerApp initApp) {
        app = initApp;
        
        itemDialog = new RegioVincoListItemDialog(app);
        changedialog = new ChangeDimensionDialog(app);
        
        oldDragValues = new ArrayList();
        newDragValues = new ArrayList();
        relatives= new ArrayList();
        
    }
    
        public void processMapMousePress(int x, int y) {
        RegioVincoData  data = (RegioVincoData )app.getDataComponent();
        data.startMapDrag(x, y);
    }
        
//        public void processOuterMapPress(int x, int y) {
//             RegioVincoData  data = (RegioVincoData )app.getDataComponent();
//             
//        data.startOuterMapDrag(x, y);
//        }
    
    /**
     * Respond to mouse button release on the rendering surface, which we call canvas,
     * but is actually a Pane.
     */
    public void processMapMouseRelease(int x, int y) {
        RegioVincoData  data = (RegioVincoData )app.getDataComponent();
        data.endMapDrag(x, y);
    }
    
//    public void processOuterMapMouseRelease(int x, int y) {
//        RegioVincoData  data = (RegioVincoData )app.getDataComponent();
//        data.endOuterMapDrag(x, y);
//    }


    public void processMapMouseClicked(boolean leftButton, int x, int y) {
        RegioVincoData  data = (RegioVincoData )app.getDataComponent();
        if (leftButton)
            data.zoomInOnPoint(x, y);
        else
            data.zoomOutOnPoint(x, y);
    }
//    public void processOuterMapMouseClicked(boolean leftButton, int x, int y) {
//        RegioVincoData  data = (RegioVincoData )app.getDataComponent();
//        
//        if (leftButton)
//            data.outerzoomInOnPoint(x, y);
//        else
//            data.outerzoomOutOnPoint(x, y);
//    }

    public void processMapMouseScroll(boolean zoomIn, int x, int y) {
       RegioVincoData data = (RegioVincoData )app.getDataComponent();
        if (zoomIn)
            data.zoomInOnPoint(x, y);
        else
            data.zoomOutOnPoint(x, y);
    }

    /**
     * Respond to mouse dragging on the rendering surface, which we call canvas,
     * but is actually a Pane.
     */
    public void processMapMouseMoved(int x, int y) {
        RegioVincoData  data = (RegioVincoData )app.getDataComponent();
        data.update(x, y);
    }
//    public void processOuterMapMouseMoved(int x, int y) {
//        RegioVincoData  data = (RegioVincoData )app.getDataComponent();
//        data.outerupdate(x, y);
//    }

    public void processMapMouseDragged(double x, double y) {
        RegioVincoData  data = (RegioVincoData )app.getDataComponent();
        data.updateMapDrag(x, y);
    }
//    public void processOuterMapMouseDragged(double x, double y) {
//        RegioVincoData  data = (RegioVincoData )app.getDataComponent();
//        data.updateOuterMapDrag(x, y);
//    }
    
//    public void processAddItem() {
//        itemDialog.showAddDialog();
//        RegioVincoItemPrototype newItem = itemDialog.getNewItem();        
//        if (newItem != null) {
//            // IF IT HAS A UNIQUE NAME AND COLOR
//            // THEN CREATE A TRANSACTION FOR IT
//            // AND ADD IT
//            RegioVincoData data = (RegioVincoData)app.getDataComponent();
//            AddItem_Transaction transaction = new AddItem_Transaction(data, newItem);
//            app.processTransaction(transaction);
//        }    
//        // OTHERWISE TELL THE USER WHAT THEY
//        // HAVE DONE WRONG
//        else {
//            djf.ui.dialogs.AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), "Invalid Line", "Invalid data for a new line");
//        }
//    }
    
//    public void processRemoveItems() {
//        RegioVincoData data = (RegioVincoData)app.getDataComponent();
//        if (data.isItemSelected() || data.areItemsSelected()) {
//            ArrayList<RegioVincoItemPrototype> itemsToRemove = new ArrayList(data.getSelectedItems());
//            RemoveItems_Transaction transaction = new RemoveItems_Transaction(app, itemsToRemove);
//            app.processTransaction(transaction);
//        }
//    }

      public void processEditItems() throws IOException {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        RegioVincoItemPrototype select = data.getSelectedItem();
        if(data.isItemSelected()) {
            itemDialog.showViewEditDialog(select);  
            RegioVincoItemPrototype itemtoedit = itemDialog.getEditItem();
            if(itemtoedit != null) {
                EditItem_Transaction transaction = new EditItem_Transaction(app, select, itemtoedit);
                app.processTransaction(transaction);
               
            }
           
            else {
                
            }
           
        }
    }
      
      public void processEditByMap(Polygon p) throws IOException {
          RegioVincoData data = (RegioVincoData)app.getDataComponent();
          RegioVincoItemPrototype select = new RegioVincoItemPrototype();
          TableView tableView = (TableView) app.getGUIModule().getGUINode(TDLM_ITEMS_TABLE_VIEW);
          for(int i = 1; i < data.getmapPane().getChildren().size(); i++) {
              if(data.getmapPane().getChildren().get(i) == p) {
                  ArrayList<RegioVincoItemPrototype> selected = data.getCurrentItemsOrder();
                  for(int j = 0; j < selected.size(); j++) {
                      if(i == j+1) {
                          select = selected.get(j);
                      }
                  }
              }
          }
          
            TableView.TableViewSelectionModel  itemsSelectionModel = tableView.getSelectionModel();
        itemsSelectionModel.setSelectionMode(SelectionMode.MULTIPLE);
          itemDialog.showViewEditDialog(select);  
          RegioVincoItemPrototype itemtoedit = itemDialog.getEditItem();
            if(itemtoedit != null) {
                EditItem_Transaction transaction = new EditItem_Transaction(app, select, itemtoedit);
                app.processTransaction(transaction);
        
      }
            
      }
    
    public void processMoveItemUp() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        if (data.isItemSelected()) {
            RegioVincoItemPrototype itemToMove = data.getSelectedItem();
            int oldIndex = data.getItemIndex(itemToMove);
          
            if (oldIndex > 0) {
//                  ObservableList<Polygon> old = data.getSubregion(oldIndex);
//           Polygon old1 = old.get(oldIndex);
//            Polygon old2 = old.get(oldIndex-1);
//            
                MoveItem_Transaction transaction = new MoveItem_Transaction(data, oldIndex, oldIndex-1);
                app.processTransaction(transaction);
                                  ObservableList<Polygon> oldp = data.getSubregion(oldIndex);
                                       ObservableList<Polygon> newp = data.getSubregion(oldIndex-1);
                                       data.returnSubregion().replace(newp, oldp);
                                        data.returnSubregion().replace(oldp, newp);
                // DESELECT THE OLD INDEX
                data.clearSelected();
                
                // AND SELECT THE MOVED ONE
                data.selectItem(itemToMove);
                
                // UPDATE BUTTONS
                app.getFoolproofModule().updateAll();
            }
        }
    }
    
    public void processMoveItemDown() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        if (data.isItemSelected()) {
            RegioVincoItemPrototype itemToMove = data.getSelectedItem();
            int oldIndex = data.getItemIndex(itemToMove);
            if (oldIndex < (data.getNumItems()-1)) {
                MoveItem_Transaction transaction = new MoveItem_Transaction(data, oldIndex, oldIndex+1);
                app.processTransaction(transaction);
              
                // DESELECT THE OLD INDEX
                data.clearSelected();
                
                // AND SELECT THE MOVED ONE
                data.selectItem(itemToMove);
                
                // UPDATE BUTTONS
                app.getFoolproofModule().updateAll();
            }
        }
    }
    
public void processAddImage(ImageView imv) {
     RegioVincoData data =  (RegioVincoData)app.getDataComponent();
       if(imv != null){
             AddImage_Transaction transaction = new AddImage_Transaction(data, imv);
            app.processTransaction(transaction);
       }
          
        else {
            AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), "Invalid Process", "No item was selected");
        }
}
     
     public void processRemoveImage(ImageView imv) {
         RegioVincoData data = (RegioVincoData)app.getDataComponent();
             RemoveImage_Transaction transaction = new RemoveImage_Transaction(data, imv);
             app.processTransaction(transaction);
         
         
     }
     
     public void processImageMove(ImageView imv, double ax, double ay, double fx, double fy) {
         RegioVincoData data = (RegioVincoData)app.getDataComponent();
                  MoveImage_Transaction transaction = new MoveImage_Transaction(data,ax, ay, fx, fy,imv);
              app.processTransaction(transaction);
//              app.getFoolproofModule().updateAll();
          }
     
    
     public void processBottomLeftTransaction(ImageView imv, double x, double y) {
          RegioVincoData data = (RegioVincoData)app.getDataComponent();
                  SnapLeftBottom_Transaction transaction = new SnapLeftBottom_Transaction(data, imv,x ,y);
             app.processTransaction(transaction);
//             app.getFoolproofModule().updateAll();
           }
     
     
     public void processTopLeftTransaction(ImageView imv, double x, double y) {
          RegioVincoData data = (RegioVincoData)app.getDataComponent();
                  SnapTopLeft_Transaction transaction = new SnapTopLeft_Transaction(data, imv,x ,y);
             app.processTransaction(transaction);
//             app.getFoolproofModule().updateAll();
           }
     
     
//     public void processFitToPolygon() {
//          RegioVincoData data = (RegioVincoData)app.getDataComponent();
//          data.fitToRegion();
//     }
//     
     public void processResetViewport() {
         RegioVincoData data = (RegioVincoData)app.getDataComponent();
         data.resetViewport();
     }
    
    public void processchange() {
        changedialog.showDimensionChangeDialog();
    }
    
   
    public void processBorderChange(double initthick, double initold) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        ChangeBorderThickness_Transaction transaction = new ChangeBorderThickness_Transaction(app, data, initthick,initold);
        app.processTransaction(transaction);
    }
    
    public void processBorderColor(Color initnex, Color initold) {
         RegioVincoData data = (RegioVincoData)app.getDataComponent();
        ChangeBorderColor_Transaction transaction = new ChangeBorderColor_Transaction(app, data, initnex,initold);
        app.processTransaction(transaction);
    }
 
    public void processRandomColor(ArrayList<Color> newplace, ArrayList<Color> oldplace) {
       RegioVincoData data = (RegioVincoData)app.getDataComponent();
       RandomColor_Transaction transaction = new RandomColor_Transaction(data, newplace, oldplace);
        app.processTransaction(transaction);
//        System.out.println(data.getold().size());
    }
    
     public void processProp(double fangle, double fdistance, double cx, double cy, double r, boolean p, CycleMethod cm, Stop[] s, RadialGradient rg) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        this.addGradient(fangle, fdistance, cx, cy, r, p, cm, s, rg);
        Rectangle oc = (Rectangle) data.getmapPane().getChildren().get(0);
        RadialGradient newrg = new RadialGradient(fangle, fdistance, cx, cy, r, p, cm, s);
        RadialGradient old = (RadialGradient) oc.getFill();
        RG_Transaction transaction = new RG_Transaction(app,data, newrg, old);
        app.processTransaction(transaction);
    }
     
      public void processFocusAngle(double fangle, double fdistance, double cx, double cy, double r, boolean p, CycleMethod cm, Stop[] s, RadialGradient rg) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        this.addGradient(fangle, fdistance, cx, cy, r, p, cm, s, rg);
        Rectangle oc = (Rectangle) data.getmapPane().getChildren().get(0);
        RadialGradient newrg = new RadialGradient(fangle, fdistance, cx, cy, r, p, cm, s);
        RadialGradient old = (RadialGradient) oc.getFill();
        RG_Transaction transaction = new RG_Transaction(app,data, newrg, old);
        app.processTransaction(transaction);
    }
      
      public void processFocusDistance(double fangle, double fdistance, double cx, double cy, double r, boolean p, CycleMethod cm, Stop[] s, RadialGradient rg) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        this.addGradient(fangle, fdistance, cx, cy, r, p, cm, s, rg);
        Rectangle oc = (Rectangle) data.getmapPane().getChildren().get(0);
        RadialGradient newrg = new RadialGradient(fangle, fdistance, cx, cy, r, p, cm, s);
        RadialGradient old = (RadialGradient) oc.getFill();
        RG_Transaction transaction = new RG_Transaction(app,data, newrg, old);
        app.processTransaction(transaction);
    }
      
      public void processCenterX(double fangle, double fdistance, double cx, double cy, double r, boolean p, CycleMethod cm, Stop[] s, RadialGradient rg) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        this.addGradient(fangle, fdistance, cx, cy, r, p, cm, s, rg);
        Rectangle oc = (Rectangle) data.getmapPane().getChildren().get(0);
        RadialGradient newrg = new RadialGradient(fangle, fdistance, cx, cy, r, p, cm, s);
        RadialGradient old = (RadialGradient) oc.getFill();
        RG_Transaction transaction = new RG_Transaction(app,data, newrg, old);
        app.processTransaction(transaction);
    }
      
     public void processCenterY(double fangle, double fdistance, double cx, double cy, double r, boolean p, CycleMethod cm, Stop[] s, RadialGradient rg) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        this.addGradient(fangle, fdistance, cx, cy, r, p, cm, s, rg);
        Rectangle oc = (Rectangle) data.getmapPane().getChildren().get(0);
        RadialGradient newrg = new RadialGradient(fangle, fdistance, cx, cy, r, p, cm, s);
        RadialGradient old = (RadialGradient) oc.getFill();
        RG_Transaction transaction = new RG_Transaction(app,data, newrg, old);
        app.processTransaction(transaction);
    }
     
      public void processRadius(double fangle, double fdistance, double cx, double cy, double r, boolean p, CycleMethod cm, Stop[] s, RadialGradient rg) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        this.addGradient(fangle, fdistance, cx, cy, r, p, cm, s, rg);
        Rectangle oc = (Rectangle) data.getmapPane().getChildren().get(0);
        RadialGradient newrg = new RadialGradient(fangle, fdistance, cx, cy, r, p, cm, s);
        RadialGradient old = (RadialGradient) oc.getFill();
        RG_Transaction transaction = new RG_Transaction(app,data, newrg, old);
        app.processTransaction(transaction);
    }
     
       public void processCombo(double fangle, double fdistance, double cx, double cy, double r, boolean p, CycleMethod cm, Stop[] s, RadialGradient rg) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        this.addGradient(fangle, fdistance, cx, cy, r, p, cm, s, rg);
        Rectangle oc = (Rectangle) data.getmapPane().getChildren().get(0);
        RadialGradient newrg = new RadialGradient(fangle, fdistance, cx, cy, r, p, cm, s);
        RadialGradient old = (RadialGradient) oc.getFill();
        RG_Transaction transaction = new RG_Transaction(app,data, newrg, old);
        app.processTransaction(transaction);
    }
      
        public void processStep0(double fangle, double fdistance, double cx, double cy, double r, boolean p, CycleMethod cm, Stop s1, Stop s2, RadialGradient rg) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        Stop[]s = new Stop[2];
        s[1] = s1;
        s[2] = s2;
        this.addGradient(fangle, fdistance, cx, cy, r, p, cm, s, rg);
        Rectangle oc = (Rectangle) data.getmapPane().getChildren().get(0);
        RadialGradient newrg = new RadialGradient(fangle, fdistance, cx, cy, r, p, cm, s);
        RadialGradient old = (RadialGradient) oc.getFill();
        RG_Transaction transaction = new RG_Transaction(app,data, newrg, old);
        app.processTransaction(transaction);
    }
        
          public void processStep1(double fangle, double fdistance, double cx, double cy, double r, boolean p, CycleMethod cm, Stop s1, Stop s2, RadialGradient rg) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        Stop[]s = new Stop[2];
        s[1] = s1;
        s[2] = s2;
        this.addGradient(fangle, fdistance, cx, cy, r, p, cm, s, rg);
        Rectangle oc = (Rectangle) data.getmapPane().getChildren().get(0);
        RadialGradient newrg = new RadialGradient(fangle, fdistance, cx, cy, r, p, cm, s);
        RadialGradient old = (RadialGradient) oc.getFill();
        RG_Transaction transaction = new RG_Transaction(app,data, newrg, old);
        app.processTransaction(transaction);
    }
    
    
    public void processFitToRegion(double sx, double sy, double ax, double ay) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        FitToRegions_Transaction transaction = new FitToRegions_Transaction(data, sx, sy, ax, ay);
         app.processTransaction(transaction);
    }
    
    public void processResetViewport(double sx, double sy, double ax, double ay) {
         RegioVincoData data = (RegioVincoData)app.getDataComponent();
        ResetViewport_Transaction transaction = new ResetViewport_Transaction(data, sx, sy, ax, ay);
         app.processTransaction(transaction);
    }

    /**
     * This helper method asks the user for a file to open. The user-selected
     * file is then loaded and the GUI updated. Note that if the user cancels
     * the open process, nothing is done. If an error occurs loading the file, a
     * message is displayed, but nothing changes.
     * @return 
     */
//    private void promptToLoadImage() {
//	       
//               BufferedImage img = null;
//               try {
//                   img = ImageIO.read(new File(AppTemplate.PATH_IMAGE));
//               } catch(IOException e) {
//                   e.printStackTrace();
//               }
//               JLabel lbl = new JLabel();
//               lbl.setIcon(new ImageIcon(img));
//               
//    }
    
    
    
//    public String getPath() {
//        String filepath = null;
//             final JFileChooser fc = new JFileChooser();
//    int retre = fc.showOpenDialog(fc);
//    
//        if(retre == JFileChooser.APPROVE_OPTION) {
//        filepath = fc.getSelectedFile().getAbsolutePath();
//        }
//        fc.hide();
//        return filepath;
//    }
    
    
    public ImageView getImageView() {
        return imv;
    }
    
//    public String getPath(File file) {
//        String base = PATH_IMAGE;
//        filepath = new File(base).toURI().relativize(new File(filepath).toURI()).getPath();
//        return filepath;
//    }
    
    public ArrayList<String> managePath() {
        return relatives;
      
    }
//   public void processfilepath() {
//       
//   }
// 


    public void processZoomIn() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.zoom(2.0);
        data.update(0, 0);
    }

    public void processZoomOut() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.zoom(1.0/2.0);
        data.update(0, 0);
    }
        
    public void processMoveLeft() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.move(moveInc, 0);
        data.update(0, 0);
    }

    public void processMoveRight() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.move(-moveInc, 0);
        data.update(0, 0);
    }

    public void processMoveUp() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.move(0, moveInc);
        data.update(0, 0);
    }

    public void processMoveDown() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.move(0, -moveInc);
        data.update(0, 0);
    }
    
   public void addGradient(double fangle, double fdistance, double cx, double cy, double r, boolean p, CycleMethod cm, Stop[] s, RadialGradient rg) {
        fangle = rg.getFocusAngle();
        fdistance = rg.getFocusDistance();
        cx = rg.getCenterX();
        cy = rg.getCenterY();
        r = rg.getRadius();
        p = rg.isProportional();
        cm = rg.getCycleMethod();
        for(int i =0; i < 2; i++) {
            s[i] = rg.getStops().get(i);
        }
    }
  
//    public void processRandomColor() {
//        RegioVincoData data = (RegioVincoData)app.getDataComponent();
//        data.setSubregionColor();
//    }
}
