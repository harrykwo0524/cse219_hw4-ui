package RV.workspace.dialogs;

import static RV.RegioVincoPropertyType.MV_MAP_PANE;
import static RV.RegioVincoPropertyType.TDLM_NAME_TEXT_FIELD;
import static RV.RegioVincoPropertyType.TDLM_OWNER_TEXT_FIELD;
import static RV.RegioVincoPropertyType.TDLM_REGION_NAME_TEXT_FIELD;
import static RV.RegioVincoPropertyType.TDLM_TITLE_LABEL;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_COLUMN;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_TEXT_FIELD;
import static djf.AppPropertyType.APP_BANNER;
import static djf.AppPropertyType.APP_LOGO;
import static djf.AppPropertyType.APP_PATH_IMAGES;
import static djf.AppPropertyType.APP_TITLE;
import static djf.AppPropertyType.LOAD_ERROR_CONTENT;
import static djf.AppPropertyType.LOAD_ERROR_TITLE;
import static djf.AppPropertyType.NEW_ERROR_CONTENT;
import static djf.AppPropertyType.NEW_ERROR_TITLE;
import static djf.AppPropertyType.NEW_SUCCESS_CONTENT;
import static djf.AppPropertyType.NEW_SUCCESS_TITLE;
import static djf.AppPropertyType.SAVE_WORK_TITLE;
import static djf.AppPropertyType.WELCOME_DIALOG_NEW_BUTTON_TEXT;
import static djf.AppPropertyType.WELCOME_DIALOG_NONE_LABEL;
import static djf.AppPropertyType.WELCOME_DIALOG_RECENT_WORK_LABEL;
import static djf.AppPropertyType.WELCOME_DIALOG_TITLE;
import djf.AppTemplate;
import static djf.modules.AppGUIModule.ENABLED;
import static djf.modules.AppGUIModule.FOCUS_TRAVERSABLE;
import static djf.modules.AppGUIModule.NO_KEY_HANDLER;
import djf.modules.AppRecentWorkModule;
import static djf.modules.AppLanguageModule.FILE_PROTOCOL;
import djf.ui.AppNodesBuilder;
import djf.ui.controllers.AppFileController;
import djf.ui.dialogs.AppDialogsFacade;
import static djf.ui.style.DJFStyle.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import properties_manager.PropertiesManager;

/**
 *
 * @author McKillaGorilla
 */
public class CreateNewDialog extends Stage {

    // LEFT PANE
    VBox recentlyEditedPane;
    Label recentWorkLabel;
    ArrayList<Button> recentWorkButtons;
    
    // RIGHT PANE
    VBox splashPane;
//    ImageView welcomeDialogImageView;
    HBox newPane;
    Button createNewButton;
    
    String selectedPath = null;
    String selectedWorkName = null;   
     
   String filename = null;   
    File directoryname = null;
    File name = null;
    
     
    public Label title; 
    public CreateNewDialog(AppTemplate app) {
//         String directory;   
    
        // GET THE RECENT WORK
        AppRecentWorkModule recentWork = app.getRecentWorkModule();
                 AppNodesBuilder tdlBuilder = app.getGUIModule().getNodesBuilder();
        // WE'LL NEED THIS
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        splashPane = new VBox();
        
        HBox titlepane = new HBox();
        Label titletext = new Label("Create New Map");
        titlepane.getChildren().add(titletext);
        
        HBox regionpane = new HBox();
        Label region = new Label("Region Name");
        TextField regiontextfield = tdlBuilder.buildTextField(TDLM_REGION_NAME_TEXT_FIELD, null, null, CLASS_TDLM_TEXT_FIELD, NO_KEY_HANDLER,       FOCUS_TRAVERSABLE,      ENABLED);
        regionpane.getChildren().add(region);
             regionpane.getChildren().add(regiontextfield);
     
        HBox directorypane = new HBox();
        Label directorylabel = new Label("Parent Region");
         String directorytextfield = new String("Choose Parent Region Directory");
         Button directoryButton = new Button(directorytextfield);
         directorypane.getChildren().add(directorylabel);
           directorypane.getChildren().add(directoryButton);

           
          HBox datapane = new HBox();
        Label datalabel = new Label("Data File");
         String datatextfield = new String("Choose Data File");
         Button dataButton = new Button(datatextfield);
        
         datapane.getChildren().add(datalabel);
           datapane.getChildren().add(dataButton);
           
           
           String ok = new String("Ok");
           Button okbutton = new Button(ok);
           
        
        newPane = new HBox();
        newPane.setAlignment(Pos.CENTER);


        splashPane.getChildren().add(titlepane);
        splashPane.getChildren().add(regionpane);
        splashPane.getChildren().add(directorypane);
        splashPane.getChildren().add(datapane);
        splashPane.getChildren().add(okbutton);
        splashPane.getChildren().add(newPane);
//         app.getFileModule().newWork();
       
       regiontextfield.setOnAction(e->{
          regiontextfield.setEditable(true);
           filename = regiontextfield.getText();
       });

directoryButton.setOnAction(e->{
    AppFileController filecontroller = new AppFileController(app);
    directoryname = AppDialogsFacade.showDirectoryDialog(this, NEW_SUCCESS_TITLE);
    if(directoryname != null) {
         Label datapath = new Label(directoryname.getPath());
                  directorypane.getChildren().add(datapath);
    }


});
       dataButton.setOnAction(e->{
        
           AppFileController filecontroller = new AppFileController(app);
           filecontroller.processImportRequest();
                 name = filecontroller.getFilename();
                 if(name != null) {
                      Label path = new Label(name.getPath());
           datapane.getChildren().add(path);
         
                 }
          
//               this.hide();
       });
       
       okbutton.setOnAction(e->{
              
//               AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), NEW_SUCCESS_TITLE, NEW_SUCCESS_CONTENT);
                AppFileController filecontroller = new AppFileController(app);
           
           if(datapane.getChildren().size() == 0 || directorypane.getChildren().size() == 0) {
               AppDialogsFacade.showMessageDialog(this, NEW_ERROR_TITLE, NEW_ERROR_CONTENT);
           }
           else {
         title =  (Label) app.getGUIModule().getGUINode(TDLM_TITLE_LABEL);
    String newname = title.toString();
    title.setText(regiontextfield.getText());
          newname = (regiontextfield.getText());
                app.getGUIModule().getWindow().setTitle(props.getProperty(APP_TITLE) + " - " + newname + ".json");
                filecontroller.processNewRequest();
                       // TELL THE USER NEW WORK IS UNDERWAY
                    AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), NEW_SUCCESS_TITLE, NEW_SUCCESS_CONTENT);
           }
            this.hide();
//            app.getGUIModule().getFileController().processNewRequest();
           });
        
        // WE ORGANIZE EVERYTHING IN HERE
        BorderPane dialogPane = new BorderPane();
        dialogPane.setLeft(recentlyEditedPane);
        dialogPane.setCenter(splashPane);

        // MAKE AND SET THE SCENE
        Scene dialogScene = new Scene(dialogPane);
        this.setScene(dialogScene);

        // PUT EVERYTHING IN THE DIALOG WINDOW
        String dialogTitle = props.getProperty(WELCOME_DIALOG_TITLE);
        this.setTitle(dialogTitle);
        
        // SET THE APP ICON
        String imagesPath = props.getProperty(APP_PATH_IMAGES);
        String appLogo = FILE_PROTOCOL + imagesPath + props.getProperty(APP_LOGO);
        Image appWindowLogo = new Image(appLogo);
        this.getIcons().add(appWindowLogo);

        // SPECIFY THE STYLE THE BANNER AND NEW BUTTON
        app.getGUIModule().initStylesheet(this);
       titlepane.getStyleClass().add(CLASS_DJF_WELCOME_HEADER);
       regionpane.getStyleClass().add(CLASS_DJF_WELCOME_NEW_PANE);
       directorypane.getStyleClass().add(CLASS_DJF_WELCOME_NEW_PANE);
       datapane.getStyleClass().add(CLASS_DJF_WELCOME_NEW_PANE);
        newPane.getStyleClass().add(CLASS_DJF_WELCOME_NEW_PANE);
       okbutton.getStyleClass().add(CLASS_DJF_WELCOME_NEW_BUTTON);
    }
    
    public Label getMapName() {
        return title;
    }
}