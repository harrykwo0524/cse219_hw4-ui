package tdlm.workspace.controllers;

import djf.ui.dialogs.AppDialogsFacade;
import java.time.LocalDate;
import java.util.ArrayList;
import javafx.scene.control.TableView;
import tdlm.ToDoListMakerApp;
import tdlm.data.ToDoData;
import tdlm.data.ToDoItemPrototype;
import tdlm.workspace.dialogs.ToDoListItemDialog;
import tdlm.transactions.AddItem_Transaction;
import tdlm.transactions.EditItem_Transaction;
import tdlm.transactions.MoveDown_Transaction;
import tdlm.transactions.MoveUp_Transaction;
import tdlm.transactions.RemoveItems_Transaction;

/**
 *
 * @author McKillaGorilla
 */
public class ItemsController {
    ToDoListMakerApp app;
    ToDoListItemDialog itemDialog;
    
    public ItemsController(ToDoListMakerApp initApp) {
        app = initApp;
        
        itemDialog = new ToDoListItemDialog(app);
    }
    
    public void processAddItem() {
        ToDoData data = (ToDoData)app.getDataComponent();
        itemDialog.showAddDialog();
        ToDoItemPrototype newItem = itemDialog.getNewItem();   
        if (newItem != null) {
            
            AddItem_Transaction transaction = new AddItem_Transaction(app,data, newItem);
            app.processTransaction(transaction);
            app.getFileModule().markAsEdited(true);
           
        }    
        
        else {
            
        }
    }
    
    public void processRemoveItems() {
        ToDoData data = (ToDoData)app.getDataComponent();
        if (data.isItemSelected() || data.areItemsSelected()) {
            ArrayList<ToDoItemPrototype> itemsToRemove = new ArrayList(data.getSelectedItems());
            RemoveItems_Transaction transaction = new RemoveItems_Transaction(app, itemsToRemove);
            app.processTransaction(transaction);
            app.getFileModule().markAsEdited(true);
        }
    }
    
    public void processEditItems() {
        ToDoData data = (ToDoData)app.getDataComponent();
        ToDoItemPrototype select = data.getSelectedItem();
        if(data.isItemSelected()) {
            itemDialog.showEditDialog(select);  
            ToDoItemPrototype itemtoedit = itemDialog.getEditItem();
            if(itemtoedit != null) {
                EditItem_Transaction transaction = new EditItem_Transaction(app, select, itemtoedit);
                app.processTransaction(transaction);
                app.getFileModule().markAsEdited(true);
            }
             if(data.getSelectedItem().getStartDate().isAfter(data.getSelectedItem().getEndDate())) {
            AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), "INVALID", "NOT DOABLE");
        }
        if(data.getSelectedItem().getEndDate().isBefore(data.getSelectedItem().getStartDate())) {
             AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), "INVALID", "NOT DOABLE");
        }
            else {
                
            }
           
        }
    }
    
    public void processMoveUpItem() {
         ToDoData data = (ToDoData)app.getDataComponent();
         if (data.isItemSelected() || data.areItemsSelected()) {
            ArrayList<ToDoItemPrototype> itemToMoveUp = new ArrayList(data.getSelectedItems());
            MoveUp_Transaction transaction = new MoveUp_Transaction(app, itemToMoveUp);
            app.processTransaction(transaction);
            app.getFileModule().markAsEdited(true);
        }
    }
    
    public void processMoveDownItem() {
          ToDoData data = (ToDoData)app.getDataComponent();
         if (data.isItemSelected() || data.areItemsSelected()) {
            ArrayList<ToDoItemPrototype> itemToMoveDown = new ArrayList(data.getSelectedItems());
            MoveDown_Transaction transaction = new MoveDown_Transaction(app, itemToMoveDown);
            app.processTransaction(transaction);
           app.getFileModule().markAsEdited(true);
        }
    }
    
//    public void processRedoItems() {
//        ToDoData data = (ToDoData)app.getDataComponent();
//        if (data.isItemSelected() || data.areItemsSelected()) {
//            ArrayList<ToDoItemPrototype> itemsToRedo = new ArrayList(data.getSelectedItems());
//            RedoItem_Transaction transaction = new RedoItems_Transaction(app, itemsToRedo);
//            app.processTransaction(transaction);
//        }
//    }```````````````
//    
//    public void processUndoItems() {
//        ToDoData data = (ToDoData)app.getDataComponent();
//        if (data.isItemSelected() || data.areItemsSelected()) {
//            ArrayList<ToDoItemPrototype> itemsToUndo = new ArrayList(data.getSelectedItems());
//            UndoItem_Transaction transaction = new UndoItems_Transaction(app, itemsToUndo);
//            app.processTransaction(transaction);
//        }
//    }
}
