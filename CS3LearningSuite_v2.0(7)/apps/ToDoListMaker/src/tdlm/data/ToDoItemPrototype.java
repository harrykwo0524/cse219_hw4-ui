package tdlm.data;

import java.time.LocalDate;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author McKillaGorilla
 */
public class ToDoItemPrototype implements Cloneable {
    public static final String DEFAULT_CATEGORY = "?";
    public static final String DEFAULT_DESCRIPTION = "?";
    public static final LocalDate DEFAULT_DATE = LocalDate.now();
     public static final String DEFAULT_ASSIGNED_TO = "?";
//    public static final LocalDate DEFAULT_FINAL_DATE = LocalDate.from(DEFAULT_DATE);
    public static final boolean DEFAULT_COMPLETED = false;
    
    
    final StringProperty category;
    final StringProperty description;
    final ObjectProperty<LocalDate> startDate;
     final ObjectProperty<LocalDate> endDate;
     final StringProperty assigned_to;
    final BooleanProperty completed;
       
    public ToDoItemPrototype() {
        category = new SimpleStringProperty(DEFAULT_CATEGORY);
        description = new SimpleStringProperty(DEFAULT_DESCRIPTION);
        startDate = new SimpleObjectProperty(DEFAULT_DATE);
        endDate = new SimpleObjectProperty(DEFAULT_DATE);
        assigned_to = new SimpleStringProperty(DEFAULT_ASSIGNED_TO);
        completed = new SimpleBooleanProperty(DEFAULT_COMPLETED);
    }

    public ToDoItemPrototype(String initCategory, String initDescription, LocalDate initStartDate, LocalDate initEndDate, String initassignedTo, boolean initCompleted) {
        this();
        category.set(initCategory);
        description.set(initDescription);
        startDate.set(initStartDate);
          endDate.set(initEndDate);
          assigned_to.set(initassignedTo);
        completed.set(initCompleted);
    }

    public String getCategory() {
        return category.get();
    }

    public void setCategory(String value) {
        category.set(value);
    }

    public StringProperty categoryProperty() {
        return category;
    }

    public String getDescription() {
        return description.get();
    }

    public void setDescription(String value) {
        description.set(value);
    }

    public StringProperty descriptionProperty() {
        return description;
    }

    public LocalDate getStartDate() {
        return startDate.get();
    }

    public void setStartDate(LocalDate value) {
        startDate.set(value);
    }

    public ObjectProperty startDateProperty() {
        return startDate;
    }
    
     public LocalDate getEndDate() {
        return endDate.get();
    }

    public void setEndDate(LocalDate value) {
        endDate.set(value);
    }

    public ObjectProperty endDateProperty() {
        return endDate;
    }

    public String getAssignedTo() {
        return assigned_to.get();
    }

    public void setAssignedTo(String value) {
        assigned_to.set(value);
    }

    public StringProperty assignedToProperty() {
        return assigned_to;
    }

    public boolean isCompleted() {
        return completed.get();
    }

    public void setCompleted(boolean value) {
        completed.set(value);
    }

    public BooleanProperty completedProperty() {
        return completed;
    }
    
    public void reset() {
        setCategory(DEFAULT_CATEGORY);
        setDescription(DEFAULT_DESCRIPTION);
        setStartDate(DEFAULT_DATE);
        setEndDate(DEFAULT_DATE);
        setAssignedTo(DEFAULT_ASSIGNED_TO);
        setCompleted(DEFAULT_COMPLETED);
    }

    public Object clone() {
        return new ToDoItemPrototype(   category.getValue(), 
                                        description.getValue(), 
                                        startDate.getValue(), 
                                        endDate.getValue(),
                                        assigned_to.getValue(),
                                        completed.getValue());
    }
    
    public boolean equals(Object obj) {
        return this == obj;
    }
    
  
}