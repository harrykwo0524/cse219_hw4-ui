package RV.data;

import djf.components.AppDataComponent;
import djf.modules.AppGUIModule;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Bounds;
import javafx.scene.Cursor;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableView;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import RV.RegioVincoListMakerApp;
import static RV.RegioVincoPropertyType.CENTER_X;
import static RV.RegioVincoPropertyType.CENTER_Y;
import static RV.RegioVincoPropertyType.COMBOBOX;
import static RV.RegioVincoPropertyType.FOCUS_ANGLE;
import static RV.RegioVincoPropertyType.FOCUS_DISTANCE;
import static RV.RegioVincoPropertyType.MV_MAP_NAME;
import static RV.RegioVincoPropertyType.MV_MAP_PANE;
import static RV.RegioVincoPropertyType.MV_OUTER_MAP_PANE;
import static RV.RegioVincoPropertyType.PROP;
import static RV.RegioVincoPropertyType.RADIUS;
import static RV.RegioVincoPropertyType.STEP_0_COLOR;
import static RV.RegioVincoPropertyType.STEP_1_COLOR;
import static RV.RegioVincoPropertyType.TDLM_ITEMS_TABLE_VIEW;
import static RV.RegioVincoPropertyType.TDLM_NAME_TEXT_FIELD;
import static RV.RegioVincoPropertyType.TDLM_OWNER_TEXT_FIELD;
import static RV.RegioVincoPropertyType.TDLM_REGION_NAME_TEXT_FIELD;
import static RV.data.RegioVincoData.ImageState.IMAGE_DRAG;
import static RV.data.RegioVincoData.ImageState.IMAGE_SELECT;
import static RV.data.RegioVincoData.MapState.MAP_DRAG;
import static RV.data.RegioVincoData.MapState.MAP_SELECT;
//import static RV.data.RegioVincoData.MapState.OUTERMAP_DRAG;
//import static RV.data.RegioVincoData.MapState.OUTERMAP_SELECT;
import RV.workspace.controllers.ItemsController;
import static RV.workspace.style.TDLStyle.CLASS_MV_MAP_LAND;
import static RV.workspace.style.TDLStyle.CLASS_MV_MAP_MOUSE_OVER_LAND;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Insets;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Slider;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;

/**
 *
 * @author McKillaGorilla
 */
public class RegioVincoData implements AppDataComponent {
    RegioVincoListMakerApp app;
    ObservableList<RegioVincoItemPrototype> items;
    TableViewSelectionModel itemsSelectionModel;
//    StringProperty nameProperty;
//    StringProperty ownerProperty;
    Pane map;
    Pane outermap;
    String mapname;
    int subregionId;
    HashMap<Integer, ObservableList<Polygon>> subregions;
    WebEngine statsEngine;
    MapState currentState;
    ImageState currentimageState;
    final double DEFAULT_LINE_THICKNESS = 1.0;
    double tX, tY, sX, sY;
 double positionx;
  double positiony;
double size;
public int imagenumber;
ItemsController itemcontroller;
public int rd;
public double border;
// public static ArrayList<ArrayList<Color>> colorplace;
 public ArrayList<Color> oldplace;
 public ImageView imv;
 public String filepath;
    public String[] paths;
      public   ArrayList<String> relatives;
    public  String relative;
    public  int length;
    public ArrayList<Color> firstcolor;
    public ArrayList<ImageView> returnimagelist;
    public ImageView newiv;
    
   public void fitToRegion() {
            
        double maxx = 0;
             double minx = 10000;
             double maxy = 0;
             double miny = 10000;
         for(int i = 1; i < map.getChildren().size(); i++){
              Polygon p = (Polygon)(map.getChildren().get(i));
             for(int k = 0; k < p.getPoints().size(); k++) {
                 double comp = p.getPoints().get(k);
                 if(k % 2 == 0 && minx >= comp) {
                         minx = comp;
                 }
                     else if(k % 2 == 0 && maxx <= comp) {
                         maxx = comp;
                     }
                 
                 else if(k % 2 == 1 && miny >= comp) {
                    
                         miny = comp;
                     }
                     else if(k % 2 == 1 && maxy <= comp) {
                         maxy = comp;
                     }
                      
                 }
             }

        double newmapwidth = (maxx - minx);
             double newmapheight = (maxy - miny);
             if(newmapwidth < newmapheight) {
                 double scaleheight = map.getHeight() * 0.8 / newmapheight;
                 map.setScaleX(scaleheight);
                  map.setScaleY(scaleheight);
//                  mapPane.setTranslate(scaleheight);
//                  mapPane.setY(scaleheight);
                  map.setTranslateX((map.getWidth()/2 - (minx + newmapwidth/2)) * scaleheight);
                 map.setTranslateY((map.getHeight()/2 - (miny + newmapheight/2)) * scaleheight);
             }
             else {
                 double scalewidth = map.getWidth() * 0.8 / newmapwidth;
                 map.setScaleX(scalewidth);
                  map.setScaleY(scalewidth);
//                   mapPane.setX(scalewidth);
//                  mapPane.setY(scalewidth);
                  map.setTranslateX((map.getWidth()/2 - (minx + newmapwidth/2)) * scalewidth);
                 map.setTranslateY((map.getHeight()/2 - (miny + newmapheight/2)) * scalewidth);
//           this.zoomOnPoint(scalewidth, (map.getWidth()/2 - (minx + newmapwidth/2)) * scalewidth, (map.getHeight()/2 - (miny + newmapheight/2)) * scalewidth);
             }
    }

    enum MapState{
        MAP_SELECT,
        MAP_DRAG
    }
    enum ImageState{
        IMAGE_SELECT,
        IMAGE_DRAG
    }

    public RegioVincoData(RegioVincoListMakerApp initApp) {
app = initApp;
        subregions = new HashMap();
        map = (Pane)app.getGUIModule().getGUINode(MV_MAP_PANE);
        outermap = (Pane)app.getGUIModule().getGUINode(MV_OUTER_MAP_PANE);
        currentState = MapState.MAP_SELECT;
        currentimageState = ImageState.IMAGE_SELECT;

itemcontroller = new ItemsController(app);
//     colorplace = new ArrayList<>();
      oldplace = new ArrayList<>();
        // GET ALL THE THINGS WE'LL NEED TO MANIUPLATE THE TABLE
        TableView tableView = (TableView) app.getGUIModule().getGUINode(TDLM_ITEMS_TABLE_VIEW);
        items = tableView.getItems();
        itemsSelectionModel = tableView.getSelectionModel();
        itemsSelectionModel.setSelectionMode(SelectionMode.MULTIPLE);relatives= new ArrayList();
        firstcolor = new ArrayList<>();
        returnimagelist = new ArrayList<>();
        relatives = new ArrayList<>();
border = 0;
    }
    
    public Polygon getMousedOverPolygon(double x, double y) {
        Iterator<Integer> idIt = subregions.keySet().iterator();
        while (idIt.hasNext()) {
            int id = idIt.next();
            ObservableList<Polygon> polygons = subregions.get(id);            
            Iterator<Polygon> polyIt = polygons.iterator();
            while (polyIt.hasNext()) {
                Polygon poly = polyIt.next();
                double localX = x - poly.getLayoutX();
                double localY = y - poly.getLayoutY();
                System.out.println("localX, localY: (" + localX + ", " + localY);
                if (poly.contains(x, y)) {
                    System.out.println("poly found");
                    return poly;
                }
            }
        }
        return null;
    }
    
    public double calcXPerc(double x) {
        double scale = map.scaleXProperty().doubleValue();
        double translateX = map.translateXProperty().doubleValue();
        double xMax = (scale-1.0)*(map.getWidth()/2.0);
        double leftX = (xMax - translateX)/scale;
        double percentX = (x-leftX)/(map.getWidth()/scale);
        return percentX;        
    }
    
    public double calcYPerc(double y) {
        double scale = map.scaleXProperty().doubleValue();
        double translateY = map.translateYProperty().doubleValue();
        double yMax = (scale-1.0)*(map.getWidth()/2.0);
        double leftY = (yMax - translateY)/scale;
        double percentY = (y-leftY)/(map.getHeight()/scale);
        return percentY;        
    }
    
    public ObservableList<Polygon> getSubregion(int id) {
        return subregions.get(id);
    }
    
    public HashMap returnSubregion() {
        return subregions;
    }

    @Override
    public void reset() {
        // CLEAR THE DATA
        subregions.clear();
        subregionId = 0;
        
        // AND THE POLYGONS THEMSELVES
          // AND THE POLYGONS THEMSELVES
        Rectangle ocean = (Rectangle)map.getChildren().get(0);
        map.getChildren().clear();
        map.getChildren().add(ocean);
        for(int i =1; i < outermap.getChildren().size(); i++) {
            outermap.getChildren().remove(i);
        }
    }

    public void addSubregion(ArrayList<ArrayList<Double>> rawPolygons) {
        ObservableList<Polygon> subregionPolygons = FXCollections.observableArrayList();
        Color randomColor = null;
        for (int i = 0; i < rawPolygons.size(); i++) {
            ArrayList<Double> rawPolygonPoints = rawPolygons.get(i);
            Polygon polygonToAdd = new Polygon();
            ObservableList<Double> transformedPolygonPoints = polygonToAdd.getPoints();
            
//              Random rand = new Random();
//                rd = rand.nextInt(255);
//                if(rd == 0) {
//                      rd = rd+1;
//                }
//                else if(rd == 255) {
//                    rd = rd-1;
//                }
//                     randomColor = Color.rgb(rd, rd, rd);
//             polygonToAdd.setFill(randomColor);
//             
//             
            for (int j = 0; j < rawPolygonPoints.size(); j+=2) {
                double longX = rawPolygonPoints.get(j);
                double latY = rawPolygonPoints.get(j+1);
                double x = longToX(longX);
                double y = latToY(latY);
                transformedPolygonPoints.addAll(x, y);
            }
            subregionPolygons.add(polygonToAdd);

                for(int k = 1; k < map.getChildren().size(); k++) {
                Random rand = new Random();
                int r = rand.nextInt(255);
                if(r == 0) {
                    r = r+1;
                }
                else if(r == 255){
                    r = r-1;
                }
                Color color = Color.rgb(r, r, r);
                polygonToAdd.setFill(color);
                firstcolor.add(color);
                }
            polygonToAdd.setStroke(Color.BLACK);
            polygonToAdd.setStrokeWidth(0);
            polygonToAdd.setUserData(subregionId);
            map.getChildren().add(polygonToAdd);
             polygonToAdd.setOnMouseMoved(e-> {
                positionx = e.getX();
                positiony = e.getY();
                Polygon poly = this.getMousedOverPolygon(positionx, positiony);
                if (poly != null) {
            if (mousedOverPolygon != null) {         
                mousedOverPolygon.getStyleClass().remove(CLASS_MV_MAP_MOUSE_OVER_LAND);
//                mousedOverPolygon.getStyleClass().add(CLASS_MV_MAP_LAND);
            }
            mousedOverPolygon = poly;
//            mousedOverPolygon.getStyleClass().remove(CLASS_MV_MAP_LAND);
            mousedOverPolygon.getStyleClass().add(CLASS_MV_MAP_MOUSE_OVER_LAND);
        }
        else if (mousedOverPolygon != null) { 
            mousedOverPolygon.getStyleClass().remove(CLASS_MV_MAP_MOUSE_OVER_LAND);
//            mousedOverPolygon.getStyleClass().add(CLASS_MV_MAP_LAND);
            mousedOverPolygon = null;    
        }
            });
             polygonToAdd.setOnMouseExited(e-> {
                   positionx = e.getX();
                positiony = e.getY();
                Polygon poly = this.getMousedOverPolygon(positionx, positiony);
                if (poly != null) {
            if (mousedOverPolygon != null) {         
                mousedOverPolygon.getStyleClass().remove(CLASS_MV_MAP_MOUSE_OVER_LAND);
//                mousedOverPolygon.getStyleClass().add(CLASS_MV_MAP_LAND);
            }
            mousedOverPolygon = poly;
//            mousedOverPolygon.getStyleClass().remove(CLASS_MV_MAP_LAND);
            mousedOverPolygon.getStyleClass().add(CLASS_MV_MAP_MOUSE_OVER_LAND);
        }
        else if (mousedOverPolygon != null) { 
            mousedOverPolygon.getStyleClass().remove(CLASS_MV_MAP_MOUSE_OVER_LAND);
//            mousedOverPolygon.getStyleClass().add(CLASS_MV_MAP_LAND);
            mousedOverPolygon = null;    
        }
             });
        }


        subregions.put(subregionId, subregionPolygons);
        subregionId++;    
       
    }

    /**
     * This calculates and returns the x pixel value that corresponds to the
     * xCoord longitude argument.
     */
    public double longToX(double longCoord) {
        double paneHeight = map.getHeight();
        double unitDegree = paneHeight/180;
        double newLongCoord = (longCoord + 180) * unitDegree;
        return newLongCoord;
    }

    /**
     * This calculates and returns the y pixel value that corresponds to the
     * yCoord latitude argument.
     */
    public double latToY(double latCoord) {
        // DEFAULT WILL SCALE TO THE HEIGHT OF THE MAP PANE
        double paneHeight = map.getHeight();
        
        // WE ONLY WANT POSITIVE COORDINATES, SO SHIFT BY 90
        double unitDegree = paneHeight/180;
        double newLatCoord = (latCoord + 90) * unitDegree;
        return paneHeight - newLatCoord;
    }

    double startX;
    double startY;
     double imagestartx;
    double imagestarty;
    double finalx;
    double finaly;
    
    public void resetLocation() {
        map.translateXProperty().set(0);
        map.translateYProperty().set(0);
    }    

    public void resetViewport() {
        scaleMap(1.0);
        moveMap(0, 0);
    }

    private void scaleMap(double zoomScale) {
        map.setScaleX(zoomScale);
        map.setScaleY(zoomScale);
    }
    private void moveMap(double x, double y) {
        map.setTranslateX(x);
        map.setTranslateY(y);
        clamp();
    }

    public void zoom(double zoomInc) {
        double scale = map.scaleXProperty().doubleValue();
        double translateX = map.translateXProperty().doubleValue();
        double translateY = map.translateYProperty().doubleValue();
        if ((scale * zoomInc) >= 1.0) {
            scale *= zoomInc;
            moveMap(0,0);
            scaleMap(scale);
            translateX *= zoomInc;
            translateY *= zoomInc;
            moveMap(translateX, translateY);
            clamp();
            adjustBorder(border);
        }
    }

    public void clamp() {
        // FIRST CLAMP X
        double scale = map.scaleXProperty().doubleValue();
        double xMax = (scale-1.0)*(map.getWidth()/2.0);
        double xTranslate = map.translateXProperty().doubleValue();
        if (xTranslate > xMax) xTranslate = xMax;
        //else if (xTranslate < adjustedX) xTranslate = adjustedX;
        map.translateXProperty().setValue(xTranslate);

        // THEN Y
        double yMax = (scale-1.0)*(map.getHeight()/2.0);
        double yTranslate = map.translateYProperty().doubleValue();
        if (yTranslate > yMax) yTranslate = yMax;
        map.translateYProperty().setValue(yTranslate);
    }
    
    
    
    public void adjustLineThickness() {
        double scale = map.scaleXProperty().doubleValue();
        Iterator<Integer> idIt = subregions.keySet().iterator();
        while (idIt.hasNext()) {
            int id = idIt.next();
            ObservableList<Polygon> polygons = subregions.get(id);
            Iterator<Polygon> polyIt = polygons.iterator();
            while (polyIt.hasNext()) {
                Polygon poly = polyIt.next();
                poly.setStrokeWidth(DEFAULT_LINE_THICKNESS/scale);
            }
        }
    }
    
    

    public void move(double xInc, double yInc) {
        // FIRST X, WITH CLAMPING AT THE EDGES
        double xTranslate = map.translateXProperty().doubleValue() + xInc;
        map.translateXProperty().setValue(xTranslate);

        double yTranslate = map.translateYProperty().doubleValue() + yInc;
        map.translateYProperty().setValue(yTranslate);
        
        // MAKE SURE WE'RE NOT OUT OF BOUNDS
        clamp();
    }
    
    public void imagemove(double x, double y, ImageView imv) {
        double xTranslate = imv.translateXProperty().doubleValue() + x;
        imv.translateXProperty().setValue(xTranslate);

        double yTranslate = imv.translateYProperty().doubleValue() + y;
        imv.translateYProperty().setValue(yTranslate);
        
        // MAKE SURE WE'RE NOT OUT OF BOUNDS
        clamp();
    }

    public double xToScaledX(double x) {
        double scale = map.scaleXProperty().doubleValue();
        double translateX = map.translateXProperty().doubleValue();
        double diffX = x - translateX;        
        return diffX/scale;
    }
    
    public double yToScaledY(double y) {
        double scale = map.scaleXProperty().doubleValue();
        double translateY = map.translateYProperty().doubleValue();
        double diffY = y - translateY;
        return diffY/scale;
    }
    
    public void startMapDrag(int x, int y) {
        startX = x;
        startY = y;
        currentState = MAP_DRAG;
        app.getGUIModule().getWindow().getScene().setCursor(Cursor.MOVE);
        
    }

    public void updateMapDrag(double x, double y) {
        if (currentState == MAP_DRAG) {
            double diffX = x - startX;
            double diffY = y - startY;
            this.move(diffX, diffY);
        }        
    }

    
    public double getScale() {
        return map.getScaleX();
    }
    public void endMapDrag(int x, int y) {
        currentState = MAP_SELECT;
        app.getGUIModule().getWindow().getScene().setCursor(Cursor.DEFAULT);
    }
    
    public void ImagestartMapDrag(double x, double y) {
        imagestartx = x;
        imagestarty = y;
        currentimageState = IMAGE_DRAG;
        app.getGUIModule().getWindow().getScene().setCursor(Cursor.MOVE);
        
    }

     public void ImageupdateMapDrag(double x, double y, ImageView imv) {
        if (currentimageState == IMAGE_DRAG) {
            double diffX = x - imagestartx;
            double diffY = y - imagestarty;
            this.imagemove(diffX, diffY, imv);
        }        
    }
     
        public void ImageendMapDrag(int x, int y) {
            
        currentimageState = IMAGE_SELECT;
        app.getGUIModule().getWindow().getScene().setCursor(Cursor.DEFAULT);
    }
    
    public void zoomInOnPoint(double x, double y) {
        zoomOnPoint(2.0, x, y);
    }

    public void zoomOutOnPoint(double x, double y) {
        zoomOnPoint(0.5, x, y);
    }

    private void zoomOnPoint(double factor, double mouseX, double mouseY) {
        double scale = map.scaleXProperty().doubleValue();
        double newScale = factor * scale;
        if (newScale >= 1.0) {
            updateStats(mouseX, mouseY);
            double diffX =(viewportMousePercentX * (viewportWidth/newScale));
            double diffY = (viewportMousePercentY * (viewportHeight/newScale));
            double newWorldViewportX = worldMouseX - diffX;
            double newWorldViewportY = worldMouseY - diffY;
            double newWorldViewportPaddingLeft = (newScale-1.0)*(viewportWidth/2.0);
            double newWorldViewportPaddingTop = (newScale-1.0)*(viewportHeight/2.0);
            viewportTranslateX = newWorldViewportPaddingLeft - (newWorldViewportX * newScale);
            viewportTranslateY = newWorldViewportPaddingTop - (newWorldViewportY * newScale);

            zoom(factor);
            map.translateXProperty().setValue(viewportTranslateX);
            map.translateYProperty().setValue(viewportTranslateY);
            this.update(mouseX, mouseY);
        }
    }

//    public String buildPercRow(String label, double perc) {
//        NumberFormat percFormat = NumberFormat.getPercentInstance();
//        return      "   <tr>\n"
//                +   "    <td class='stats_prompt'>" + label + "</td>\n"
//                +   "    <td class='stats_data'>" + percFormat.format(perc) + "</td>\n"
//                +   "   </tr>\n";   
//    }
//    public String buildStatsRow(String label, double stat) {
//        return      "   <tr>\n"
//                +   "    <td class='stats_prompt'>" + label + "</td>\n"
//                +   "    <td class='stats_data'>" + stat + "</td>\n"
//                +   "   </tr>\n";   
//    }
//    final String STATS_PREFIX 
//                            =   "<html>\n"
//                            +   " <head>\n"
//                            +   "  <style>\n"
//                            +   "   .stats_prompt {\n"
//                            +   "       font-size:16pt;\n"
//                            +   "       font-weight:bold;\n"
//                            +   "   }\n"
//                            +   "   .stats_data {\n"
//                            +   "       font-size:16pt;\n"
//                            +   "   }\n"
//                            +   "  </style>\n"
//                            +   " </head>\n"
//                            +   " <body>\n"
//                            +   "  <table>\n";
//    final String STATS_SUFFIX = "  </table>\n"
//                            +   " </body>\n"
//                            +   "</html>";
    Polygon mousedOverPolygon;
    
    public void highlightPolygon(double mouseX, double mouseY) {        Polygon poly = this.getMousedOverPolygon(mouseX, mouseY);
        if (poly != null) {
            if (mousedOverPolygon != null) {         
                mousedOverPolygon.getStyleClass().remove(CLASS_MV_MAP_MOUSE_OVER_LAND);
//                mousedOverPolygon.getStyleClass().add(CLASS_MV_MAP_LAND);
            }
            mousedOverPolygon = poly;
//            mousedOverPolygon.getStyleClass().remove(CLASS_MV_MAP_LAND);
            mousedOverPolygon.getStyleClass().add(CLASS_MV_MAP_MOUSE_OVER_LAND);
        }
        else if (mousedOverPolygon != null) { 
            mousedOverPolygon.getStyleClass().remove(CLASS_MV_MAP_MOUSE_OVER_LAND);
//            mousedOverPolygon.getStyleClass().add(CLASS_MV_MAP_LAND);
            mousedOverPolygon = null;    
        }
    }
    public void update(double mouseX, double mouseY) {
        updateStats(mouseX, mouseY);
//        highlightPolygon(mouseX, mouseY);
//        displayStats();
    }
//    public void displayStats() {        
//        double scale = map.scaleXProperty().doubleValue();
//        String htmlText =   STATS_PREFIX
//                            + buildStatsRow("Scale: ", scale)
//                            + buildStatsRow("Viewport Width: ", viewportWidth)
//                            + buildStatsRow("Viewport Height: ", viewportHeight)
//                            + buildStatsRow("Viewport Translate X: ", viewportTranslateX)
//                            + buildStatsRow("Viewport Translate Y: ", viewportTranslateY)
//                            + buildStatsRow("World Width: ", worldWidth)
//                            + buildStatsRow("World Height: ", worldHeight)
//                            + buildStatsRow("World Mouse X: " , worldMouseX)
//                            + buildStatsRow("World Mouse Y: " , worldMouseY)
//                            + buildPercRow("Viewport Mouse Percent X: ", viewportMousePercentX)
//                            + buildPercRow("Viewport Mouse Percent Y: ", viewportMousePercentY)
//                            + buildStatsRow("World Viewport Width: ", worldViewportWidth)
//                            + buildStatsRow("World Viewport Height: ", worldViewportHeight)
//                            + buildStatsRow("World Viewport Padding Left: ", worldViewportPaddingLeft)
//                            + buildStatsRow("World Viewport Padding Top: " , worldViewportPaddingTop)
//                            + buildStatsRow("World ViewportX: ", worldViewportX)
//                            + buildStatsRow("World ViewportY: ", worldViewportY);
//        if (this.mousedOverPolygon != null) {
//            int numPoints = mousedOverPolygon.getPoints().size()/2;
//            htmlText += buildStatsRow("# of Polygon Points: ", numPoints);
//        }
//        htmlText += STATS_SUFFIX;
//        statsEngine.loadContent(htmlText);
//    }
    
    // VIEWPORT
    double viewportWidth, viewportHeight;
    double viewportTranslateX, viewportTranslateY;
    double viewportMousePercentX, viewportMousePercentY;
    
    // THESE ARE VALUES USING WORLD COORDINATES
    double worldWidth, worldHeight;
    double worldMouseX, worldMouseY;
    double worldViewportWidth, worldViewportHeight;
    double worldViewportPaddingLeft, worldViewportPaddingTop;
    double worldViewportX, worldViewportY;

    private void updateStats(double mouseX, double mouseY) {
        double scale = map.scaleXProperty().doubleValue();
        viewportWidth = map.widthProperty().doubleValue();
        viewportHeight = map.heightProperty().doubleValue();
        viewportTranslateX = map.translateXProperty().doubleValue();
        viewportTranslateY = map.translateYProperty().doubleValue();
        
        worldWidth = map.heightProperty().doubleValue()*2.0;
        worldHeight = map.heightProperty().doubleValue();
        worldMouseX = mouseX;
        worldMouseY = mouseY;
        worldViewportWidth = viewportWidth/scale;
        worldViewportHeight = viewportHeight/scale;
        worldViewportPaddingLeft = (scale-1.0)*(map.getWidth()/2.0);
        worldViewportPaddingTop = (scale-1.0)*(map.getHeight()/2.0);
        worldViewportX = (worldViewportPaddingLeft - viewportTranslateX)/scale;
        worldViewportY = (worldViewportPaddingTop - viewportTranslateY)/scale;
        
        viewportMousePercentX = (worldMouseX - worldViewportX)/worldViewportWidth;
        viewportMousePercentY = (worldMouseY - worldViewportY)/worldViewportHeight;
    }
    
        public ItemsController getItemsController() {
        return itemcontroller;
    }
       public Iterator<RegioVincoItemPrototype> itemsIterator() {
        return this.items.iterator();
    }
       
       public boolean isItemSelected() {
        ObservableList<RegioVincoItemPrototype> selectedItems = this.getSelectedItems();
        return (selectedItems != null) && (selectedItems.size() == 1);
    }
    
    public boolean areItemsSelected() {
        ObservableList<RegioVincoItemPrototype> selectedItems = this.getSelectedItems();
        return (selectedItems != null) && (selectedItems.size() > 1);        
    }

    public boolean isValidToDoItemEdit(RegioVincoItemPrototype itemToEdit, String subregion, String capital, String leader) {
        return isValidNewToDoItem(subregion, capital, leader);
    }

    public boolean isValidNewToDoItem(String subregion, String capital, String leader) {
        if (subregion.trim().length() == 0)
            return false;
        if (capital.trim().length() == 0)
            return false;
        if (leader.trim().length() == 0)
            return false;
        return true;
    }

    public void addItem(RegioVincoItemPrototype itemToAdd) {
        items.add(itemToAdd);
    }
    
    public void snpaImagebottomleft(ImageView imv) {
       imv.setX(0);
        imv.setY(app.getGUIModule().getWindow().getHeight());
    }
    public void snpaImageoriginal(ImageView imv, double x, double y) {
          imv.setTranslateX(x);
        imv.setTranslateY(y);
        
    }
      public void removeItem(RegioVincoItemPrototype itemToAdd) {
        items.remove(itemToAdd);
    }

    public RegioVincoItemPrototype getSelectedItem() {
        if (!isItemSelected()) {
            return null;
        }
        return getSelectedItems().get(0);
    }
    public ObservableList<RegioVincoItemPrototype> getSelectedItems() {
        return (ObservableList<RegioVincoItemPrototype>)this.itemsSelectionModel.getSelectedItems();
    }
    public ImageView getLastImage() {
        return (ImageView)outermap.getChildren().get(outermap.getChildren().size()-1);
    }

    public int getItemIndex(RegioVincoItemPrototype item) {
        return items.indexOf(item);
    }
    
    public void addItemAt(RegioVincoItemPrototype item, int itemIndex) {
        items.add(itemIndex, item);
    }

    public void moveItem(int oldIndex, int newIndex) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        RegioVincoItemPrototype itemToMove = items.remove(oldIndex);
        items.add(newIndex, itemToMove);
         
    }

    public int getNumItems() {
        return items.size();
    }

    public void selectItem(RegioVincoItemPrototype itemToSelect) {
        this.itemsSelectionModel.select(itemToSelect);
    }

    public ArrayList<Integer> removeAll(ArrayList<RegioVincoItemPrototype> itemsToRemove) {
        ArrayList<Integer> itemIndices = new ArrayList();
        for (RegioVincoItemPrototype item: itemsToRemove) {
            itemIndices.add(items.indexOf(item));
        }
        for (RegioVincoItemPrototype item: itemsToRemove) {
            items.remove(item);
        }
        return itemIndices;
    }

    public void addAll(ArrayList<RegioVincoItemPrototype> itemsToAdd, ArrayList<Integer> addItemLocations) {
        for (int i = 0; i < itemsToAdd.size(); i++) {
            RegioVincoItemPrototype itemToAdd = itemsToAdd.get(i);
            Integer location = addItemLocations.get(i);
            items.add(location, itemToAdd);
        }
    }
    public ArrayList<RegioVincoItemPrototype> getCurrentItemsOrder() {
        ArrayList<RegioVincoItemPrototype> orderedItems = new ArrayList();
        for (RegioVincoItemPrototype item : items) {
            orderedItems.add(item);
        }
        return orderedItems;
    }

    public void clearSelected() {
        this.itemsSelectionModel.clearSelection();
    }

    public void sortItems(Comparator sortComparator) {
        Collections.sort(items, sortComparator);
    }

    public void rearrangeItems(ArrayList<RegioVincoItemPrototype> oldListOrder) {
        items.clear();
        for (RegioVincoItemPrototype item : oldListOrder) {
            items.add(item);
        }
    }
    

    public double returnxtolong(double mapX){

        double paneHeight = map.getHeight();

        double unitDegree = paneHeight/180;

        double longcoord = mapX/unitDegree - 180;

        

        return longcoord;

    }
    
     public double returnytolong(double mapcorY){
        double paneHeight = map.getHeight();
           double unitDegree = paneHeight/180;
        double latcoord = (paneHeight - mapcorY)/unitDegree - 90;
        return latcoord;
    }
     
       public int polygonsize() {
     return subregions.size();
 }
     
     public Pane getmapPane() {
        return map;
    }
     
     public Pane getouterpane() {
         return outermap;
     }
     
     public String getMapName() {
         return mapname;
     }
//     public ImageView getImageView() {
//         return itemcontroller.getImageView();
//     }
     public boolean imageselect() {
//         boolean select = true;
//            ColorAdjust colorAdjust = new ColorAdjust();
//        colorAdjust.setBrightness(-0.5);
//        
//         select = this.getouterpane().getChildren().get(1).getEffect() == colorAdjust;
         return this.getouterpane().getChildren().size() > 1;
     }
     public boolean mapsize() {
         Iterator<Integer> idIt = subregions.keySet().iterator();
         return idIt.hasNext();
     }
      public void adjustBorder(double init) {
        double scale = map.scaleXProperty().doubleValue();
        Iterator<Integer> idIt = subregions.keySet().iterator();
        while (idIt.hasNext()) {
            int id = idIt.next();
            ObservableList<Polygon> polygons = subregions.get(id);
            Iterator<Polygon> polyIt = polygons.iterator();
            while (polyIt.hasNext()) {
                Polygon poly = polyIt.next();
                poly.setStrokeWidth(init/scale);
            }
        }
        border = init;
    }
      
      public void setColor(Color init) {
        Iterator<Integer> idIt = subregions.keySet().iterator();
        while (idIt.hasNext()) {
            int id = idIt.next();
            ObservableList<Polygon> polygons = subregions.get(id);
            Iterator<Polygon> polyIt = polygons.iterator();
            while (polyIt.hasNext()) {
                Polygon poly = polyIt.next();
                poly.setStroke(init);
            }
        }
        
      }
   
        public void setMapColor(ArrayList<Color> place) {
        Iterator<Integer> idIt = subregions.keySet().iterator();
            int colorsize = 0;
        while (idIt.hasNext()) {
            int id = idIt.next();
            ObservableList<Polygon> polygons = subregions.get(id);
            Iterator<Polygon> polyIt = polygons.iterator();
            
            while (polyIt.hasNext()) {
                Polygon poly = polyIt.next();
if(colorsize < place.size()) {
                poly.setFill(place.get(colorsize));
               colorsize++;
}
               
            }
        }
//        colorplace.add(place); 
      }
        
public ImageView returnimage() {
    newiv = new ImageView();
//         RegioVincoData data = (RegioVincoData)app.getDataComponent();
        GridPane gridpane = new GridPane();
        gridpane.setPadding(new Insets(5));
        gridpane.setHgap(10);
        gridpane.setVgap(10);
        ImageView imv = new ImageView();
        BufferedImage img = null;
             final JFileChooser fc = new JFileChooser();
            
    int retre = fc.showOpenDialog(fc);
    
        if(retre == JFileChooser.APPROVE_OPTION) {
        filepath = fc.getSelectedFile().getAbsolutePath();

        }
         try {
             
                   img = ImageIO.read(new File(filepath));
               } catch(IOException e) {
                   e.printStackTrace();
               }
              
Image image = SwingFXUtils.toFXImage(img, null);
imv.setImage(image);
   
     filepath =  fc.getSelectedFile().toURI().toString();
    paths = filepath.split("/");
        relative = paths[paths.length-2] + "\\" + paths[paths.length-1];
            relatives.add(relative);
        returnimagelist.add(imv);
        newiv = imv;
        newiv.setOnMouseEntered(e->{
           ColorAdjust ca = new ColorAdjust();
                     ca.setBrightness(0.5);
                     newiv.setEffect(ca);
        });
        newiv.setOnMousePressed(e->{
            imagestartx = e.getX();
            imagestarty = e.getY();
            ImagestartMapDrag(imagestartx, imagestarty);
        });
        newiv.setOnMouseDragged(e->{
            finalx = e.getX();
            finaly = e.getY();
            ImageupdateMapDrag(finalx, finaly, newiv);
        });
        newiv.setOnMouseReleased(e->{
            itemcontroller.processImageMove(imv, imagestartx, imagestarty, finalx, finaly);
             ColorAdjust ca = new ColorAdjust();
                     ca.setBrightness(0);
                     newiv.setEffect(ca);
//             itemcontroller.processImageMove(newiv, dragpointx, dragpointy, imagestartx, imagestarty);
        });
       return imv;
    }
    
    public ArrayList<ImageView> getImageView() {
        return returnimagelist;
    }
    
       public ArrayList<String> managePath() {
        return relatives;
    }
       
       public ImageView selectone() {
           return newiv;
       }
       
       public void imagelist(ImageView imv) {
    this.getouterpane().getChildren().add(imv);
        relatives.add(relative);
        returnimagelist.add(imv);
        for(int i = 0; i < relatives.size(); i++) {
        System.out.println(relatives.get(i));     
        }
       
          System.out.println(returnimagelist.size());
}

public void removeimagelist(ImageView imv) {
//    int location = 0;
//    for(int i = 1; i < returnimagelist.size(); i++) {
//        if(returnimagelist.get(i-1) == imv) {
//            location = i;
//        }
//    }
    this.getouterpane().getChildren().remove(imv);
    returnimagelist.remove(imv);
//    relatives.remove(location);
        System.out.println(relatives.size());
          System.out.println(returnimagelist.size());
}
       
    
//    public String getPath(File file) {
//        String base = PATH_IMAGE;
//        filepath = new File(base).toURI().relativize(new File(filepath).toURI()).getPath();
//        return filepath;
//    }
    
//    public ArrayList<String> managePath() {
//        return relatives;
//      
//    }
         
    public void addGradient(RadialGradient rg, Rectangle ocean){
        CheckBox p = (CheckBox)app.getGUIModule().getGUINode(PROP);
        Slider fangle = (Slider)app.getGUIModule().getGUINode(FOCUS_ANGLE);
        Slider fdistance = (Slider)app.getGUIModule().getGUINode(FOCUS_DISTANCE);
        Slider cx = (Slider)app.getGUIModule().getGUINode(CENTER_X);
        Slider cy = (Slider)app.getGUIModule().getGUINode(CENTER_Y);
        Slider r = (Slider)app.getGUIModule().getGUINode(RADIUS);
        ComboBox cb = (ComboBox)app.getGUIModule().getGUINode(COMBOBOX);
        ColorPicker s1 = (ColorPicker)app.getGUIModule().getGUINode(STEP_0_COLOR);
          ColorPicker s2 = (ColorPicker)app.getGUIModule().getGUINode(STEP_1_COLOR);
          
          boolean newp = rg.isProportional();
          double fanglev = rg.getFocusAngle();
          double fdistancev = rg.getFocusDistance();
          double cxv = rg.getCenterX();
          double cyv = rg.getCenterY();
          double rv = rg.getRadius();
          CycleMethod cm = rg.getCycleMethod();
          String s0v = cm.toString();
          Color c1 = rg.getStops().get(0).getColor();
          Color c2 = rg.getStops().get(1).getColor();
          
          p.setSelected(newp);
          fangle.setValue(fanglev);
          fdistance.setValue(fdistancev);
          cx.setValue(cxv);
          cy.setValue(cyv);
          r.setValue(rv);
          cb.setValue(s0v);
          s1.setValue(c1);
          s2.setValue(c2);

    }
//        
//        public ArrayList<Color> getMapColor(int count) {
//          return colorplace.get(count);
//        }
//      
//        public ArrayList<ArrayList<Color>> getc() {
//            return colorplace;
//        }
//        
//        public void removeMapcolor(ArrayList<Color> place) {
//            colorplace.remove(place);
//        }
        


        
        public double getBorder() {
            return border;
        }
        
        public ArrayList<Color> getFirstColor() {
            return firstcolor;
        }
    
//        public ArrayList<Color> getold() {
//            colorplace.add(oldplace);
//            return oldplace;
//        }
}