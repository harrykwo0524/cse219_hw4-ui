/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RV.transactions;

import RV.RegioVincoListMakerApp;
import RV.data.RegioVincoData;
import javafx.scene.image.ImageView;
import jtps.jTPS_Transaction;

/**
 *
 * @author harry
 */
public class SnapTopLeft_Transaction implements jTPS_Transaction {
     RegioVincoData data;
     RegioVincoListMakerApp app;
    ImageView imageToRemove;
double x;
double y;
    
      public SnapTopLeft_Transaction(RegioVincoListMakerApp initApp, RegioVincoData initData, ImageView initiv, double initx, double inity) {
 data = initData;
 imageToRemove= initiv ;
 app = initApp;
 x = initx;
  y = inity;
    }
    @Override
    public void doTransaction() {
       data.snpaImageoriginal(imageToRemove, 0, 0);
       app.getFileModule().markAsEdited(true);
    }

    @Override
    public void undoTransaction() {
       data.snpaImageoriginal(imageToRemove, x, y);
         app.getFileModule().markAsEdited(true);
    }
}

