/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RV.transactions;

import java.util.ArrayList;
import jtps.jTPS_Transaction;
import RV.RegioVincoListMakerApp;
import RV.data.RegioVincoData;
import RV.data.RegioVincoItemPrototype;

/**
 *
 * @author harry
 */
public class EditItem_Transaction implements jTPS_Transaction {
    
RegioVincoListMakerApp app;
    RegioVincoItemPrototype olditem;
    RegioVincoItemPrototype itemToEdit;
    
     public EditItem_Transaction(RegioVincoListMakerApp initApp, RegioVincoItemPrototype initItem, RegioVincoItemPrototype EditItem) {
                 app = initApp;
                olditem = initItem;
                itemToEdit = EditItem;
                
    }
    @Override
    public void doTransaction() {
       RegioVincoData data = (RegioVincoData)app.getDataComponent();   
      int index = data.getItemIndex(olditem);
      data.addItemAt(itemToEdit, index);
      data.removeItem(olditem);
           app.getFileModule().markAsEdited(true);
    }

    @Override
    public void undoTransaction() {
      RegioVincoData data = (RegioVincoData)app.getDataComponent();   
      int index = data.getItemIndex(itemToEdit);
      data.addItemAt(olditem, index);
      data.removeItem(itemToEdit);
           app.getFileModule().markAsEdited(true);
     
    }
    
}
