/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RV.transactions;

import RV.RegioVincoListMakerApp;
import RV.data.RegioVincoData;
import javafx.scene.image.ImageView;
import jtps.jTPS_Transaction;

/**
 *
 * @author harry
 */
public class AddImage_Transaction implements jTPS_Transaction{
    RegioVincoData initData;
    ImageView initiv;
    RegioVincoListMakerApp app;
    
    public AddImage_Transaction(RegioVincoListMakerApp initApp, RegioVincoData data, ImageView iv) {
       app = initApp;
        initData = data;
        initiv = iv;
    }
    @Override
    public void doTransaction() {
       initData.imagelist(initiv);
       initiv.setFitHeight(200);
       initiv.setFitWidth(200);
       app.getFileModule().markAsEdited(true);
    }

    @Override
    public void undoTransaction() {
          initData.removeimagelist(initiv);
        app.getFileModule().markAsEdited(true);
    }
    
}
