/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RV.transactions;

import RV.RegioVincoListMakerApp;
import RV.data.RegioVincoData;
import jtps.jTPS_Transaction;

/**
 *
 * @author harry
 */
public class ResetViewport_Transaction implements jTPS_Transaction {
RegioVincoData data;
RegioVincoListMakerApp app;
double sx;
double sy;
  double ax;
double ay;    

public ResetViewport_Transaction(RegioVincoListMakerApp initApp, RegioVincoData initData, double initsx, double initsy, double initax, double initay) {
    app = initApp;
    data = initData;
    sx = initsx;
    sy = initsy;
    ax = initax;
    ay = initay;
}

    @Override
    public void doTransaction() {
        data.resetViewport();
        app.getFileModule().markAsEdited(true);
    }

    @Override
    public void undoTransaction() {
         data.getmapPane().setTranslateX(ax);
      data.getmapPane().setTranslateY(ay);
      data.getmapPane().setScaleX(sx);
        data.getmapPane().setScaleY(sy);
        app.getFileModule().markAsEdited(true);
    }
}
