/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RV.transactions;

import RV.RegioVincoListMakerApp;
import RV.data.RegioVincoData;
import java.util.ArrayList;
import javafx.scene.image.ImageView;
import jtps.jTPS_Transaction;

/**
 *
 * @author harry
 */
public class RemoveImage_Transaction implements jTPS_Transaction{
    RegioVincoListMakerApp app;
    RegioVincoData data;
    ImageView imageToRemove;
//    ArrayList<Integer> removedItemLocations;

    public RemoveImage_Transaction(RegioVincoListMakerApp initApp, RegioVincoData initData, ImageView initiv) {
        app = initApp;
 data = initData;
 imageToRemove= initiv ;
    }

    
    @Override
    public void doTransaction() {
        
        data.removeimagelist(imageToRemove);
        app.getFileModule().markAsEdited(true);
    }

    @Override
    public void undoTransaction() {
data.imagelist(imageToRemove);
       imageToRemove.setFitHeight(200);
       imageToRemove.setFitWidth(200);
       app.getFileModule().markAsEdited(true);
    }
    

}