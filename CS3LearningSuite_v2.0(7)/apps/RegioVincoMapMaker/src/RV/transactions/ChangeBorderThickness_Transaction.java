/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RV.transactions;

import RV.RegioVincoListMakerApp;
import static RV.RegioVincoPropertyType.BORDER_THICKNESS;
import static RV.RegioVincoPropertyType.COLOR_PICKER;
import RV.data.RegioVincoData;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Slider;
import jtps.jTPS_Transaction;

/**
 *
 * @author harry
 */
public class ChangeBorderThickness_Transaction implements jTPS_Transaction {
RegioVincoListMakerApp app;
RegioVincoData data;
double newthick;
   double oldthick; 
    public ChangeBorderThickness_Transaction(RegioVincoListMakerApp initapp, RegioVincoData initdata, double initnew, double initold) {
        app = initapp;
        data = initdata;
        oldthick = initold;
        newthick = initnew;
    }
    @Override
    public void doTransaction() {
       data.adjustBorder(newthick);
       Slider s = (Slider) app.getGUIModule().getGUINode(BORDER_THICKNESS);
       s.setValue(newthick);
            app.getFileModule().markAsEdited(true);
    }

    @Override
    public void undoTransaction() {
        data.adjustBorder(oldthick);Slider s = (Slider) app.getGUIModule().getGUINode(BORDER_THICKNESS);
       s.setValue(oldthick);
            app.getFileModule().markAsEdited(true);
    }
    
}
