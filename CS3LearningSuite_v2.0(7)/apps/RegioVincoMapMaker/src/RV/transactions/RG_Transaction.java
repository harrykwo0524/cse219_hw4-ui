/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RV.transactions;

import RV.RegioVincoListMakerApp;
import RV.data.RegioVincoData;
import javafx.scene.paint.RadialGradient;
import javafx.scene.shape.Rectangle;
import jtps.jTPS_Transaction;

/**
 *
 * @author harry
 */
public class RG_Transaction implements jTPS_Transaction {
RegioVincoListMakerApp app;
RegioVincoData data;
RadialGradient newrg;
RadialGradient oldrg;
    
    public RG_Transaction(RegioVincoListMakerApp initapp, RegioVincoData initdata, RadialGradient initnew, RadialGradient initold) {
        app = initapp;
        data = initdata;
        newrg = initnew;
        oldrg = initold;
    }
    @Override
    public void doTransaction() {
        Rectangle oc = (Rectangle) data.getmapPane().getChildren().get(0);
       data.addGradient(newrg, oc);
       oc.setFill(newrg);
       app.getFileModule().markAsEdited(true);
    }

    @Override
    public void undoTransaction() {
        Rectangle oc = (Rectangle) data.getmapPane().getChildren().get(0);
          data.addGradient(oldrg, oc);
          oc.setFill(oldrg);
            app.getFileModule().markAsEdited(true);
    }
    
}
