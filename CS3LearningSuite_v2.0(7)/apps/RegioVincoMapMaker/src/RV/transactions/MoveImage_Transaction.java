/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RV.transactions;

import RV.RegioVincoListMakerApp;
import RV.data.RegioVincoData;
import javafx.scene.image.ImageView;
import jtps.jTPS_Transaction;

/**
 *
 * @author harry
 */
public class MoveImage_Transaction implements jTPS_Transaction {
RegioVincoData data;
RegioVincoListMakerApp app;
double cx;
double cy;
double sx;
double sy;
ImageView imv;
double diffx;
double diffy;
    
    public MoveImage_Transaction(RegioVincoListMakerApp initApp, RegioVincoData initData, double initx, double inity, double oldx, double oldy, ImageView initimv) {
    app = initApp;
        data = initData;
    sx = initx;
    sy = inity;
    cx = oldx;
    cy = oldy;
    imv = initimv;
    }

    
    @Override
    public void doTransaction() {
data.imagemove(sx-cx, sy-cy, imv);
app.getFileModule().markAsEdited(true);
    }

    @Override
    public void undoTransaction() {
imv.setTranslateX(sx-cx);
imv.setTranslateY(sy-cy);
//            imv.setTranslateX(sx);
//         imv.setTranslateY(sy);
//        data.clamp();
app.getFileModule().markAsEdited(true);
    }
    
}
