/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RV.transactions;

import RV.RegioVincoListMakerApp;
import static RV.RegioVincoPropertyType.BORDER_THICKNESS;
import static RV.RegioVincoPropertyType.COLOR_PICKER;
import RV.data.RegioVincoData;
import javafx.scene.control.ColorPicker;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;

/**
 *
 * @author harry
 */
public class ChangeBorderColor_Transaction implements jTPS_Transaction {
RegioVincoData data;
RegioVincoListMakerApp app;
Color newcolor;
   Color oldcolor; 

   public ChangeBorderColor_Transaction(RegioVincoListMakerApp initapp, RegioVincoData initdata, Color initnew, Color initold) {
       app = initapp;
       data= initdata;
       newcolor = initnew;
       oldcolor= initold;
   }
    @Override
    public void doTransaction() {
      data.setColor(newcolor);
     ColorPicker color =  (ColorPicker) app.getGUIModule().getGUINode(COLOR_PICKER);
     color.setValue(newcolor);
     app.getFileModule().markAsEdited(true);
    }

    @Override
    public void undoTransaction() {
        data.setColor(oldcolor);
          ColorPicker color =  (ColorPicker) app.getGUIModule().getGUINode(COLOR_PICKER);
     color.setValue(oldcolor);
         app.getFileModule().markAsEdited(true);
    }
}
