/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RV.transactions;

import RV.RegioVincoListMakerApp;
import RV.data.RegioVincoData;
import java.util.ArrayList;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import jtps.jTPS_Transaction;

/**
 *
 * @author harry
 */
public class RandomColor_Transaction implements jTPS_Transaction {
RegioVincoData data;
RegioVincoListMakerApp app;
ArrayList<Color> newcolor;
ArrayList<Color> oldcolor;

    public RandomColor_Transaction(RegioVincoListMakerApp initApp, RegioVincoData initData,ArrayList<Color> initnew, ArrayList<Color> initold) {
    app = initApp;
        data = initData;
    newcolor = initnew;
    oldcolor = initold;
    }



    @Override
    public void doTransaction() {
//         ArrayList<Color> newc =data.getMapColor(newcolor.size());
        data.setMapColor(newcolor);
        app.getFileModule().markAsEdited(true);
    }

    @Override
    public void undoTransaction() {
         data.setMapColor(oldcolor);
          app.getFileModule().markAsEdited(true);
//          int count = data.getc().size();
//          
//          System.out.println(count);
//        if(count > 0){
//            
//              count--;
//              if(count == 0) {
//                  for(int i = 1; i < data.getmapPane().getChildren().size(); i++) {
//                      Polygon p =  (Polygon) data.getmapPane().getChildren().get(i);
//                      data.getmapPane().getChildren().get(i).setStyle("-fx-fill: linear-gradient(FORESTGREEN, GREENYELLOW);");
//                  }
//              }
//              else {
//                  
//              }
//        ArrayList<Color> oldcolor =data.getMapColor(count);
//        data.removeMapcolor(newcolor);
//        data.setMapColor(oldcolor);
//         
//        }
//      
//        
    }
    
}
    

