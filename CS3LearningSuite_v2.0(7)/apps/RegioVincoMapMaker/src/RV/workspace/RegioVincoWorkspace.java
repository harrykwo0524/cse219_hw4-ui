package RV.workspace;

import static djf.AppPropertyType.HAS_CLIPBOARD_TOOLBAR;
import static djf.AppPropertyType.HAS_FILE_TOOLBAR;
import static djf.AppPropertyType.HAS_HELP_TOOLBAR;
import static djf.AppPropertyType.HAS_TOP_TOOLBAR;
import static djf.AppPropertyType.HAS_UNDO_TOOLBAR;
import static djf.AppPropertyType.LOAD_BUTTON;
import static djf.AppPropertyType.NEW_BUTTON;
import static djf.AppPropertyType.REDO_BUTTON;
import static djf.AppPropertyType.UNDO_BUTTON;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import djf.modules.AppFoolproofModule;
import djf.modules.AppGUIModule;
import static djf.modules.AppGUIModule.DISABLED;
import static djf.modules.AppGUIModule.ENABLED;
import static djf.modules.AppGUIModule.FOCUS_TRAVERSABLE;
import static djf.modules.AppGUIModule.HAS_KEY_HANDLER;
import static djf.modules.AppGUIModule.NOT_FOCUS_TRAVERSABLE;
import static djf.modules.AppGUIModule.NO_KEY_HANDLER;
import djf.ui.AppNodesBuilder;
import djf.ui.controllers.AppFileController;
import djf.ui.dialogs.AppDialogsFacade;
import static djf.ui.style.DJFStyle.CLASS_DJF_ICON_BUTTON;
import java.awt.Checkbox;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.SortEvent;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.SortType;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import properties_manager.PropertiesManager;
import RV.RegioVincoListMakerApp;
import static RV.RegioVincoPropertyType.BORDER_THICKNESS;
import static RV.RegioVincoPropertyType.CENTER_X;
import static RV.RegioVincoPropertyType.CENTER_Y;
import static RV.RegioVincoPropertyType.COLOR_PICKER;
import static RV.RegioVincoPropertyType.COMBOBOX;
import static RV.RegioVincoPropertyType.CYCLE_METHOD;
import static RV.RegioVincoPropertyType.EXIT_BUTTON;
import static RV.RegioVincoPropertyType.FILE_EXPORT_BUTTON;
import static RV.RegioVincoPropertyType.FLOPPY_SAVE_BUTTON;
import static RV.RegioVincoPropertyType.FOCUS_ANGLE;
import static RV.RegioVincoPropertyType.FOCUS_DISTANCE;
import static RV.RegioVincoPropertyType.FOLDER_NEW_BUTTON;
import static RV.RegioVincoPropertyType.FOLDER_OPEN_BUTTON;
import static RV.RegioVincoPropertyType.MV_MAP_NAME;
import static RV.RegioVincoPropertyType.MV_MAP_PANE;
import static RV.RegioVincoPropertyType.MV_OUTER_MAP_PANE;
import static RV.RegioVincoPropertyType.PROP;
import static RV.RegioVincoPropertyType.RADIUS;
import static RV.RegioVincoPropertyType.STEP_0_COLOR;
import static RV.RegioVincoPropertyType.STEP_1_COLOR;
import static RV.RegioVincoPropertyType.TDLM_FOOLPROOF_SETTINGS;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_BIG_HEADER;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_BUTTON;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_COLUMN;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_PANE;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_PROMPT;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_SMALL_HEADER;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_TABLE;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_TEXT_FIELD;
import static RV.RegioVincoPropertyType.TDLM_PANE;
import static RV.RegioVincoPropertyType.TDLM_TO_DO_LIST_LABEL;
import static RV.RegioVincoPropertyType.TDLM_NAME_PANE;
import static RV.RegioVincoPropertyType.TDLM_NAME_LABEL;
import static RV.RegioVincoPropertyType.TDLM_NAME_TEXT_FIELD;
import static RV.RegioVincoPropertyType.TDLM_OWNER_PANE;
import static RV.RegioVincoPropertyType.TDLM_OWNER_LABEL;
import static RV.RegioVincoPropertyType.TDLM_OWNER_TEXT_FIELD;
import static RV.RegioVincoPropertyType.TDLM_ITEMS_PANE;
import static RV.RegioVincoPropertyType.TDLM_ITEM_BUTTONS_PANE;
import static RV.RegioVincoPropertyType.TDLM_ADD_ITEM_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_ASSIGNED_TO_COLUMN;
import static RV.RegioVincoPropertyType.TDLM_BORDER_COLOR_LABEL;
import static RV.RegioVincoPropertyType.TDLM_BORDER_THICKNESS_LABEL;
import static RV.RegioVincoPropertyType.TDLM_CAPITAL_COLUMN;
import static RV.RegioVincoPropertyType.TDLM_CATEGORY_COLUMN;
import static RV.RegioVincoPropertyType.TDLM_COMPLETED_COLUMN;
import static RV.RegioVincoPropertyType.TDLM_DESCRIPTION_COLUMN;
import static RV.RegioVincoPropertyType.TDLM_DOWN_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_EDIT_ITEM_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_END_DATE_COLUMN;
import static RV.RegioVincoPropertyType.TDLM_FIFTH_PANE;
import static RV.RegioVincoPropertyType.TDLM_FIRST_PANE;
import static RV.RegioVincoPropertyType.TDLM_FIT_TO_REGION_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_FOURTH_PANE;
import static RV.RegioVincoPropertyType.TDLM_FRAME_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_GRID_PANE;
import static RV.RegioVincoPropertyType.TDLM_IMAGE_ADD_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_IMAGE_REMOVE_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_REMOVE_ITEM_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_MOVE_UP_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_MOVE_DOWN_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_ITEMS_TABLE_VIEW;
import static RV.RegioVincoPropertyType.TDLM_LEADER_COLUMN;
import static RV.RegioVincoPropertyType.TDLM_NAME_OWNER_PANE;
import static RV.RegioVincoPropertyType.TDLM_POLYGON;
import static RV.RegioVincoPropertyType.TDLM_POLYGON_LABEL;
import static RV.RegioVincoPropertyType.TDLM_RANDOM_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_REGION_NAME_TEXT_FIELD;
import static RV.RegioVincoPropertyType.TDLM_RESIZE_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_SECOND_PANE;
import static RV.RegioVincoPropertyType.TDLM_SNAP_IMAGE_BOTTOMLEFT_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_SNAP_IMAGE_TOPLEFT_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_START_DATE_COLUMN;
import static RV.RegioVincoPropertyType.TDLM_SUBREGION_COLUMN;
import static RV.RegioVincoPropertyType.TDLM_THIRD_PANE;
import static RV.RegioVincoPropertyType.TDLM_TITLE_LABEL;
import static RV.RegioVincoPropertyType.TDLM_TOGGLE;
import static RV.RegioVincoPropertyType.TDLM_TOGGLE_LABEL;
import static RV.RegioVincoPropertyType.TDLM_UP_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_VIEWPORT_BUTTON;
import RV.workspace.controllers.ItemsController;
import RV.workspace.controllers.ItemsTableController;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_BOX;
import RV.data.RegioVincoData;
import RV.data.RegioVincoItemPrototype;
import RV.workspace.foolproof.RegioVincoSelectionFoolproofDesign;
import RV.workspace.dialogs.CreateNewDialog;
import static RV.workspace.style.TDLStyle.CLASS_MV_MAP_MOUSE_OVER_LAND;
import static RV.workspace.style.TDLStyle.CLASS_MV_MAP_OCEAN;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_FILE_BUTTON;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_FILE_TOOLBAR;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_GRID_PANE;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_MAP_TOOLBAR;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_NAVIGATION_BUTTON;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_TEXT_FIELD;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_TITLE_PROMPT;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_TOOLBAR1;
import com.sun.prism.j2d.paint.MultipleGradientPaint;
import static djf.AppPropertyType.APP_BANNER;
import static djf.AppPropertyType.APP_PATH_IMAGES;
import static djf.AppPropertyType.APP_TITLE;
import static djf.AppPropertyType.LOAD_ERROR_CONTENT;
import static djf.AppPropertyType.LOAD_ERROR_TITLE;
import static djf.AppPropertyType.LOAD_WORK_TITLE;
import static djf.AppTemplate.PATH_IMAGE;
import static djf.AppTemplate.PATH_WORK;
import static djf.modules.AppGUIModule.ENABLED;
import static djf.modules.AppGUIModule.FOCUS_TRAVERSABLE;
import static djf.modules.AppGUIModule.NO_KEY_HANDLER;
import djf.modules.AppRecentWorkModule;
import djf.ui.controllers.AppUndoController;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Random;
import javafx.scene.Cursor;
import javafx.scene.control.SelectionMode;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Path;
import javafx.scene.shape.Polygon;
import javax.swing.JFileChooser;
import javax.swing.JSlider;

/**
 *
 * @author McKillaGorilla
 */
public class RegioVincoWorkspace extends AppWorkspaceComponent {
protected FlowPane topToolbarPane;

    // THIS IS THE FILE TOOLBAR AND ITS CONTROLS
    protected ToolBar fileToolbar;

    // THIS IS FOR THE CUT/COPY/PASTE BUTTONS IF WE'RE USING THEM
    protected ToolBar clipboardToolbar;

    // THIS IS FOR THE UNDO/REDO BUTTONS IF WE'RE USING THEM
    protected ToolBar undoToolbar;

    // THIS IS FOR THE HELP/LANGUAGE/ABOUT BUTTONS IF WE'RE USING THEM
    protected ToolBar helpToolbar;

    protected ToolBar helpToolbar2;
         static final int FPS_MIN = 0;
static final int FPS_MAX = 30;
static final int FPS_INIT = 15; 
    public VBox imagepane;
    public ImageView simageview;
    public File newf;
    public static TextField regiontextfield;
    public static String changename;
    public int ImageCount;
    public Label title;
    public double value;
    public Color color;
    public int mapnumber;
    public int count = 0;
    public  ArrayList<Color>newplace = new ArrayList<>();
               ArrayList<Color> firstplace = new ArrayList<>();
    ArrayList<ArrayList<Color>> col = new ArrayList<>();
    ArrayList<ArrayList<Color>> control = new ArrayList<>();
    public  double startx;
    public    double starty;
     public  double finalx = 0;
    public    double finaly = 0;
    public ArrayList<ImageView> list;
    public double ax = 0;
    public double ay = 0;
    public ImageView rimage;
    public Rectangle rec ;

   public String focuscm;
   public Color newc;
    public RegioVincoWorkspace(RegioVincoListMakerApp app) {
        super(app);

        // LAYOUT THE APP
        initLayout();
        
        // 
        initFoolproofDesign();
        
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
//        for(int i = 0; i < .size(); i++) {
//            firstplace.add(Color.CHARTREUSE);
//            
//        }
//        col.add(firstplace);
    }
        
    // THIS HELPER METHOD INITIALIZES ALL THE CONTROLS IN THE WORKSPACE
    private void initLayout() {
        // FIRST LOAD THE FONT FAMILIES FOR THE COMBO BOX
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // THIS WILL BUILD ALL OF OUR JavaFX COMPONENTS FOR US
        AppNodesBuilder tdlBuilder = app.getGUIModule().getNodesBuilder();
        
	// THIS HOLDS ALL THE CONTROLS IN THE WORKSPACE
        
        
        
       rec = new Rectangle();

       VBox toDoListPane           = tdlBuilder.buildVBox(TDLM_PANE,               null,           null,   CLASS_TDLM_BOX, HAS_KEY_HANDLER,             FOCUS_TRAVERSABLE,      ENABLED);
       
        HBox firstPane          = tdlBuilder.buildHBox( TDLM_PANE,    toDoListPane,    null,   CLASS_TDLM_FILE_TOOLBAR, HAS_KEY_HANDLER,            FOCUS_TRAVERSABLE,      ENABLED);
        HBox filePane= tdlBuilder.buildHBox( CLASS_TDLM_FILE_TOOLBAR,    firstPane,    null,   CLASS_TDLM_FILE_TOOLBAR, HAS_KEY_HANDLER,            FOCUS_TRAVERSABLE,      ENABLED);
        Button newButton = tdlBuilder.buildIconButton(FOLDER_NEW_BUTTON, filePane, null, CLASS_TDLM_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
 Button loadButton = tdlBuilder.buildIconButton(FOLDER_OPEN_BUTTON, filePane, null, CLASS_TDLM_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
       Button saveButton = tdlBuilder.buildIconButton(FLOPPY_SAVE_BUTTON, filePane, null, CLASS_TDLM_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
 Button exportButton = tdlBuilder.buildIconButton(FILE_EXPORT_BUTTON, filePane, null, CLASS_TDLM_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
 Button exitButton = tdlBuilder.buildIconButton(EXIT_BUTTON, filePane, null, CLASS_TDLM_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
 Button undoButton = tdlBuilder.buildIconButton(UNDO_BUTTON, filePane, null, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
         Button redoButton = tdlBuilder.buildIconButton(REDO_BUTTON, filePane, null, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);

 
        HBox navigationPane= tdlBuilder.buildHBox( CLASS_TDLM_FILE_TOOLBAR,    firstPane,    null,   CLASS_TDLM_FILE_TOOLBAR, HAS_KEY_HANDLER,            FOCUS_TRAVERSABLE,      ENABLED);
       Button resetViewportButton  = tdlBuilder.buildIconButton( TDLM_VIEWPORT_BUTTON,   navigationPane,       null,   CLASS_TDLM_BUTTON,        HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  ENABLED);
        Button fitToRegionButton      = tdlBuilder.buildIconButton( TDLM_FIT_TO_REGION_BUTTON,      navigationPane,       null,   CLASS_TDLM_BUTTON,        HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  ENABLED);
       
         HBox polygonPane= tdlBuilder.buildHBox(CLASS_TDLM_FILE_TOOLBAR,    firstPane,    null,   CLASS_TDLM_FILE_TOOLBAR, HAS_KEY_HANDLER,            FOCUS_TRAVERSABLE,      ENABLED);
         Button resizeButton      = tdlBuilder.buildIconButton(TDLM_RESIZE_BUTTON,      polygonPane,       null,   CLASS_TDLM_BUTTON,        HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  ENABLED);
         Button addimageButton      = tdlBuilder.buildIconButton(TDLM_IMAGE_ADD_BUTTON,      polygonPane,       null,   CLASS_TDLM_BUTTON,        HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  ENABLED);
         Button removeimageButton      = tdlBuilder.buildIconButton(TDLM_IMAGE_REMOVE_BUTTON,      polygonPane,       null,   CLASS_TDLM_BUTTON,        HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  ENABLED);
         Button snapimagetopleftButton      = tdlBuilder.buildIconButton(TDLM_SNAP_IMAGE_TOPLEFT_BUTTON,      polygonPane,       null,   CLASS_TDLM_BUTTON,        HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  ENABLED);
         Button snapimagebottomleftButton      = tdlBuilder.buildIconButton(TDLM_SNAP_IMAGE_BOTTOMLEFT_BUTTON,      polygonPane,       null,   CLASS_TDLM_BUTTON,        HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  ENABLED);
         Button randomButton      = tdlBuilder.buildIconButton(TDLM_RANDOM_BUTTON,      polygonPane,       null,   CLASS_TDLM_NAVIGATION_BUTTON,        HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  ENABLED);
         Button frameButton      = tdlBuilder.buildIconButton(TDLM_FRAME_BUTTON,      polygonPane,       null,   CLASS_TDLM_NAVIGATION_BUTTON,        HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  ENABLED);

         
          HBox mapDealPane= tdlBuilder.buildHBox( CLASS_TDLM_FILE_TOOLBAR,    firstPane,    null,   CLASS_TDLM_FILE_TOOLBAR, HAS_KEY_HANDLER,            FOCUS_TRAVERSABLE,      ENABLED);
         Label toggle = tdlBuilder.buildLabel(TDLM_TOGGLE_LABEL,         mapDealPane,       null,   CLASS_TDLM_PROMPT, HAS_KEY_HANDLER,           FOCUS_TRAVERSABLE,      ENABLED);
         CheckBox togglebox = tdlBuilder.buildCheckBox(TDLM_TOGGLE, mapDealPane, null, CLASS_TDLM_MAP_TOOLBAR, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
         Label polygon = tdlBuilder.buildLabel(TDLM_POLYGON_LABEL,         mapDealPane,       null,   CLASS_TDLM_PROMPT, HAS_KEY_HANDLER,           FOCUS_TRAVERSABLE,      ENABLED);
         CheckBox polygonbox = tdlBuilder.buildCheckBox(TDLM_POLYGON, mapDealPane, null, CLASS_TDLM_MAP_TOOLBAR, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
         
           HBox borderPane= tdlBuilder.buildHBox( CLASS_TDLM_FILE_TOOLBAR,    firstPane,    null,   CLASS_TDLM_FILE_TOOLBAR, HAS_KEY_HANDLER,            FOCUS_TRAVERSABLE,      ENABLED);
         Label bordercolor = tdlBuilder.buildLabel(TDLM_BORDER_COLOR_LABEL,         borderPane,       null,   CLASS_TDLM_PROMPT, HAS_KEY_HANDLER,           FOCUS_TRAVERSABLE,      ENABLED);
         ColorPicker bordercolorpicker = tdlBuilder.buildColorPicker(COLOR_PICKER, borderPane, null, CLASS_TDLM_MAP_TOOLBAR, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
         bordercolorpicker.setValue(Color.BLACK);
         Label borderthickness = tdlBuilder.buildLabel(TDLM_BORDER_THICKNESS_LABEL,         borderPane,       null,   CLASS_TDLM_PROMPT, HAS_KEY_HANDLER,           FOCUS_TRAVERSABLE,      ENABLED);
         Slider borderthicknessslider = tdlBuilder.buildSlider(BORDER_THICKNESS, borderPane, null, CLASS_TDLM_MAP_TOOLBAR, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED, 0, 0);
        borderthicknessslider.setMinorTickCount(1);
        borderthicknessslider.setMajorTickUnit(1);
          borderthicknessslider.setMax(2);

          borderthicknessslider.setShowTickLabels(ENABLED);
         regiontextfield = tdlBuilder.buildTextField(TDLM_REGION_NAME_TEXT_FIELD, null, null, CLASS_TDLM_TEXT_FIELD, NO_KEY_HANDLER,       FOCUS_TRAVERSABLE,      ENABLED);
//         firstPane.setHgrow(toggle, Priority.NEVER);
//         HBox nameOwnerPane          = tdlBuilder.buildHBox(TDLM_NAME_OWNER_PANE,    toDoListPane,    null,   CLASS_TDLM_BOX, HAS_KEY_HANDLER,            FOCUS_TRAVERSABLE,      ENABLED);
         
//        HBox namePane               = tdlBuilder.buildHBox(TDLM_NAME_PANE,          null,    null,   CLASS_TDLM_BOX, HAS_KEY_HANDLER,             FOCUS_TRAVERSABLE,      ENABLED);
//        Label nameLabel             = tdlBuilder.buildLabel(TDLM_NAME_LABEL,         namePane,       null,   CLASS_TDLM_PROMPT, HAS_KEY_HANDLER,           FOCUS_TRAVERSABLE,      ENABLED);
//        TextField nameTextField     = tdlBuilder.buildTextField(TDLM_NAME_TEXT_FIELD,    namePane,       null,   CLASS_TDLM_TEXT_FIELD, NO_KEY_HANDLER,       FOCUS_TRAVERSABLE,      ENABLED);
//        HBox ownerPane              = tdlBuilder.buildHBox(TDLM_OWNER_PANE,         null,    null,   CLASS_TDLM_BOX, HAS_KEY_HANDLER,             FOCUS_TRAVERSABLE,      ENABLED);
//        Label ownerLabel            = tdlBuilder.buildLabel(TDLM_OWNER_LABEL,        ownerPane,      null,   CLASS_TDLM_PROMPT, HAS_KEY_HANDLER,           FOCUS_TRAVERSABLE,      ENABLED);
//        TextField ownerTextField    = tdlBuilder.buildTextField(TDLM_OWNER_TEXT_FIELD,   ownerPane,      null,   CLASS_TDLM_TEXT_FIELD, NO_KEY_HANDLER,       FOCUS_TRAVERSABLE,      ENABLED);

//RadialGradient ra = new RadialGradient(270, 5, 5, 5, 5, ENABLED, CycleMethod.REFLECT; new Stop(0.5f, Color.WHITE), new Stop(0.7f, Color.BLACK));
        Pane mapPane = new Pane();
//        rimage = new ImageView();
ArrayList<ImageView> list = new ArrayList<>();
simageview = new ImageView();
    double oldfangle = 0;
     double oldfdistance = 0;
    double oldcx = 0;
     double oldcy = 0;
    double oldr = 0;
    boolean oldpro = false;
    CycleMethod oldcm = null;
     Stop[] stop = new Stop[2];
        Stop[] ts = new Stop[2];
        ts[0] = new Stop(0, Color.ALICEBLUE);
        ts[1] = new Stop(1, Color.BLUE);
//     RadialGradient newr;
        // AND THIS WILL BE USED TO CLIP THE MAP SO WE CAN ZOOM
        BorderPane outerMapPane = new BorderPane();
        Rectangle clippingRectangle = new Rectangle();
        outerMapPane.setClip(clippingRectangle);        
        Pane clippedPane = new Pane();
        outerMapPane.setCenter(clippedPane);
        clippedPane.getChildren().add(mapPane);
        Rectangle ocean = new Rectangle();
        mapPane.getChildren().add(ocean);
     
//        RadialGradient tr = new RadialGradient(0, 0, 0, 10, 0, false, CycleMethod.REPEAT, ts);
//        ocean.setFill(tr);
        ocean.getStyleClass().add(CLASS_MV_MAP_OCEAN);
        app.getGUIModule().addGUINode(MV_MAP_PANE, mapPane);
        app.getGUIModule().addGUINode(MV_OUTER_MAP_PANE,outerMapPane);
        mapPane.minWidthProperty().bind(outerMapPane.widthProperty());
        mapPane.maxWidthProperty().bind(outerMapPane.widthProperty());
        mapPane.minHeightProperty().bind(outerMapPane.heightProperty());
        mapPane.maxHeightProperty().bind(outerMapPane.heightProperty());
        outerMapPane.layoutBoundsProperty().addListener((ov, oldValue, newValue) -> {
            clippingRectangle.setWidth(newValue.getWidth());
            clippingRectangle.setHeight(newValue.getHeight());
            ocean.setWidth(newValue.getHeight()*2);
            ocean.setHeight(newValue.getHeight());
        });

//for(int i = 1; i < mapPane.getChildren().size(); i++) {
//                Random rand = new Random();
//                int r = rand.nextInt(255);
//                if(r == 1) {
//                    r = r+1;
//                }
//                else if(r == 255){
//                    r = r-1;
//                }
//                Color color = Color.rgb(r, r, r);
//                Polygon p = (Polygon)mapPane.getChildren().get(i);
//                p.setFill(color);
//                c.add(color);
//            }
//        place.add(c);
        // THIS HAS THE ITEMS PANE COMPONENTS
        VBox itemlistpane = tdlBuilder.buildVBox(TDLM_ITEMS_PANE,                 toDoListPane,       null,   CLASS_TDLM_BOX, HAS_KEY_HANDLER,     FOCUS_TRAVERSABLE,  ENABLED);
        HBox titlepane = tdlBuilder.buildHBox(TDLM_ITEMS_PANE,                 itemlistpane,       null,   CLASS_TDLM_BOX, HAS_KEY_HANDLER,     FOCUS_TRAVERSABLE,  ENABLED);
        title = tdlBuilder.buildLabel(TDLM_TITLE_LABEL,         titlepane,       null,   CLASS_TDLM_TITLE_PROMPT, HAS_KEY_HANDLER,           FOCUS_TRAVERSABLE,      ENABLED);
        title.setAlignment(Pos.CENTER);
        
        Button up = tdlBuilder.buildIconButton(TDLM_UP_BUTTON,      titlepane,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);
                Button down = tdlBuilder.buildIconButton(TDLM_DOWN_BUTTON,      titlepane,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);

        VBox itemsPane              = tdlBuilder.buildVBox(TDLM_ITEMS_PANE,                 itemlistpane,       null,   CLASS_TDLM_BOX, HAS_KEY_HANDLER,     FOCUS_TRAVERSABLE,  ENABLED);
//        HBox itemButtonsPane        = tdlBuilder.buildHBox(TDLM_ITEM_BUTTONS_PANE,          itemsPane,          null,   CLASS_TDLM_BOX, HAS_KEY_HANDLER,     FOCUS_TRAVERSABLE,  ENABLED);
        Button addItemButton        = tdlBuilder.buildIconButton(TDLM_ADD_ITEM_BUTTON,      null,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);
        Button removeItemButton     = tdlBuilder.buildIconButton(TDLM_REMOVE_ITEM_BUTTON,   null,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  DISABLED);
        Button editItemButton       = tdlBuilder.buildIconButton(TDLM_EDIT_ITEM_BUTTON,     null,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  DISABLED);
        Button moveUpItemButton     = tdlBuilder.buildIconButton(TDLM_MOVE_UP_BUTTON,      null,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  DISABLED);
        Button moveDownItemButton   = tdlBuilder.buildIconButton(TDLM_MOVE_DOWN_BUTTON,     null,   null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  DISABLED);
             
        GridPane gp = tdlBuilder.buildGridPane(CLASS_TDLM_FILE_TOOLBAR, itemlistpane, null, CLASS_TDLM_FILE_TOOLBAR, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
                gp.setHgap(1);
        gp.setVgap(1);
        CheckBox prop = tdlBuilder.buildCheckBox(PROP, null, null, CLASS_TDLM_MAP_TOOLBAR, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Slider focusangle = tdlBuilder.buildSlider(FOCUS_ANGLE, null, null, CLASS_TDLM_MAP_TOOLBAR, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED, 0, 0);
         focusangle.setBlockIncrement(25);
        focusangle.setMajorTickUnit(25);
          focusangle.setMax(360);
          focusangle.setShowTickLabels(ENABLED);
           Slider focusdistance = tdlBuilder.buildSlider(FOCUS_DISTANCE, null, null, CLASS_TDLM_MAP_TOOLBAR, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED, 0, 0);
         focusdistance.setBlockIncrement(1);
        focusdistance.setMajorTickUnit(1);
          focusdistance.setMax(1);
          focusdistance.setMin(-1);
          focusdistance.setShowTickLabels(ENABLED);
            Slider centerx = tdlBuilder.buildSlider(CENTER_X, null, null, CLASS_TDLM_MAP_TOOLBAR, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED, 0, 0);
         centerx.setBlockIncrement(100);
        centerx.setMajorTickUnit(100);
          centerx.setMax(1920);
          centerx.setShowTickLabels(ENABLED);
           Slider centery = tdlBuilder.buildSlider(CENTER_Y, null, null, CLASS_TDLM_MAP_TOOLBAR, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED, 0, 0);
         centery.setBlockIncrement(100);
        centery.setMajorTickUnit(100);
          centery.setMax(1080);
          centery.setShowTickLabels(ENABLED);
              Slider radius = tdlBuilder.buildSlider(RADIUS, null, null, CLASS_TDLM_MAP_TOOLBAR, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED, 0, 0);
         radius.setBlockIncrement(50);
        radius.setMajorTickUnit(50);
          radius.setMax(960);
          radius.setShowTickLabels(ENABLED);
         ComboBox<String> combox = new ComboBox<>();
         combox.getItems().addAll("NO_CYCLE", "REFLECT", "REPEAT");
         combox.setPromptText("CYCLE_METHOD");
         app.getGUIModule().addGUINode(COMBOBOX, combox);
          ColorPicker step0 = tdlBuilder.buildColorPicker(STEP_0_COLOR, null, null, CLASS_TDLM_MAP_TOOLBAR, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
           ColorPicker step1 = tdlBuilder.buildColorPicker(STEP_1_COLOR, null, null, CLASS_TDLM_MAP_TOOLBAR, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        gp.add(new Label("Background Gradient"), 0, 0);
        gp.add(new Label("Proportional"), 2, 0);
        gp.add(prop, 3, 0);
        gp.add(new Label("Focus Angle"), 0, 1);
         gp.add(focusangle, 1, 1);
          gp.add(new Label("Focus Distance"), 2, 1);
        gp.add(focusdistance, 3, 1);
         gp.add(new Label("Center X"), 0, 2);
         gp.add(centerx, 1, 2);
          gp.add(new Label("Center Y"), 2, 2);
        gp.add(centery, 3, 2);
        gp.add(new Label("Radius"), 0, 3);
         gp.add(radius, 1, 3);
         gp.add(new Label("Cycle Method"), 2, 3);
         gp.add(combox, 3,3);
          gp.add(new Label("Step 0 Color"), 0, 4);
         gp.add(step0, 1, 4);
         gp.add(new Label("Step 1 Color"), 2, 4);
         gp.add(step1, 3,4);
        
        // AND NOW THE TABLE
        TableView<RegioVincoItemPrototype> itemsTable  = tdlBuilder.buildTableView(TDLM_ITEMS_TABLE_VIEW,       itemsPane,          null,   CLASS_TDLM_TABLE, HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  true);
        TableColumn subregionColumn      = tdlBuilder.buildTableColumn(  TDLM_SUBREGION_COLUMN,    itemsTable,         CLASS_TDLM_COLUMN);
        TableColumn capitalColumn   = tdlBuilder.buildTableColumn(  TDLM_CAPITAL_COLUMN, itemsTable,         CLASS_TDLM_COLUMN);
        TableColumn leaderColumn     = tdlBuilder.buildTableColumn(  TDLM_LEADER_COLUMN,  itemsTable,         CLASS_TDLM_COLUMN);

        // SPECIFY THE TYPES FOR THE COLUMNS
        subregionColumn.setCellValueFactory(new PropertyValueFactory<String,    String>("Subregion"));
        capitalColumn.setCellValueFactory(new PropertyValueFactory<String,    String>("Capital"));
        leaderColumn.setCellValueFactory(new PropertyValueFactory<String, String>("Leader"));
        SplitPane sp = new SplitPane();
         sp.getItems().addAll(outerMapPane, itemlistpane);
sp.setDividerPositions(0.5f, 1.5f);
	// AND PUT EVERYTHING IN THE WORKSPACE
	workspace = new BorderPane();
	((BorderPane)workspace).setTop(firstPane);
        ((BorderPane)workspace).setCenter(sp);
 

        // AND NOW SETUP ALL THE EVENT HANDLING CONTROLLERS

        ItemsController itemsController = new ItemsController((RegioVincoListMakerApp)app);
        AppUndoController undoController = new AppUndoController((RegioVincoListMakerApp)app);


newButton.setOnAction(e->{
    this.showNewDialog();
});
        loadButton.setOnAction(e->{
            app.getGUIModule().getFileController().processLoadRequest();
            if(app.getFileModule().getMapName() != null) {
                 String name = app.getFileModule().getMapName();
            name = name.substring(0, name.indexOf("."));
            title.setText(name);
            }
           
            
        });
        saveButton.setOnAction(e->{
            app.getGUIModule().getFileController().processSaveRequest();
            
        });
        exportButton.setOnAction(e->{
            app.getGUIModule().getFileController().getDirectory();
            app.getGUIModule().getFileController().processExportRequest();
            
        });
        exitButton.setOnAction(e->{
            
             app.getGUIModule().getFileController().processExitRequest();
             
        });
        undoButton.setOnAction(e -> {
            undoController.processUndoRequest();
            ImageCount--;
        });
        redoButton.setOnAction(e -> {
               undoController.processRedoRequest();
               ImageCount++;
        });
        
        resizeButton.setOnAction(e->{
            String changename = AppDialogsFacade.showTextInputDialog(app.getGUIModule().getWindow(), titlepane, CLASS_TDLM_COLUMN);
            if(changename == null) {
                  String newtitle = props.getProperty(APP_TITLE) + " - " + app.getFileModule().getMapName();
        app.getGUIModule().getWindow().setTitle(newtitle);
            }
            else {
                String bannerPath = "./work/" + changename;
              newf = new File(bannerPath);
           app.getFileModule().getWorkFile().renameTo(newf);
           String newtitle = props.getProperty(APP_TITLE) + " - " + newf.getName() + ".json";
        app.getGUIModule().getWindow().setTitle(newtitle);
        title.setText(newf.getName());
           app.getFileModule().markAsEdited(true);
            }
          
        });
       
              
            
      
        
        
        resetViewportButton.setOnAction(e->{
            itemsController.processResetViewport(mapPane.getScaleX(), mapPane.getScaleY(), mapPane.getTranslateX(), mapPane.getTranslateY());
            
        });
       fitToRegionButton.setOnAction(e-> {

       itemsController.processFitToRegion(mapPane.getScaleX(), mapPane.getScaleY(), mapPane.getTranslateX(), mapPane.getTranslateY());
           
         });

        frameButton.setOnAction(e->{
            itemsController.processchange();
            app.getFileModule().markAsEdited(ENABLED);
        });

        
         addimageButton.setOnAction(e->{
             itemsController.processAddImage();
             
         });
         
//  outerMapPane.setOnMousePressed(e->{
//                         ColorAdjust colorAdjust = new ColorAdjust();
//        colorAdjust.setBrightness(-0.5);
//                 if(imageview.contains(e.getX(), e.getY())) {
//                     imageview.setEffect(colorAdjust);
//                      startx = e.getX();
//            starty = e.getY();
////                   ax= imageview.getTranslateX();
////                     ay= imageview.getTranslateY();
//                 }
//                 else {
//                     colorAdjust = new ColorAdjust();
//        colorAdjust.setBrightness(-5);
////                     for(int i = 1; i< ImageCount; i++) {
////                          imageview.setEffect(null);
////                          if(outerMapPane.getChildren().get(i).contains(e.getX(), e.getY())) {
////                              imageview = (ImageView) outerMapPane.getChildren().get(i);
////                               imageview.setEffect(colorAdjust);
////                               startx = e.getX();
////            starty = e.getY();
////                          }
////                     }
//               
//                
//              
//            app.getGUIModule().getWindow().getScene().setCursor(Cursor.MOVE);
//        }
//        });
//   outerMapPane.setOnMouseDragged(exx->{
//               ColorAdjust colorAdjust = new ColorAdjust();
//        colorAdjust.setBrightness(-0.5);
//           
//                 ax = (int)exx.getX();
//            ay = (int)exx.getY();
//          
//                 imageview.setEffect(colorAdjust);
//            finalx =  ax-startx;
//              finaly = ay -starty;
////              double tx = imageview.translateXProperty().doubleValue() + finalx;
////               double ty = imageview.translateYProperty().doubleValue() + finaly;
//                     
//           
//              imageview.translateXProperty().setValue(finalx);
//              imageview.translateYProperty().setValue(finaly);
//          
////                 if(ImageCount >= 1) {
////                      
////                          if(outerMapPane.getChildren().get(ImageCount).contains(exx.getX(), exx.getY())) {
////                              imageview = (ImageView) outerMapPane.getChildren().get(ImageCount);
////                               imageview.setEffect(colorAdjust);
////                               ax = exx.getX();
////            ay = exx.getY();
////              finalx =  ax-startx;
////              finaly = ay -starty;
////              double tx = imageview.translateXProperty().doubleValue() + finalx;
////               double ty = imageview.translateYProperty().doubleValue() + finaly;
////              
////              imageview.translateXProperty().setValue(finalx);
////              imageview.translateYProperty().setValue(finaly);
////                 }
////
////                          }
//             
//           
//             
//   });
//    outerMapPane.setOnMouseReleased(eyyy->{
//         itemsController.processImageMove(imageview, ax,ay, startx, starty);
//               imageview.setEffect(null);
//                app.getGUIModule().getWindow().getScene().setCursor(Cursor.DEFAULT);
//    });
        

        removeimageButton.setOnAction(e->{
itemsController.processRemoveImage();
        });
        togglebox.setOnAction(e->{
   boolean toggleselect= togglebox.isSelected();
   
   
   if(toggleselect) {
       rec = new Rectangle(clippedPane.getWidth(), clippedPane.getHeight());
    rec.setFill(null);
    rec.setStroke(Color.GREEN);
    rec.setStrokeWidth(20);
    clippedPane.getChildren().add(rec);
    app.getFileModule().markAsEdited(ENABLED);
   }
   else {
       clippedPane.getChildren().remove(rec);
       app.getFileModule().markAsEdited(ENABLED);
   }
  
});

//        frameButton.setOnAction(e->{
//            try {
//                itemsController.processviewedit();
//            } catch (IOException ex) {
//                Logger.getLogger(RegioVincoWorkspace.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        });
       


        randomButton.setOnAction(e->{
           newplace = new ArrayList<>();
            RegioVincoData data = (RegioVincoData)app.getDataComponent();
            for(int i = 0; i < mapPane.getChildren().size()-1; i++) {
                Random rand = new Random();
                int r = rand.nextInt(255);
                if(r == 1) {
                    r = r+1;
                }
                else if(r == 255){
                    r = r-1;
                }
                Color color = Color.rgb(r, r, r);
                newplace.add(color);
                
            }
            control.add(newplace);
            count++;
            if(count == 1) {
                ArrayList<Color> exist = new ArrayList<>();
                exist = data.getFirstColor();
//                for(int i = 0; i < mapPane.getChildren().size()-1; i++) {
//                    exist.add(Color.CHARTREUSE);
//                }
                itemsController.processRandomColor(newplace, exist);
            }
            else {
                ArrayList<Color> exist =  control.get(count-2);
                itemsController.processRandomColor(newplace, exist);
//                count--;
            }
            
            
                
        });
        frameButton.setOnAction(e->{
            itemsController.processchange();
            app.getFileModule().markAsEdited(ENABLED);
        });
//        itemsTable.setOnMouseClicked(e -> {      
//                itemsController.processchange();
//            
//        });
 itemsTable.setOnMouseClicked(e->{
     int p = 0;
     RegioVincoData data = (RegioVincoData)app.getDataComponent();
      Polygon poly = new Polygon();
             app.getFoolproofModule().updateAll();
            if (e.getClickCount() == 2) {
                 try {
                     RegioVincoItemPrototype select = data.getSelectedItem();
                     ArrayList<RegioVincoItemPrototype> selected = data.getCurrentItemsOrder();
                     for(int i = 0; i < selected.size(); i++) {
                         if(selected.get(i) == select) {
                             for(p = 1; p<mapPane.getChildren().size(); p++){
                                 if(i+1 == p) {
                                     poly = (Polygon) mapPane.getChildren().get(p);
                                     poly.setStyle("-fx-fill: linear-gradient(PINK, RED);");
                                 }
                             }
                         }
                 

                     }
                     TableView.TableViewSelectionModel itemsSelectionModel = itemsTable.getSelectionModel();
        itemsSelectionModel.setSelectionMode(SelectionMode.MULTIPLE);
                         itemsController.processEditItems();
                         poly.getStyleClass().clear();
                         poly.setStyle(null);

                 } catch (IOException ex) {
                     Logger.getLogger(RegioVincoWorkspace.class.getName()).log(Level.SEVERE, null, ex);
                 }
            }
        });
        ItemsTableController iTC = new ItemsTableController(app);
        itemsTable.widthProperty().addListener(e->{
            iTC.processChangeTableSize();
        });
        
        borderthicknessslider.setOnMousePressed(e->{
            value = borderthicknessslider.getValue();
           
        });
        borderthicknessslider.setOnMouseDragged(e->{
            RegioVincoData data = (RegioVincoData)app.getDataComponent();
            data.adjustBorder(borderthicknessslider.getValue());
        });
        borderthicknessslider.setOnMouseReleased(e->{
            itemsController.processBorderChange(borderthicknessslider.getValue(), value);
        });
        
         bordercolorpicker.setOnShown(e->{
             color = bordercolorpicker.getValue();
             
         });
        bordercolorpicker.setOnAction(e->{
            itemsController.processBorderColor(bordercolorpicker.getValue(), color);
        });

        mapPane.setOnMousePressed(e->{
            itemsController.processMapMousePress((int)e.getX(), (int)e.getY());
//            if(imageview != null) {
//                imageview.setEffect(null);
////               imageview = null;
//            }
        });
        mapPane.setOnMouseReleased(e->{
            itemsController.processMapMouseRelease((int)e.getX(), (int)e.getY());
            app.getFileModule().markAsEdited(ENABLED);
//            if(imageview != null) {
//                imageview.setEffect(null);
////               imageview = null;
//            }
        });
        mapPane.setOnMouseClicked(e->{
               RegioVincoData data = (RegioVincoData)app.getDataComponent();  
               Polygon poly = new Polygon();  
            if ((e.getButton() == MouseButton.PRIMARY) && (e.getClickCount() == 2)){
                double positionx = e.getX();
               double positiony = e.getY();
                Polygon p = new Polygon();
                for(mapnumber = 1; mapnumber < mapPane.getChildren().size(); mapnumber++) {
                     if(mapPane.getChildren().get(mapnumber).contains(positionx, positiony)) {
                         poly = (Polygon) mapPane.getChildren().get(mapnumber);
                     poly.setStyle("-fx-fill: linear-gradient(PINK, RED);");
                    p = (Polygon)mapPane.getChildren().get(mapnumber);
                     }
                }
                   try {
                       TableView.TableViewSelectionModel itemsSelectionModel = itemsTable.getSelectionModel();
        itemsSelectionModel.setSelectionMode(SelectionMode.MULTIPLE);
                       itemsController.processEditByMap(p);
                       poly.getStyleClass().clear();
                       poly.setStyle(null);
                   } catch (IOException ex) {
                       Logger.getLogger(RegioVincoWorkspace.class.getName()).log(Level.SEVERE, null, ex);
                   }
             itemsController.processMapMouseClicked(true, (int)e.getX(), (int)e.getY());
            }
            else if (e.getButton() == MouseButton.SECONDARY)
                itemsController.processMapMouseClicked(false, (int)e.getX(), (int)e.getY());
            app.getFileModule().markAsEdited(ENABLED);
//             if(imageview != null) {
//                imageview.setEffect(null);
////               imageview = null;
//            }
        });
        mapPane.setOnScroll(e->{
            boolean zoomIn = e.getDeltaY() > 0;
            itemsController.processMapMouseScroll(zoomIn, (int)e.getX(), (int)e.getY());
            app.getFileModule().markAsEdited(ENABLED);
//             if(imageview != null) {
//                imageview.setEffect(null);
////               imageview = null;
//            }
        });
        mapPane.setOnMouseMoved(e->{
            itemsController.processMapMouseMoved((int)e.getX(), (int)e.getY());
            app.getFileModule().markAsEdited(ENABLED);
//            if(imageview != null) {
//                imageview.setEffect(null);
////               imageview = null;
//            }
             
        });
        mapPane.setOnMouseDragged(e->{
            itemsController.processMapMouseDragged((int)e.getX(), (int)e.getY());
            app.getFileModule().markAsEdited(ENABLED);
//            if(imageview != null) {
//                imageview.setEffect(null);
////               imageview = null;
//            }
        });
        snapimagetopleftButton.setOnAction(e->{
            itemsController.processTopLeftTransaction(simageview, finalx, finaly);
        });
           snapimagebottomleftButton.setOnAction(e->{
            itemsController.processBottomLeftTransaction(simageview, finalx, finaly);
        });
//        mapPane.setOnMouseExited(e->{
//            RegioVincoData data = (RegioVincoData)app.getDataComponent();
//            double positionx = e.getX();
//               double positiony = e.getY();
//            for(int i = 1; i < mapPane.getChildren().size(); i++) {
//                if(mapPane.contains(positionx, positiony)) {
//                  mapPane.getChildren().get(i).getStyleClass().remove("-fx-fill: linear-gradient(PINK, RED);");
//                }
//            }
//        });
//        ocean.setOnMouseMoved(e->{
//            RegioVincoData data = (RegioVincoData)app.getDataComponent();
//            double positionx = e.getX();
//               double positiony = e.getY();
//            for(int i = 0; i < mapPane.getChildren().size(); i++) {
//                if(ocean.contains(positionx, positiony)) {
//                    Polygon p = (Polygon)mapPane.getChildren().get(i);
//                    p.setFill(place.get(i));
//                   p.getStyleClass().remove("-fx-fill: linear-gradient(PINK, RED);");
//                }
//            }
//        });



prop.setOnAction(e->{
    boolean newp = prop.isSelected();
    RadialGradient oldp = (RadialGradient)ocean.getFill();
    itemsController.processProp(oldfangle, oldfdistance, oldcx, oldcy, oldr, newp, oldcm, stop, oldp);
});
focusangle.setOnMouseDragged(e->{
        RadialGradient oldp = (RadialGradient)ocean.getFill();
   itemsController.processFocusAngle(focusangle.getValue(), oldfdistance, oldcx, oldcy, oldr, oldpro, oldcm, stop, oldp);
});

focusdistance.setOnMouseDragged(e->{
        RadialGradient oldp = (RadialGradient)ocean.getFill();
    itemsController.processFocusDistance(oldfangle, focusdistance.getValue(), oldcx, oldcy, oldr, oldpro, oldcm, stop, oldp);
});


centerx.setOnMouseDragged(e->{
        RadialGradient oldp = (RadialGradient)ocean.getFill();
itemsController.processCenterX(oldfangle, oldfdistance, centerx.getValue(), oldcy, oldr, oldpro, oldcm, stop, oldp);
});


centery.setOnMouseDragged(e->{
        RadialGradient oldp = (RadialGradient)ocean.getFill();
        itemsController.processCenterY(oldfangle, oldfdistance, oldcx, centery.getValue(), oldr, oldpro, oldcm, stop, oldp);
});


radius.setOnMouseDragged(e->{
        RadialGradient oldp = (RadialGradient)ocean.getFill();
     itemsController.processRadius(oldfangle, oldfdistance, oldcx, oldcy, radius.getValue(), oldpro, oldcm, stop, oldp);
});


combox.setOnAction(e->{
    RadialGradient oldp = (RadialGradient)ocean.getFill();
 itemsController.processCombo(oldfangle, oldfdistance, oldcx, oldcy, oldr, oldpro, CycleMethod.valueOf(combox.getValue()), stop, oldp);
});

step0.setOnAction(e->{
    RadialGradient oldp = (RadialGradient)ocean.getFill();
    Stop s = new Stop(0, step0.getValue());
 itemsController.processStep0(oldfangle, oldfdistance, oldcx, oldcy, oldr, oldpro, oldcm, s, stop[1], oldp);
});

step1.setOnAction(e->{
    RadialGradient oldp = (RadialGradient)ocean.getFill();
      Stop s = new Stop(1, step1.getValue());
 itemsController.processStep1(oldfangle, oldfdistance, oldcx, oldcy, oldr, oldpro, oldcm,stop[0],s, oldp);
});

        itemsTable.setOnSort(new EventHandler<SortEvent<TableView<RegioVincoItemPrototype>>>(){
            @Override
            public void handle(SortEvent<TableView<RegioVincoItemPrototype>> event) {
                RegioVincoData data = (RegioVincoData)app.getDataComponent();
                ArrayList<RegioVincoItemPrototype> oldListOrder = data.getCurrentItemsOrder();
                TableView view = event.getSource();
                ObservableList sortOrder = view.getSortOrder();
                if ((sortOrder != null) && (sortOrder.size() == 1)) {
                    TableColumn sortColumn = event.getSource().getSortOrder().get(0);
                    String columnText = sortColumn.getText();
                    SortType sortType = sortColumn.getSortType();
                    System.out.println("Sort by " + columnText);
                    event.consume();
//                    SortItems_Transaction transaction = new SortItems_Transaction(data, oldListOrder, columnText, sortType);
//                    app.processTransaction(transaction);
                    app.getFoolproofModule().updateAll();
                }
            }            
        });
        
       up.setOnAction(e->{
           itemsController.processMoveItemUp();
       });
        down.setOnAction(e->{
           itemsController.processMoveItemDown();
       });
        
//       focusangle.setOnMousePressed(e->{
//            value = focusangle.getValue(); 
//           
//        });
//        focusangle.setOnMouseDragged(e->{
//            RegioVincoData data = (RegioVincoData)app.getDataComponent();
//            RadialGradient ra = new RadialGradient(value, focusdistance.getValue(), centerx.getValue(), centery.getValue(), radius.getValue(), ENABLED, CycleMethod.REFLECT,new Stop[] {
//        new Stop(0, Color.DODGERBLUE),
//        new Stop(1, Color.BLACK)
//    });
//            data.adjustRadialGradient(ra);
//        });
//        focusangle.setOnMouseReleased(e->{
//            itemsController.processGradientChange(focusangle.getValue(), value);
//        });
//        
//         focusdistance .setOnMousePressed(e->{
//            value = focusdistance.getValue();
//           
//        });
//        focusdistance .setOnMouseDragged(e->{
//            RegioVincoData data = (RegioVincoData)app.getDataComponent();
//            data.adjustRadialGradient(focusdistance .getValue());
//        });
//        focusdistance .setOnMouseReleased(e->{
//            itemsController.processGradientChange(focusdistance .getValue(), value);
//        });
//        
//         centerx  .setOnMousePressed(e->{
//            value = centerx .getValue();
//           
//        });
//        centerx  .setOnMouseDragged(e->{
//            RegioVincoData data = (RegioVincoData)app.getDataComponent();
//            data.adjustRadialGradient(centerx  .getValue());
//        });
//        centerx  .setOnMouseReleased(e->{
//            itemsController.processGradientChange(centerx  .getValue(), value);
//        });
//        
//        centery   .setOnMousePressed(e->{
//            value = centery  .getValue();
//           
//        });
//        centery   .setOnMouseDragged(e->{
//            RegioVincoData data = (RegioVincoData)app.getDataComponent();
//            data.adjustRadialGradient(centery   .getValue());
//        });
//        centery   .setOnMouseReleased(e->{
//            itemsController.processGradientChange(centery   .getValue(), value);
//        });
//        
//        radius   .setOnMousePressed(e->{
//            value = centery  .getValue();
//           
//        });
//        radius   .setOnMouseDragged(e->{
//            RegioVincoData data = (RegioVincoData)app.getDataComponent();
//            data.adjustRadialGradient(radius   .getValue());
//        });
//        radius   .setOnMouseReleased(e->{
//            itemsController.processGradientChange(radius   .getValue(), value);
//        });
        
        
        
    }
    
    public void initFoolproofDesign() {
        AppGUIModule gui = app.getGUIModule();
        AppFoolproofModule foolproofSettings = app.getFoolproofModule();
        foolproofSettings.registerModeSettings(TDLM_FOOLPROOF_SETTINGS, 
                new RegioVincoSelectionFoolproofDesign((RegioVincoListMakerApp)app));
    }

    @Override
    public void processWorkspaceKeyEvent(KeyEvent ke) {
       // WE AREN'T USING THIS FOR THIS APPLICATION
    }

    @Override
    public void showNewDialog() {
       AppRecentWorkModule recentWork = app.getRecentWorkModule();
        recentWork.loadRecentWorkList();

        // OPEN THE DIALOG
        CreateNewDialog wd = new CreateNewDialog(app);
        wd.show();
        
        // AND RETURN THE USER SELECTION
//        return wd.selectedWorkName;
    }

//    public void getGradient(RadialGradient rad) {
//        
//    }
     
     public TextField getRegion() {
         return regiontextfield;
     }
     
//     public Label getTitle() {
//         return title;
//     }
     
}