/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RV.transactions;

import RV.RegioVincoListMakerApp;
import RV.data.RegioVincoData;
import javafx.scene.image.ImageView;
import jtps.jTPS_Transaction;

/**
 *
 * @author harry
 */
public class SnapLeftBottom_Transaction implements jTPS_Transaction {
     RegioVincoListMakerApp app;
    ImageView imageToRemove;

      public SnapLeftBottom_Transaction(RegioVincoListMakerApp initApp, ImageView initiv) {
 app = initApp;
 imageToRemove= initiv ;
    }
    @Override
    public void doTransaction() {
       RegioVincoData data = (RegioVincoData)app.getDataComponent();
       data.snpaImagebottomleft(imageToRemove);
    }

    @Override
    public void undoTransaction() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
       data.snpaImageoriginal(imageToRemove);
    }
}
