package RV.workspace.controllers;

import java.io.IOException;
import java.util.ArrayList;
import RV.RegioVincoListMakerApp;
import RV.data.RegioVincoData;
import RV.data.RegioVincoItemPrototype;
import RV.transactions.AddImage_Transaction;
import RV.transactions.MoveImage_Transaction;
import RV.transactions.RemoveImage_Transaction;
import RV.transactions.SnapLeftBottom_Transaction;
import RV.workspace.dialogs.RegioVincoListItemDialog;
//import RV.transactions.AddItem_Transaction;
//import RV.transactions.EditItem_Transaction;
//import RV.transactions.MoveItem_Transaction;
//import RV.transactions.RemoveItems_Transaction;
import RV.workspace.dialogs.ChangeDimensionDialog;
import RV.workspace.dialogs.View_EditSubreionDialog;
import static djf.AppPropertyType.LOAD_ERROR_CONTENT;
import static djf.AppPropertyType.LOAD_ERROR_TITLE;
import static djf.AppPropertyType.LOAD_WORK_TITLE;
import static djf.AppPropertyType.SAVE_VERIFY_CONTENT;
import static djf.AppPropertyType.SAVE_VERIFY_TITLE;
import static djf.AppPropertyType.SAVE_WORK_TITLE;
import djf.AppTemplate;
import static djf.AppTemplate.PATH_IMAGE;
import djf.modules.AppFileModule;
import djf.ui.dialogs.AppDialogsFacade;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URI;
import java.nio.file.Paths;
import java.util.Collection;
import javafx.beans.property.StringProperty;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Insets;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Path;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import properties_manager.PropertiesManager;

/**
 *
 * @author McKillaGorilla
 */
public class ItemsController {
    RegioVincoListMakerApp app;
    RegioVincoListItemDialog itemDialog;
    ChangeDimensionDialog changedialog;
    ArrayList<Double> oldDragValues;
    ArrayList<Double> newDragValues;
    View_EditSubreionDialog vieweditdialog;
    public static String filepath;
    public static String[] paths;
    public static ArrayList<String> relatives;
    public static String relative;
    public static int length;
    double moveInc = 25.0;
    public ImageView imv;
    public ItemsController(RegioVincoListMakerApp initApp) {
        app = initApp;
        
        itemDialog = new RegioVincoListItemDialog(app);
        changedialog = new ChangeDimensionDialog(app);
        vieweditdialog = new View_EditSubreionDialog(app);
        
        oldDragValues = new ArrayList();
        newDragValues = new ArrayList();
        relatives= new ArrayList();
        
    }
    
        public void processMapMousePress(int x, int y) {
        RegioVincoData  data = (RegioVincoData )app.getDataComponent();
        data.startMapDrag(x, y);
    }
        
        public void processOuterMapPress(int x, int y) {
             RegioVincoData  data = (RegioVincoData )app.getDataComponent();
             
        data.startOuterMapDrag(x, y);
        }
    
    /**
     * Respond to mouse button release on the rendering surface, which we call canvas,
     * but is actually a Pane.
     */
    public void processMapMouseRelease(int x, int y) {
        RegioVincoData  data = (RegioVincoData )app.getDataComponent();
        data.endMapDrag(x, y);
    }
    
    public void processOuterMapMouseRelease(int x, int y) {
        RegioVincoData  data = (RegioVincoData )app.getDataComponent();
        data.endOuterMapDrag(x, y);
    }


    public void processMapMouseClicked(boolean leftButton, int x, int y) {
        RegioVincoData  data = (RegioVincoData )app.getDataComponent();
        if (leftButton)
            data.zoomInOnPoint(x, y);
        else
            data.zoomOutOnPoint(x, y);
    }
    public void processOuterMapMouseClicked(boolean leftButton, int x, int y) {
        RegioVincoData  data = (RegioVincoData )app.getDataComponent();
        
        if (leftButton)
            data.outerzoomInOnPoint(x, y);
        else
            data.outerzoomOutOnPoint(x, y);
    }

    public void processMapMouseScroll(boolean zoomIn, int x, int y) {
       RegioVincoData data = (RegioVincoData )app.getDataComponent();
        if (zoomIn)
            data.zoomInOnPoint(x, y);
        else
            data.zoomOutOnPoint(x, y);
    }

    /**
     * Respond to mouse dragging on the rendering surface, which we call canvas,
     * but is actually a Pane.
     */
    public void processMapMouseMoved(int x, int y) {
        RegioVincoData  data = (RegioVincoData )app.getDataComponent();
        data.update(x, y);
    }
    public void processOuterMapMouseMoved(int x, int y) {
        RegioVincoData  data = (RegioVincoData )app.getDataComponent();
        data.outerupdate(x, y);
    }

    public void processMapMouseDragged(double x, double y) {
        RegioVincoData  data = (RegioVincoData )app.getDataComponent();
        data.updateMapDrag(x, y);
    }
    public void processOuterMapMouseDragged(double x, double y) {
        RegioVincoData  data = (RegioVincoData )app.getDataComponent();
        data.updateOuterMapDrag(x, y);
    }
    
//    public void processAddItem() {
//        itemDialog.showAddDialog();
//        RegioVincoItemPrototype newItem = itemDialog.getNewItem();        
//        if (newItem != null) {
//            // IF IT HAS A UNIQUE NAME AND COLOR
//            // THEN CREATE A TRANSACTION FOR IT
//            // AND ADD IT
//            RegioVincoData data = (RegioVincoData)app.getDataComponent();
//            AddItem_Transaction transaction = new AddItem_Transaction(data, newItem);
//            app.processTransaction(transaction);
//        }    
//        // OTHERWISE TELL THE USER WHAT THEY
//        // HAVE DONE WRONG
//        else {
//            djf.ui.dialogs.AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), "Invalid Line", "Invalid data for a new line");
//        }
//    }
    
//    public void processRemoveItems() {
//        RegioVincoData data = (RegioVincoData)app.getDataComponent();
//        if (data.isItemSelected() || data.areItemsSelected()) {
//            ArrayList<RegioVincoItemPrototype> itemsToRemove = new ArrayList(data.getSelectedItems());
//            RemoveItems_Transaction transaction = new RemoveItems_Transaction(app, itemsToRemove);
//            app.processTransaction(transaction);
//        }
//    }

//    public void processEditItem() {
//        RegioVincoData data = (RegioVincoData)app.getDataComponent();
//        if (data.isItemSelected()) {
//            RegioVincoItemPrototype itemToEdit = data.getSelectedItem();
//            itemDialog.showEditDialog(itemToEdit);
//            RegioVincoItemPrototype editItem = itemDialog.getEditItem();
//            if (editItem != null) {
//                EditItem_Transaction transaction = new EditItem_Transaction(itemToEdit, editItem.getSubregion(), editItem.getCapital(), editItem.getLeader());
//                app.processTransaction(transaction);
//            }
//        }
//    }
    
//    public void processMoveItemUp() {
//        RegioVincoData data = (RegioVincoData)app.getDataComponent();
//        if (data.isItemSelected()) {
//            RegioVincoItemPrototype itemToMove = data.getSelectedItem();
//            int oldIndex = data.getItemIndex(itemToMove);
//            if (oldIndex > 0) {
//                MoveItem_Transaction transaction = new MoveItem_Transaction(data, oldIndex, oldIndex-1);
//                app.processTransaction(transaction);
//                
//                // DESELECT THE OLD INDEX
//                data.clearSelected();
//                
//                // AND SELECT THE MOVED ONE
//                data.selectItem(itemToMove);
//                
//                // UPDATE BUTTONS
//                app.getFoolproofModule().updateAll();
//            }
//        }
//    }
    
//    public void processMoveItemDown() {
//        RegioVincoData data = (RegioVincoData)app.getDataComponent();
//        if (data.isItemSelected()) {
//            RegioVincoItemPrototype itemToMove = data.getSelectedItem();
//            int oldIndex = data.getItemIndex(itemToMove);
//            if (oldIndex < (data.getNumItems()-1)) {
//                MoveItem_Transaction transaction = new MoveItem_Transaction(data, oldIndex, oldIndex+1);
//                app.processTransaction(transaction);
//                
//                // DESELECT THE OLD INDEX
//                data.clearSelected();
//                
//                // AND SELECT THE MOVED ONE
//                data.selectItem(itemToMove);
//                
//                // UPDATE BUTTONS
//                app.getFoolproofModule().updateAll();
//            }
//        }
//    }
    
     public void processAddImage() {
        imv = this.returnimage();
        if(imv != null) {
            RegioVincoData data =  (RegioVincoData)app.getDataComponent();
            AddImage_Transaction transaction = new AddImage_Transaction(data, imv);
            app.processTransaction(transaction);
        }
        else {
            AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), "Invalid Process", "No item was selected");
        }
}
     
     public void processRemoveImage() {
         RegioVincoData data = (RegioVincoData)app.getDataComponent();
         if(data.imageselect()) {
            imv = data.getLastImage();
             RemoveImage_Transaction transaction = new RemoveImage_Transaction(app, imv);
             app.processTransaction(transaction);
         }
         
     }
     
     public void processMoveImage(int x, int y) {
         RegioVincoData data = (RegioVincoData)app.getDataComponent();
          if(data.imageselect()) {
              imv = data.getImageView();
              int tx = x;
              int ty = y;
              MoveImage_Transaction transaction = new MoveImage_Transaction(data, tx, ty, imv);
              app.processTransaction(transaction);
//              app.getFoolproofModule().updateAll();
          }
     }
    
     public void processBottomLeftTransaction() {
          RegioVincoData data = (RegioVincoData)app.getDataComponent();
           if(data.imageselect()) {
                imv = data.getLastImage();
                  SnapLeftBottom_Transaction transaction = new SnapLeftBottom_Transaction(app, imv);
             app.processTransaction(transaction);
//             app.getFoolproofModule().updateAll();
           }
     }
    
    public void processchange() {
        changedialog.showDimensionChangeDialog();
    }
    
    public void processviewedit() throws IOException {
        vieweditdialog.showViewEditDialog();
    }
 

    /**
     * This helper method asks the user for a file to open. The user-selected
     * file is then loaded and the GUI updated. Note that if the user cancels
     * the open process, nothing is done. If an error occurs loading the file, a
     * message is displayed, but nothing changes.
     * @return 
     */
//    private void promptToLoadImage() {
//	       
//               BufferedImage img = null;
//               try {
//                   img = ImageIO.read(new File(AppTemplate.PATH_IMAGE));
//               } catch(IOException e) {
//                   e.printStackTrace();
//               }
//               JLabel lbl = new JLabel();
//               lbl.setIcon(new ImageIcon(img));
//               
//    }
    
    
    
//    public String getPath() {
//        String filepath = null;
//             final JFileChooser fc = new JFileChooser();
//    int retre = fc.showOpenDialog(fc);
//    
//        if(retre == JFileChooser.APPROVE_OPTION) {
//        filepath = fc.getSelectedFile().getAbsolutePath();
//        }
//        fc.hide();
//        return filepath;
//    }
    public ImageView returnimage() {
//         RegioVincoData data = (RegioVincoData)app.getDataComponent();
        GridPane gridpane = new GridPane();
        gridpane.setPadding(new Insets(5));
        gridpane.setHgap(10);
        gridpane.setVgap(10);
        imv = new ImageView();
        BufferedImage img = null;
             final JFileChooser fc = new JFileChooser();
            
    int retre = fc.showOpenDialog(fc);
    
        if(retre == JFileChooser.APPROVE_OPTION) {
        filepath = fc.getSelectedFile().getAbsolutePath();

        }
         try {
             
                   img = ImageIO.read(new File(filepath));
               } catch(IOException e) {
                   e.printStackTrace();
               }
              
Image image = SwingFXUtils.toFXImage(img, null);
imv.setImage(image);
   
     filepath =  fc.getSelectedFile().toURI().toString();
    paths = filepath.split("/");
        relative = paths[paths.length-2] + "\\" + paths[paths.length-1];
        relatives.add(relative);
       return imv;
    }
    
    public ImageView getImageView() {
        return imv;
    }
    
//    public String getPath(File file) {
//        String base = PATH_IMAGE;
//        filepath = new File(base).toURI().relativize(new File(filepath).toURI()).getPath();
//        return filepath;
//    }
    
    public ArrayList<String> managePath() {
        return relatives;
      
    }
//   public void processfilepath() {
//       
//   }
// 
     public void processResetViewport() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.resetViewport();
    }
    
    public void processFitToRegion() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.fitToRegion();
    }


    public void processZoomIn() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.zoom(2.0);
        data.update(0, 0);
    }

    public void processZoomOut() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.zoom(1.0/2.0);
        data.update(0, 0);
    }
        
    public void processMoveLeft() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.move(moveInc, 0);
        data.update(0, 0);
    }

    public void processMoveRight() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.move(-moveInc, 0);
        data.update(0, 0);
    }

    public void processMoveUp() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.move(0, moveInc);
        data.update(0, 0);
    }

    public void processMoveDown() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.move(0, -moveInc);
        data.update(0, 0);
    }
    
}
