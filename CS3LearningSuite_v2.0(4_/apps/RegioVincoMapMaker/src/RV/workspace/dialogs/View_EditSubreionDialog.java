package RV.workspace.dialogs;

import static djf.AppPropertyType.APP_BANNER;
import static djf.AppPropertyType.APP_PATH_IMAGES;
import djf.modules.AppLanguageModule;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import javax.swing.text.html.ImageView;
import properties_manager.PropertiesManager;
import RV.RegioVincoListMakerApp;
import static RV.RegioVincoPropertyType.ENGLAND;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_ADD_HEADER_TEXT;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_ASSIGNED_TO_PROMPT;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_ASSIGNED_TO_TEXT_FIELD;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_CANCEL_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_CATEGORY_PROMPT;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_COMPLETED_CHECK_BOX;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_COMPLETED_PROMPT;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_DESCRIPTION_PROMPT;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_EDIT_HEADER_TEXT;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_END_DATE_PROMPT;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_HEADER;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_OK_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_ITEM_DIALOG_START_DATE_PROMPT;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_BUTTON;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_CHECK_BOX;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_DATE_PICKER;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_GRID;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_HEADER;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_PANE;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_PROMPT;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_TEXT_FIELD;
import RV.data.RegioVincoData;
import RV.data.RegioVincoItemPrototype;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_BUTTON2;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_DIALOG_PROMPT2;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_TITLE_PROMPT;

/**
 *
 * @author McKillaGorilla
 */
public class View_EditSubreionDialog extends Stage {
    RegioVincoListMakerApp app;
    GridPane gridPane;
    
    Button prev = new Button("Previous");   
    Button next = new Button("Next");   
    Label headerLabel = new Label();    
    Label subregionlabel = new Label("Subregion Name ");   
   Label capitalLabel = new Label("Capital ");    
    Label leaderlabel = new Label("Leader's Name");   
    Label flaglabel = new Label("Flag"); 
        TextField subregionTextField = new TextField();
        TextField capitalTextField = new TextField();
        TextField leaderTextField = new TextField();
         javafx.scene.image.ImageView welcomeDialogImageView;
    HBox okCancelPane = new HBox();
    Button okButton = new Button();
    HBox flagpane = new HBox();

    RegioVincoItemPrototype itemToEdit;
//    RegioVincoItemPrototype newItem;
//    RegioVincoItemPrototype editItem;
//    boolean editing;
    

    EventHandler cancelHandler;
    EventHandler addItemOkHandler;
    EventHandler editItemOkHandler;
    
    public View_EditSubreionDialog(RegioVincoListMakerApp initApp) {
       
        // KEEP THIS FOR WHEN THE WORK IS ENTERED
        app = initApp;

        // EVERYTHING GOES IN HERE
        gridPane = new GridPane();
        gridPane.getStyleClass().add(CLASS_TDLM_DIALOG_GRID);
        initDialog();

        // NOW PUT THE GRID IN THE SCENE AND THE SCENE IN THE DIALOG
        Scene scene = new Scene(gridPane);
        this.setScene(scene);
        
        // SETUP THE STYLESHEET
        app.getGUIModule().initStylesheet(this);
        scene.getStylesheets().add(CLASS_TDLM_DIALOG_GRID);                             
        
        // MAKE IT MODAL
        this.initOwner(app.getGUIModule().getWindow());
        this.initModality(Modality.APPLICATION_MODAL);
    }
       
    protected void initGridNode(Node node, Object nodeId, String styleClass, int col, int row, int colSpan, int rowSpan, boolean isLanguageDependent) {
        // GET THE LANGUAGE SETTINGS
        AppLanguageModule languageSettings = app.getLanguageModule();
        
        // TAKE CARE OF THE TEXT
        if (isLanguageDependent) {
            languageSettings.addLabeledControlProperty(nodeId + "_TEXT", ((Labeled)node).textProperty());
            ((Labeled)node).setTooltip(new Tooltip(""));
            languageSettings.addLabeledControlProperty(nodeId + "_TOOLTIP", ((Labeled)node).tooltipProperty().get().textProperty());
        }
        
        // PUT IT IN THE UI
        if (col >= 0)
            gridPane.add(node, col, row, colSpan, rowSpan);

        // SETUP IT'S STYLE SHEET
        node.getStyleClass().add(styleClass);
    }

    private void initDialog() {
         
        // THE NODES ABOVE GO DIRECTLY INSIDE THE GRID
        initGridNode(headerLabel,           TDLM_ITEM_DIALOG_HEADER,                CLASS_TDLM_DIALOG_HEADER,       0, 0, 3, 1, true);
        initGridNode(prev, null, CLASS_TDLM_DIALOG_BUTTON2, 0, 1, 3, 1, true);
        initGridNode(next, null, CLASS_TDLM_DIALOG_BUTTON2, 1, 1, 3, 1, true);
        initGridNode(subregionlabel, CLASS_TDLM_DIALOG_PROMPT2, CLASS_TDLM_DIALOG_PROMPT2, 0,2,1,1, true);
        initGridNode(subregionTextField, null, CLASS_TDLM_DIALOG_TEXT_FIELD,  1,2,1,1, false);
         initGridNode(capitalLabel, CLASS_TDLM_DIALOG_PROMPT2, CLASS_TDLM_DIALOG_PROMPT2, 0,3,1,1, true);
        initGridNode(capitalTextField, null, CLASS_TDLM_DIALOG_TEXT_FIELD,  1,3,1,1, false);
         initGridNode(leaderlabel, CLASS_TDLM_DIALOG_PROMPT2, CLASS_TDLM_DIALOG_PROMPT2, 0,4,1,1, true);
        initGridNode(leaderTextField, null, CLASS_TDLM_DIALOG_TEXT_FIELD,  1,4,1,1, false);
        initGridNode(flaglabel, CLASS_TDLM_DIALOG_PROMPT2, CLASS_TDLM_DIALOG_PROMPT2,  0,5,2,1, true);
        initGridNode(flagpane, null, null, 1,6,2,1, false);
        initGridNode(okCancelPane,          null,                                   CLASS_TDLM_DIALOG_PANE,         0, 10, 2, 1, false);

        okButton = new Button();
         okButton.setText("Ok");
         
        app.getGUIModule().addGUINode(TDLM_ITEM_DIALOG_OK_BUTTON, okButton);
        okButton.getStyleClass().add(CLASS_TDLM_DIALOG_BUTTON);
        okCancelPane.getChildren().add(okButton);
        okCancelPane.setAlignment(Pos.CENTER);

        AppLanguageModule languageSettings = app.getLanguageModule();
        languageSettings.addLabeledControlProperty(TDLM_ITEM_DIALOG_OK_BUTTON + "_TEXT",    okButton.textProperty());
//        languageSettings.addLabeledControlProperty(TDLM_ITEM_DIALOG_CANCEL_BUTTON + "_TEXT",    cancelButton.textProperty());
       
        // AND SETUP THE EVENT HANDLERS
//        categoryTextField.setOnAction(e->{
//            processCompleteWork();
//        });
//        descriptionTextField.setOnAction(e->{
//            processCompleteWork();
//        });
        okButton.setOnAction(e->{
//            processCompleteWork();
        });
//        cancelButton.setOnAction(e->{
//            newItem = null;
//            editItem = null;
//            this.hide();
//        });   
    }
    
//    private void makeNewItem() {
//        String category = categoryTextField.getText();
//        String description = descriptionTextField.getText();
//        LocalDate startDate = startDatePicker.getValue();
//        LocalDate endDate = endDatePicker.getValue();
//        String assignedTo = assignedToTextField.getText();
//        boolean completed = completedCheckBox.selectedProperty().getValue();
//        newItem = new RegioVincoItemPrototype(category, description, startDate, endDate, assignedTo, completed);
//        this.hide();
//    }
    
//    private void processCompleteWork() {
//        // GET THE SETTINGS
//        String category = categoryTextField.getText();
//        String description = descriptionTextField.getText();
//        LocalDate startDate = startDatePicker.getValue();
//        LocalDate endDate = endDatePicker.getValue();
//        String assignedTo = assignedToTextField.getText();
//        boolean completed = completedCheckBox.selectedProperty().getValue();
//        
//        // IF WE ARE EDITING
//        RegioVincoData data = (RegioVincoData)app.getDataComponent();
//        if (editing) {
//            if (data.isValidToDoItemEdit(itemToEdit, category, description, startDate, endDate, completed)) {
//                itemToEdit.setCategory(category);
//                itemToEdit.setDescription(description);
//                itemToEdit.setStartDate(startDate);
//                itemToEdit.setEndDate(endDate);
//                itemToEdit.setAssignedTo(assignedTo);
//                itemToEdit.setCompleted(completed);
//            }
//            else {
//                // OPEN MESSAGE DIALOG EXPLAINING WHAT WENT WRONG
//                // @todo
//            }
//        }
//        // IF WE ARE ADDING
//        else {
//            if (data.isValidNewToDoItem(category, description, startDate, endDate, completed)) {
//                this.makeNewItem();
//            }
//            else {
//                // OPEN MESSAGE DIALOG EXPLAINING WHAT WENT WRONG
//                // @todo
//            }
//        }
//        
//        // CLOSE THE DIALOG
//        this.hide();
//    }

//    public void showAddDialog() {        
//        // USE THE TEXT IN THE HEADER FOR ADD
//        PropertiesManager props = PropertiesManager.getPropertiesManager();
//        String headerText = props.getProperty(TDLM_ITEM_DIALOG_ADD_HEADER_TEXT);
//        headerLabel.setText(headerText);
//        setTitle(headerText);
//
//        // USE THE TEXT IN THE HEADER FOR ADD
//        categoryTextField.setText("");
//        descriptionTextField.setText("");
//        startDatePicker.setValue(LocalDate.now());
//        endDatePicker.setValue(LocalDate.now());
//        assignedToTextField.setText("");
//        completedCheckBox.selectedProperty().setValue(false);
//        
//        // WE ARE ADDING A NEW ONE, NOT EDITING
//        editing = false;
//        editItem = null;
//        
//        // AND OPEN THE DIALOG
//        showAndWait();
//    }

//    public void showEditDialog(RegioVincoItemPrototype initItemToEdit) {
//        // WE'LL NEED THIS FOR VALIDATION
//        itemToEdit = initItemToEdit;
//        
//        // USE THE TEXT IN THE HEADER FOR EDIT
//        PropertiesManager props = PropertiesManager.getPropertiesManager();
//        String headerText = props.getProperty(TDLM_ITEM_DIALOG_EDIT_HEADER_TEXT);
//        headerLabel.setText(headerText);
//        setTitle(headerText);
//        
//        // WE'LL ONLY PROCEED IF THERE IS A LINE TO EDIT
//        editing = true;
//        editItem = null;
//        
//        // USE THE TEXT IN THE HEADER FOR EDIT
//        categoryTextField.setText(itemToEdit.getCategory());
//        descriptionTextField.setText(itemToEdit.getDescription());
//        startDatePicker.setValue(itemToEdit.getStartDate());
//        endDatePicker.setValue(itemToEdit.getEndDate());
//        assignedToTextField.setText(itemToEdit.getAssignedTo());
//        completedCheckBox.selectedProperty().setValue(itemToEdit.isCompleted());
//               
//        // AND OPEN THE DIALOG
//        showAndWait();
//    }
    
//    public RegioVincoItemPrototype getNewItem() {
//        return newItem;
//    }
//    
//    public RegioVincoItemPrototype getEditItem() {
//        return editItem;
//    }
    
    public void showViewEditDialog() throws IOException {
        welcomeDialogImageView = new javafx.scene.image.ImageView();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
       try {
            String bannerFileName = props.getProperty(APP_BANNER);
            String bannerPath = props.getProperty(APP_PATH_IMAGES) + "/" + bannerFileName;
            File bannerFile = new File(bannerPath);
            BufferedImage bufferedImage = ImageIO.read(bannerFile);
            javafx.scene.image.Image bannerImage = SwingFXUtils.toFXImage(bufferedImage, null);
            welcomeDialogImageView.setImage(bannerImage);
        } catch (IOException ex) {
            ex.printStackTrace();
        }        
flagpane.getChildren().add(welcomeDialogImageView);
        String title = new String("View|Edit Subregion");
        headerLabel.setText(title);
        next.setText("Next");
subregionlabel.setText("Subregion");
capitalLabel.setText("Capital");
leaderlabel.setText("Leader's Name");
flaglabel.setText("Flag");
subregionTextField.setText("");
capitalTextField.setText("");
leaderTextField.setText("");

         showAndWait();
    }
}