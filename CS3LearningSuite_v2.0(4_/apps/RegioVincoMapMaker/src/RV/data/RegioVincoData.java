package RV.data;

import djf.components.AppDataComponent;
import djf.modules.AppGUIModule;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Bounds;
import javafx.scene.Cursor;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableView;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import RV.RegioVincoListMakerApp;
import static RV.RegioVincoPropertyType.MV_MAP_NAME;
import static RV.RegioVincoPropertyType.MV_MAP_PANE;
import static RV.RegioVincoPropertyType.MV_OUTER_MAP_PANE;
import static RV.RegioVincoPropertyType.TDLM_ITEMS_TABLE_VIEW;
import static RV.RegioVincoPropertyType.TDLM_NAME_TEXT_FIELD;
import static RV.RegioVincoPropertyType.TDLM_OWNER_TEXT_FIELD;
import static RV.RegioVincoPropertyType.TDLM_REGION_NAME_TEXT_FIELD;
import static RV.data.RegioVincoData.MapState.MAP_DRAG;
import static RV.data.RegioVincoData.MapState.MAP_SELECT;
import static RV.data.RegioVincoData.MapState.OUTERMAP_DRAG;
import static RV.data.RegioVincoData.MapState.OUTERMAP_SELECT;
import RV.workspace.controllers.ItemsController;
import static RV.workspace.style.TDLStyle.CLASS_MV_MAP_LAND;
import static RV.workspace.style.TDLStyle.CLASS_MV_MAP_MOUSE_OVER_LAND;
import javafx.scene.image.ImageView;

/**
 *
 * @author McKillaGorilla
 */
public class RegioVincoData implements AppDataComponent {
    RegioVincoListMakerApp app;
    ObservableList<RegioVincoItemPrototype> items;
    TableViewSelectionModel itemsSelectionModel;
//    StringProperty nameProperty;
//    StringProperty ownerProperty;
    Pane map;
    Pane outermap;
    String mapname;
    int subregionId;
    HashMap<Integer, ObservableList<Polygon>> subregions;
     ItemsController itemsController;
    WebEngine statsEngine;
    MapState currentState;
    final double DEFAULT_LINE_THICKNESS = 1.0;
    double tX, tY, sX, sY;
 double positionx;
  double positiony;
double size;
public int imagenumber;
ItemsController itemcontroller;
    public RegioVincoData(RegioVincoListMakerApp initApp) {
        app = initApp;
        subregions = new HashMap();
        map = (Pane)app.getGUIModule().getGUINode(MV_MAP_PANE);
        outermap = (Pane)app.getGUIModule().getGUINode(MV_OUTER_MAP_PANE);
        currentState = MapState.MAP_SELECT;
     itemsController = new ItemsController((RegioVincoListMakerApp)app);
itemcontroller = new ItemsController(app);
     
        // GET ALL THE THINGS WE'LL NEED TO MANIUPLATE THE TABLE
        TableView tableView = (TableView) app.getGUIModule().getGUINode(TDLM_ITEMS_TABLE_VIEW);
        items = tableView.getItems();
        itemsSelectionModel = tableView.getSelectionModel();
        itemsSelectionModel.setSelectionMode(SelectionMode.MULTIPLE);
        
        // AND FOR LIST NAME AND OWNER DATA
//        nameProperty = ((TextField)app.getGUIModule().getGUINode(TDLM_REGION_NAME_TEXT_FIELD)).textProperty();
//        ownerProperty = ((TextField)app.getGUIModule().getGUINode(TDLM_OWNER_TEXT_FIELD)).textProperty();
    }
    
    public void fitToRegion() {
        // GO THROUGH ALL THE POLYGONS TO FIND THE BOUNDS
        double minX = 5000;
        double minY = 5000;
        double maxX = -5000;
        double maxY = -5000;
        Iterator<Integer> idIt = subregions.keySet().iterator();
        while (idIt.hasNext()) {
            int id = idIt.next();
            ObservableList<Polygon> polygons = subregions.get(id);            
            Iterator<Polygon> polyIt = polygons.iterator();
            while (polyIt.hasNext()) {
                Polygon poly = polyIt.next();
                Bounds polyBounds = poly.getBoundsInLocal();
                if (polyBounds.getMinX() < minX)
                    minX = polyBounds.getMinX();
                if (polyBounds.getMaxX() > maxX)
                    maxX = polyBounds.getMaxX();
                if (polyBounds.getMinY() < minY)
                    minY = polyBounds.getMinY();
                if (polyBounds.getMaxY() > maxY)
                    maxY = polyBounds.getMaxY();
            }
        }
        double BUFFER_PERCENT = 0.05;
        minX -= (minX * BUFFER_PERCENT);
        maxX += (maxX * BUFFER_PERCENT);
        minY -= (minY * BUFFER_PERCENT);
        maxY += (maxY * BUFFER_PERCENT);
        
        
        // CALCULATE THE CENTER
        double centerX = (minX + maxX)/2.0;
        double centerY = (minY + maxY)/2.0f;
        double mapWidth = map.widthProperty().doubleValue();
        double mapHeight = map.heightProperty().doubleValue();
        double scaleX = mapWidth/(maxX - minX);
        double scaleY = mapHeight/(maxY - minY);
        double scale = scaleX;
        if (scaleY > scale)
            scale = scaleY;
        double factor = scale/map.scaleXProperty().doubleValue();
        
        // FIRST MOVE THE MAP SO IT'S CENTERED ON THE CENTER
        double translateX = (mapWidth/2) - centerX;
        double translateY = (mapHeight/2) - centerY;
        this.resetViewport();
        map.translateXProperty().setValue(translateX);
        map.translateYProperty().setValue(translateY);
//        this.zoomOnPoint(factor, centerX, centerY);
    }

    
     enum MapState{
        MAP_SELECT,
        MAP_DRAG,
        OUTERMAP_SELECT,
        OUTERMAP_DRAG,
    }

      public Polygon getMousedOverPolygon(double x, double y) {
        Iterator<Integer> idIt = subregions.keySet().iterator();
        while (idIt.hasNext()) {
            int id = idIt.next();
            ObservableList<Polygon> polygons = subregions.get(id);            
            Iterator<Polygon> polyIt = polygons.iterator();
            while (polyIt.hasNext()) {
                Polygon poly = polyIt.next();
                double localX = x - poly.getLayoutX();
                double localY = y - poly.getLayoutY();
                System.out.println("localX, localY: (" + localX + ", " + localY);
                if (poly.contains(x, y)) {
                    System.out.println("poly found");
                    return poly;
                }
            }
        }
        return null;
    }
      
       public double calcXPerc(double x) {
        double scale = map.scaleXProperty().doubleValue();
        double translateX = map.translateXProperty().doubleValue();
        double xMax = (scale-1.0)*(map.getWidth()/2.0);
        double leftX = (xMax - translateX)/scale;
        double percentX = (x-leftX)/(map.getWidth()/scale);
        return percentX;        
    }
    
    public double calcYPerc(double y) {
        double scale = map.scaleXProperty().doubleValue();
        double translateY = map.translateYProperty().doubleValue();
        double yMax = (scale-1.0)*(map.getWidth()/2.0);
        double leftY = (yMax - translateY)/scale;
        double percentY = (y-leftY)/(map.getHeight()/scale);
        return percentY;        
    }
    
    public ObservableList<Polygon> getSubregion(int id) {
        return subregions.get(id);
    }
    
     public void addSubregion(ArrayList<ArrayList<Double>> rawPolygons) {
        ObservableList<Polygon> subregionPolygons = FXCollections.observableArrayList();
        for (int i = 0; i < rawPolygons.size(); i++) {
            ArrayList<Double> rawPolygonPoints = rawPolygons.get(i);
            Polygon polygonToAdd = new Polygon();
            ObservableList<Double> transformedPolygonPoints = polygonToAdd.getPoints();
            for (int j = 0; j < rawPolygonPoints.size(); j+=2) {
                double longX = rawPolygonPoints.get(j);
                double latY = rawPolygonPoints.get(j+1);
                double x = longToX(longX);
                double y = latToY(latY);
                transformedPolygonPoints.addAll(x, y);
            }
            subregionPolygons.add(polygonToAdd);
            polygonToAdd.getStyleClass().add(CLASS_MV_MAP_LAND);
            polygonToAdd.setStroke(Color.BLACK);
            polygonToAdd.setStrokeWidth(DEFAULT_LINE_THICKNESS);
            polygonToAdd.setUserData(subregionId);
            map.getChildren().add(polygonToAdd);
        }
        subregions.put(subregionId, subregionPolygons);
        subregionId++;   
    }
     
    
             
     
//     public void addImageOnMap() {
//         
//         map.getChildren().add(map);
//     }
    
       public double longToX(double longCoord) {
        double paneHeight = map.getHeight();
        double unitDegree = paneHeight/180;
        double newLongCoord = (longCoord + 180) * unitDegree;
        return newLongCoord;
    }

    /**
     * This calculates and returns the y pixel value that corresponds to the
     * yCoord latitude argument.
     */
    public double latToY(double latCoord) {
        // DEFAULT WILL SCALE TO THE HEIGHT OF THE MAP PANE
        double paneHeight = map.getHeight();
        
        // WE ONLY WANT POSITIVE COORDINATES, SO SHIFT BY 90
        double unitDegree = paneHeight/180;
        double newLatCoord = (latCoord + 90) * unitDegree;
        return paneHeight - newLatCoord;
    }

    double startX;
    double startY;
     
      public void resetLocation() {
        map.translateXProperty().set(0);
        map.translateYProperty().set(0);
    }    

    public void resetViewport() {
        scaleMap(1.0);
        moveMap(0, 0);
    }

    private void scaleMap(double zoomScale) {
        map.scaleXProperty().setValue(zoomScale);
        map.scaleYProperty().setValue(zoomScale);
    }
      private void outerscaleMap(double zoomScale) {
        outermap.scaleXProperty().setValue(zoomScale);
        outermap.scaleYProperty().setValue(zoomScale);
    }
    
    private void moveMap(double x, double y) {
        map.translateXProperty().set(x);
        map.translateYProperty().set(y);
    }
    
    private void outermoveMap(double x, double y) {
        outermap.translateXProperty().set(x);
        outermap.translateYProperty().set(y);
    }
    
     public void zoom(double zoomInc) {
        double scale = map.scaleXProperty().doubleValue();
        double translateX = map.translateXProperty().doubleValue();
        double translateY = map.translateYProperty().doubleValue();
        if ((scale * zoomInc) >= 1.0) {
            scale *= zoomInc;
            moveMap(0,0);
            scaleMap(scale);
            translateX *= zoomInc;
            translateY *= zoomInc;
            moveMap(translateX, translateY);
            clamp();
            adjustLineThickness();
        }
    }
     
     public void outerzoom(double zoomInc) {
        double scale = outermap.scaleXProperty().doubleValue();
        double translateX = outermap.translateXProperty().doubleValue();
        double translateY = outermap.translateYProperty().doubleValue();
        if ((scale * zoomInc) >= 1.0) {
            scale *= zoomInc;
            outermoveMap(0,0);
            scaleMap(scale);
            translateX *= zoomInc;
            translateY *= zoomInc;
            outermoveMap(translateX, translateY);
            clamp();
//            adjustLineThickness();
        }
    }
     

      public void clamp() {
        // FIRST CLAMP X
        double scale = map.scaleXProperty().doubleValue();
        double xMax = (scale-1.0)*(map.getWidth()/2.0);
        double xTranslate = map.translateXProperty().doubleValue();
        if (xTranslate > xMax) xTranslate = xMax;
        //else if (xTranslate < adjustedX) xTranslate = adjustedX;
        map.translateXProperty().setValue(xTranslate);

        // THEN Y
        double yMax = (scale-1.0)*(map.getHeight()/2.0);
        double yTranslate = map.translateYProperty().doubleValue();
        if (yTranslate > yMax) yTranslate = yMax;
        map.translateYProperty().setValue(yTranslate);
    }
      
      public void outerclamp() {
           double scale = outermap.scaleXProperty().doubleValue();
        double xMax = (scale-1.0)*(outermap.getWidth()/2.0);
        double xTranslate = outermap.translateXProperty().doubleValue();
        if (xTranslate > xMax) xTranslate = xMax;
        //else if (xTranslate < adjustedX) xTranslate = adjustedX;
        outermap.translateXProperty().setValue(xTranslate);

        // THEN Y
        double yMax = (scale-1.0)*(outermap.getHeight()/2.0);
        double yTranslate = outermap.translateYProperty().doubleValue();
        if (yTranslate > yMax) yTranslate = yMax;
        outermap.translateYProperty().setValue(yTranslate);
      }
    
    public void adjustLineThickness() {
        double scale = map.scaleXProperty().doubleValue();
        Iterator<Integer> idIt = subregions.keySet().iterator();
        while (idIt.hasNext()) {
            int id = idIt.next();
            ObservableList<Polygon> polygons = subregions.get(id);
            Iterator<Polygon> polyIt = polygons.iterator();
            while (polyIt.hasNext()) {
                Polygon poly = polyIt.next();
                poly.setStrokeWidth(DEFAULT_LINE_THICKNESS/scale);
            }
        }
    }
      public void move(double xInc, double yInc) {
        // FIRST X, WITH CLAMPING AT THE EDGES
        double xTranslate = map.translateXProperty().doubleValue() + xInc;
        map.translateXProperty().setValue(xTranslate);

        double yTranslate = map.translateYProperty().doubleValue() + yInc;
        map.translateYProperty().setValue(yTranslate);
        
        // MAKE SURE WE'RE NOT OUT OF BOUNDS
        clamp();
    }
      
      public void outermove(double xInc, double yInc) {
        // FIRST X, WITH CLAMPING AT THE EDGES
        double xTranslate = outermap.translateXProperty().doubleValue() + xInc;
        outermap.translateXProperty().setValue(xTranslate);
        outermap.getChildren().get(1).setTranslateX(xTranslate);
        
        double yTranslate = outermap.translateYProperty().doubleValue() + yInc;
        outermap.translateYProperty().setValue(yTranslate);
        outermap.getChildren().get(1).setTranslateY(yTranslate);
        
        // MAKE SURE WE'RE NOT OUT OF BOUNDS
        outerclamp();
    }
    
       public double xToScaledX(double x) {
        double scale = map.scaleXProperty().doubleValue();
        double translateX = map.translateXProperty().doubleValue();
        double diffX = x - translateX;        
        return diffX/scale;
    }
    
    public double yToScaledY(double y) {
        double scale = map.scaleXProperty().doubleValue();
        double translateY = map.translateYProperty().doubleValue();
        double diffY = y - translateY;
        return diffY/scale;
    }
    
    public void startMapDrag(int x, int y) {
        startX = x;
        startY = y;
        currentState = MAP_DRAG;
        app.getGUIModule().getWindow().getScene().setCursor(Cursor.MOVE);
    }
    
    public void startOuterMapDrag(int x, int y) {
        startX = x;
        startY = y;
        currentState = OUTERMAP_DRAG;
        app.getGUIModule().getWindow().getScene().setCursor(Cursor.HAND);
        imagenumber = outermap.getChildren().indexOf(this.getImageView());
    }
     public void updateMapDrag(double x, double y) {
        if (currentState == MAP_DRAG) {
            double diffX = x - startX;
            double diffY = y - startY;
            this.move(diffX, diffY);
        }
        }
     
     public void reupdateMapDrag(double x, double y) {
         
     }
        public void updateOuterMapDrag(double x, double y) {
     if (currentState == OUTERMAP_DRAG) {
            double diffX = x - startX;
            double diffY = y - startY;
            this.outermove(diffX, diffY);
        }

     }

    public void endMapDrag(int x, int y) {
        currentState = MAP_SELECT;
        app.getGUIModule().getWindow().getScene().setCursor(Cursor.DEFAULT);
    }
    public void endOuterMapDrag(int x, int y) {
         currentState = OUTERMAP_SELECT;
        app.getGUIModule().getWindow().getScene().setCursor(Cursor.DEFAULT);
         imagenumber = outermap.getChildren().indexOf(this.getImageView());
    }

    public void zoomInOnPoint(double x, double y) {
        zoomOnPoint(2.0, x, y);
    }

    public void zoomOutOnPoint(double x, double y) {
        zoomOnPoint(0.5, x, y);
    }
     public void outerzoomInOnPoint(double x, double y) {
        outerzoomOnPoint(2.0, x, y);
    }

    public void outerzoomOutOnPoint(double x, double y) {
        outerzoomOnPoint(0.5, x, y);
    }
  
    private void zoomOnPoint(double factor, double mouseX, double mouseY) {
        double scale = map.scaleXProperty().doubleValue();
        double newScale = factor * scale;
        if (newScale >= 1.0) {
//            updateStats(mouseX, mouseY);
            double diffX =(viewportMousePercentX * (viewportWidth/newScale));
            double diffY = (viewportMousePercentY * (viewportHeight/newScale));
            double newWorldViewportX = worldMouseX - diffX;
            double newWorldViewportY = worldMouseY - diffY;
            double newWorldViewportPaddingLeft = (newScale-1.0)*(viewportWidth/2.0);
            double newWorldViewportPaddingTop = (newScale-1.0)*(viewportHeight/2.0);
            viewportTranslateX = newWorldViewportPaddingLeft - (newWorldViewportX * newScale);
            viewportTranslateY = newWorldViewportPaddingTop - (newWorldViewportY * newScale);

            zoom(factor);
            map.translateXProperty().setValue(viewportTranslateX);
            map.translateYProperty().setValue(viewportTranslateY);
            this.update(mouseX, mouseY);
        }
    }
    
    private void outerzoomOnPoint(double factor, double mouseX, double mouseY) {
        double scale = outermap.scaleXProperty().doubleValue();
        double newScale = factor * scale;
        if (newScale >= 1.0) {
//            updateStats(mouseX, mouseY);
            double diffX =(viewportMousePercentX * (viewportWidth/newScale));
            double diffY = (viewportMousePercentY * (viewportHeight/newScale));
            double newWorldViewportX = worldMouseX - diffX;
            double newWorldViewportY = worldMouseY - diffY;
            double newWorldViewportPaddingLeft = (newScale-1.0)*(viewportWidth/2.0);
            double newWorldViewportPaddingTop = (newScale-1.0)*(viewportHeight/2.0);
            viewportTranslateX = newWorldViewportPaddingLeft - (newWorldViewportX * newScale);
            viewportTranslateY = newWorldViewportPaddingTop - (newWorldViewportY * newScale);

            zoom(factor);
            outermap.translateXProperty().setValue(viewportTranslateX);
            outermap.translateYProperty().setValue(viewportTranslateY);
            this.update(mouseX, mouseY);
        }
    }
    
     Polygon mousedOverPolygon;
    public void highlightPolygon(double mouseX, double mouseY) {        Polygon poly = this.getMousedOverPolygon(mouseX, mouseY);
        if (poly != null) {
            if (mousedOverPolygon != null) {         
                mousedOverPolygon.getStyleClass().remove(CLASS_MV_MAP_MOUSE_OVER_LAND);
                mousedOverPolygon.getStyleClass().add(CLASS_MV_MAP_LAND);
            }
            mousedOverPolygon = poly;
            mousedOverPolygon.getStyleClass().remove(CLASS_MV_MAP_LAND);
            mousedOverPolygon.getStyleClass().add(CLASS_MV_MAP_MOUSE_OVER_LAND);
        }
        else if (mousedOverPolygon != null) { 
            mousedOverPolygon.getStyleClass().remove(CLASS_MV_MAP_MOUSE_OVER_LAND);
            mousedOverPolygon.getStyleClass().add(CLASS_MV_MAP_LAND);
            mousedOverPolygon = null;    
        }
    }
    double viewportWidth, viewportHeight;
    double viewportTranslateX, viewportTranslateY;
    double viewportMousePercentX, viewportMousePercentY;
    
    // THESE ARE VALUES USING WORLD COORDINATES
    double worldWidth, worldHeight;
    double worldMouseX, worldMouseY;
    double worldViewportWidth, worldViewportHeight;
    double worldViewportPaddingLeft, worldViewportPaddingTop;
    double worldViewportX, worldViewportY;
    
     private void updateStats(double mouseX, double mouseY) {
        double scale = map.scaleXProperty().doubleValue();
        viewportWidth = map.widthProperty().doubleValue();
        viewportHeight = map.heightProperty().doubleValue();
        viewportTranslateX = map.translateXProperty().doubleValue();
        viewportTranslateY = map.translateYProperty().doubleValue();
        
        worldWidth = map.heightProperty().doubleValue()*2.0;
        worldHeight = map.heightProperty().doubleValue();
        worldMouseX = mouseX;
        worldMouseY = mouseY;
        worldViewportWidth = viewportWidth/scale;
        worldViewportHeight = viewportHeight/scale;
        worldViewportPaddingLeft = (scale-1.0)*(map.getWidth()/2.0);
        worldViewportPaddingTop = (scale-1.0)*(map.getHeight()/2.0);
        worldViewportX = (worldViewportPaddingLeft - viewportTranslateX)/scale;
        worldViewportY = (worldViewportPaddingTop - viewportTranslateY)/scale;
        
        viewportMousePercentX = (worldMouseX - worldViewportX)/worldViewportWidth;
        viewportMousePercentY = (worldMouseY - worldViewportY)/worldViewportHeight;
    }
     
     private void outerupdateStats(double mouseX, double mouseY) {
        double scale = outermap.scaleXProperty().doubleValue();
        viewportWidth = outermap.widthProperty().doubleValue();
        viewportHeight = outermap.heightProperty().doubleValue();
        viewportTranslateX = outermap.translateXProperty().doubleValue();
        viewportTranslateY = outermap.translateYProperty().doubleValue();
        
        worldWidth = outermap.heightProperty().doubleValue()*2.0;
        worldHeight = outermap.heightProperty().doubleValue();
        worldMouseX = mouseX;
        worldMouseY = mouseY;
        worldViewportWidth = viewportWidth/scale;
        worldViewportHeight = viewportHeight/scale;
        worldViewportPaddingLeft = (scale-1.0)*(map.getWidth()/2.0);
        worldViewportPaddingTop = (scale-1.0)*(map.getHeight()/2.0);
        worldViewportX = (worldViewportPaddingLeft - viewportTranslateX)/scale;
        worldViewportY = (worldViewportPaddingTop - viewportTranslateY)/scale;
        
        viewportMousePercentX = (worldMouseX - worldViewportX)/worldViewportWidth;
        viewportMousePercentY = (worldMouseY - worldViewportY)/worldViewportHeight;
    }
    
//    public String getName() {
//        return nameProperty.getValue();
//    }
    
//    public String getPath() {
//        return path.getValue();
//    }
//    
//    public void setPath(String pathtogo) {
//        path.setValue(pathtogo);
//    }
    
    public ItemsController getItemsController() {
        return itemcontroller;
    }
    
//    public String getOwner() {
//        return ownerProperty.getValue();
//    }
//    
    public Iterator<RegioVincoItemPrototype> itemsIterator() {
        return this.items.iterator();
    }
    
//    public void setName(String initName) {
//        nameProperty.setValue(initName);
//    }
    
//    public void setOwner(String initOwner) {
//        ownerProperty.setValue(initOwner);
//    }

    @Override
    public void reset() {
        // CLEAR THE DATA
        subregions.clear();
        subregionId = 0;
        
        // AND THE POLYGONS THEMSELVES
        Rectangle ocean = (Rectangle)map.getChildren().get(0);
        map.getChildren().clear();
        map.getChildren().add(ocean);
        for(int i =1; i < outermap.getChildren().size(); i++) {
            outermap.getChildren().remove(i);
        }
    }

     public void update(double mouseX, double mouseY) {
        updateStats(mouseX, mouseY);
        highlightPolygon(mouseX, mouseY);
        
    }
     
     public void outerupdate(double mouseX, double mouseY) {
          updateStats(mouseX, mouseY);
     }

    public boolean isItemSelected() {
        ObservableList<RegioVincoItemPrototype> selectedItems = this.getSelectedItems();
        return (selectedItems != null) && (selectedItems.size() == 1);
    }
    
    public boolean areItemsSelected() {
        ObservableList<RegioVincoItemPrototype> selectedItems = this.getSelectedItems();
        return (selectedItems != null) && (selectedItems.size() > 1);        
    }

    public boolean isValidToDoItemEdit(RegioVincoItemPrototype itemToEdit, String subregion, String capital, String leader) {
        return isValidNewToDoItem(subregion, capital, leader);
    }

    public boolean isValidNewToDoItem(String subregion, String capital, String leader) {
        if (subregion.trim().length() == 0)
            return false;
        if (capital.trim().length() == 0)
            return false;
        if (leader.trim().length() == 0)
            return false;
        return true;
    }

    public void addItem(RegioVincoItemPrototype itemToAdd) {
        items.add(itemToAdd);
    }
    
    public void snpaImagebottomleft(ImageView imv) {
       imv.setX(0);
        imv.setY(app.getGUIModule().getWindow().getHeight());
    }
    public void snpaImageoriginal(ImageView imv) {
          imv.setX(0);
        imv.setY(0);
        
    }
    

    public void removeItem(RegioVincoItemPrototype itemToAdd) {
        items.remove(itemToAdd);
    }

    public RegioVincoItemPrototype getSelectedItem() {
        if (!isItemSelected()) {
            return null;
        }
        return getSelectedItems().get(0);
    }
    public ObservableList<RegioVincoItemPrototype> getSelectedItems() {
        return (ObservableList<RegioVincoItemPrototype>)this.itemsSelectionModel.getSelectedItems();
    }
    public ImageView getLastImage() {
        return (ImageView)outermap.getChildren().get(outermap.getChildren().size()-1);
    }

    public int getItemIndex(RegioVincoItemPrototype item) {
        return items.indexOf(item);
    }

    public void addItemAt(RegioVincoItemPrototype item, int itemIndex) {
        items.add(itemIndex, item);
    }

    public void moveItem(int oldIndex, int newIndex) {
        RegioVincoItemPrototype itemToMove = items.remove(oldIndex);
        items.add(newIndex, itemToMove);
    }

    public int getNumItems() {
        return items.size();
    }

    public void selectItem(RegioVincoItemPrototype itemToSelect) {
        this.itemsSelectionModel.select(itemToSelect);
    }

    public ArrayList<Integer> removeAll(ArrayList<RegioVincoItemPrototype> itemsToRemove) {
        ArrayList<Integer> itemIndices = new ArrayList();
        for (RegioVincoItemPrototype item: itemsToRemove) {
            itemIndices.add(items.indexOf(item));
        }
        for (RegioVincoItemPrototype item: itemsToRemove) {
            items.remove(item);
        }
        return itemIndices;
    }

    public void addAll(ArrayList<RegioVincoItemPrototype> itemsToAdd, ArrayList<Integer> addItemLocations) {
        for (int i = 0; i < itemsToAdd.size(); i++) {
            RegioVincoItemPrototype itemToAdd = itemsToAdd.get(i);
            Integer location = addItemLocations.get(i);
            items.add(location, itemToAdd);
        }
    }

    public ArrayList<RegioVincoItemPrototype> getCurrentItemsOrder() {
        ArrayList<RegioVincoItemPrototype> orderedItems = new ArrayList();
        for (RegioVincoItemPrototype item : items) {
            orderedItems.add(item);
        }
        return orderedItems;
    }

    public void clearSelected() {
        this.itemsSelectionModel.clearSelection();
    }

    public void sortItems(Comparator sortComparator) {
        Collections.sort(items, sortComparator);
    }

    public void rearrangeItems(ArrayList<RegioVincoItemPrototype> oldListOrder) {
        items.clear();
        for (RegioVincoItemPrototype item : oldListOrder) {
            items.add(item);
        }
    }
    
//    public boolean imageSelected() {
//        ImageView iv = getItemsController().getImageView();
//        return (iv != null);
//    }
     

    

    public double returnxtolong(double mapX){

        double paneHeight = map.getHeight();

        double unitDegree = paneHeight/180;

        double longcoord = mapX/unitDegree - 180;

        

        return longcoord;

    }
    
     public double returnytolong(double mapcorY){
        double paneHeight = map.getHeight();
           double unitDegree = paneHeight/180;
        double latcoord = (paneHeight - mapcorY)/unitDegree - 90;
        return latcoord;
    }

        public double getTransLateX(){
        return tX;

    }

    public double getTransLateY(){
        return tY;

    }
      public double getScaleX(){
        return sX;

    }

    public double getScaleY(){
        return sY;

    }  public void translate(double initTranslateX, double initTranslateY){
        tX = initTranslateX;
        tY = initTranslateY;
        map.setTranslateX(tX);
        map.setTranslateY(tY);
        map.relocate(tX, tY);
    }
    
    public void scale(double initScalex, double initScaley) {
        sX = initScalex;
        sY = initScaley;
        map.setScaleX(sX);
        map.setScaleY(sY);
    }
     public int polygonsize() {
     return subregions.size();
 }
     
     public Pane getmapPane() {
        return map;
    }
     
     public Pane getouterpane() {
         return outermap;
     }
     
     public String getMapName() {
         return mapname;
     }
     public ImageView getImageView() {
         return itemcontroller.getImageView();
     }
     public boolean imageselect() {
         return this.getouterpane().getChildren().size() > 1;
     }
//     public String getImageName() {
//         ItemsController it = new ItemsController(app);
//         String id = it.returnimage().getImage().
//         return id;
//         
//     }
     
//     public String getPath() {
//       
//     }
     
//        public String getFileName() {
//        String path = outermap.
//        return path;
//    }
     
}