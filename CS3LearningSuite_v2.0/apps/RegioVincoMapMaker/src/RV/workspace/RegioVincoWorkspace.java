package RV.workspace;

import static djf.AppPropertyType.HAS_CLIPBOARD_TOOLBAR;
import static djf.AppPropertyType.HAS_FILE_TOOLBAR;
import static djf.AppPropertyType.HAS_HELP_TOOLBAR;
import static djf.AppPropertyType.HAS_TOP_TOOLBAR;
import static djf.AppPropertyType.HAS_UNDO_TOOLBAR;
import static djf.AppPropertyType.LOAD_BUTTON;
import static djf.AppPropertyType.NEW_BUTTON;
import static djf.AppPropertyType.REDO_BUTTON;
import static djf.AppPropertyType.UNDO_BUTTON;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import djf.modules.AppFoolproofModule;
import djf.modules.AppGUIModule;
import static djf.modules.AppGUIModule.DISABLED;
import static djf.modules.AppGUIModule.ENABLED;
import static djf.modules.AppGUIModule.FOCUS_TRAVERSABLE;
import static djf.modules.AppGUIModule.HAS_KEY_HANDLER;
import static djf.modules.AppGUIModule.NOT_FOCUS_TRAVERSABLE;
import static djf.modules.AppGUIModule.NO_KEY_HANDLER;
import djf.ui.AppNodesBuilder;
import djf.ui.controllers.AppFileController;
import djf.ui.dialogs.AppDialogsFacade;
import static djf.ui.style.DJFStyle.CLASS_DJF_ICON_BUTTON;
import java.awt.Checkbox;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.SortEvent;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.SortType;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import properties_manager.PropertiesManager;
import RV.RegioVincoListMakerApp;
import static RV.RegioVincoPropertyType.BORDER_THICKNESS;
import static RV.RegioVincoPropertyType.COLOR_PICKER;
import static RV.RegioVincoPropertyType.EXIT_BUTTON;
import static RV.RegioVincoPropertyType.FILE_EXPORT_BUTTON;
import static RV.RegioVincoPropertyType.FLOPPY_SAVE_BUTTON;
import static RV.RegioVincoPropertyType.FOLDER_NEW_BUTTON;
import static RV.RegioVincoPropertyType.FOLDER_OPEN_BUTTON;
import static RV.RegioVincoPropertyType.MV_MAP_PANE;
import static RV.RegioVincoPropertyType.TDLM_FOOLPROOF_SETTINGS;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_BIG_HEADER;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_BUTTON;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_COLUMN;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_PANE;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_PROMPT;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_SMALL_HEADER;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_TABLE;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_TEXT_FIELD;
import static RV.RegioVincoPropertyType.TDLM_PANE;
import static RV.RegioVincoPropertyType.TDLM_TO_DO_LIST_LABEL;
import static RV.RegioVincoPropertyType.TDLM_NAME_PANE;
import static RV.RegioVincoPropertyType.TDLM_NAME_LABEL;
import static RV.RegioVincoPropertyType.TDLM_NAME_TEXT_FIELD;
import static RV.RegioVincoPropertyType.TDLM_OWNER_PANE;
import static RV.RegioVincoPropertyType.TDLM_OWNER_LABEL;
import static RV.RegioVincoPropertyType.TDLM_OWNER_TEXT_FIELD;
import static RV.RegioVincoPropertyType.TDLM_ITEMS_PANE;
import static RV.RegioVincoPropertyType.TDLM_ITEM_BUTTONS_PANE;
import static RV.RegioVincoPropertyType.TDLM_ADD_ITEM_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_ASSIGNED_TO_COLUMN;
import static RV.RegioVincoPropertyType.TDLM_BORDER_COLOR_LABEL;
import static RV.RegioVincoPropertyType.TDLM_BORDER_THICKNESS_LABEL;
import static RV.RegioVincoPropertyType.TDLM_CAPITAL_COLUMN;
import static RV.RegioVincoPropertyType.TDLM_CATEGORY_COLUMN;
import static RV.RegioVincoPropertyType.TDLM_COMPLETED_COLUMN;
import static RV.RegioVincoPropertyType.TDLM_DESCRIPTION_COLUMN;
import static RV.RegioVincoPropertyType.TDLM_DOWN_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_EDIT_ITEM_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_END_DATE_COLUMN;
import static RV.RegioVincoPropertyType.TDLM_FIFTH_PANE;
import static RV.RegioVincoPropertyType.TDLM_FIRST_PANE;
import static RV.RegioVincoPropertyType.TDLM_FIT_TO_REGION_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_FOURTH_PANE;
import static RV.RegioVincoPropertyType.TDLM_FRAME_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_GRID_PANE;
import static RV.RegioVincoPropertyType.TDLM_IMAGE_ADD_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_IMAGE_REMOVE_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_REMOVE_ITEM_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_MOVE_UP_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_MOVE_DOWN_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_ITEMS_TABLE_VIEW;
import static RV.RegioVincoPropertyType.TDLM_LEADER_COLUMN;
import static RV.RegioVincoPropertyType.TDLM_NAME_OWNER_PANE;
import static RV.RegioVincoPropertyType.TDLM_POLYGON;
import static RV.RegioVincoPropertyType.TDLM_POLYGON_LABEL;
import static RV.RegioVincoPropertyType.TDLM_RANDOM_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_RESIZE_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_SECOND_PANE;
import static RV.RegioVincoPropertyType.TDLM_SNAP_IMAGE_BOTTOMLEFT_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_SNAP_IMAGE_TOPLEFT_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_START_DATE_COLUMN;
import static RV.RegioVincoPropertyType.TDLM_SUBREGION_COLUMN;
import static RV.RegioVincoPropertyType.TDLM_THIRD_PANE;
import static RV.RegioVincoPropertyType.TDLM_TITLE_LABEL;
import static RV.RegioVincoPropertyType.TDLM_TOGGLE;
import static RV.RegioVincoPropertyType.TDLM_TOGGLE_LABEL;
import static RV.RegioVincoPropertyType.TDLM_UP_BUTTON;
import static RV.RegioVincoPropertyType.TDLM_VIEWPORT_BUTTON;
import RV.workspace.controllers.ItemsController;
import RV.workspace.controllers.ItemsTableController;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_BOX;
import RV.data.RegioVincoData;
import RV.data.RegioVincoItemPrototype;
import RV.workspace.foolproof.RegioVincoSelectionFoolproofDesign;
import RV.transactions.SortItems_Transaction;
import static RV.workspace.style.TDLStyle.CLASS_MV_MAP_OCEAN;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_FILE_BUTTON;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_FILE_TOOLBAR;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_GRID_PANE;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_MAP_TOOLBAR;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_NAVIGATION_BUTTON;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_TITLE_PROMPT;
import static RV.workspace.style.TDLStyle.CLASS_TDLM_TOOLBAR1;

/**
 *
 * @author McKillaGorilla
 */
public class RegioVincoWorkspace extends AppWorkspaceComponent {
protected FlowPane topToolbarPane;

    // THIS IS THE FILE TOOLBAR AND ITS CONTROLS
    protected ToolBar fileToolbar;

    // THIS IS FOR THE CUT/COPY/PASTE BUTTONS IF WE'RE USING THEM
    protected ToolBar clipboardToolbar;

    // THIS IS FOR THE UNDO/REDO BUTTONS IF WE'RE USING THEM
    protected ToolBar undoToolbar;

    // THIS IS FOR THE HELP/LANGUAGE/ABOUT BUTTONS IF WE'RE USING THEM
    protected ToolBar helpToolbar;

    protected ToolBar helpToolbar2;
   
    public RegioVincoWorkspace(RegioVincoListMakerApp app) {
        super(app);

        // LAYOUT THE APP
        initLayout();
        
        // 
        initFoolproofDesign();
    }
        
    // THIS HELPER METHOD INITIALIZES ALL THE CONTROLS IN THE WORKSPACE
    private void initLayout() {
        // FIRST LOAD THE FONT FAMILIES FOR THE COMBO BOX
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // THIS WILL BUILD ALL OF OUR JavaFX COMPONENTS FOR US
        AppNodesBuilder tdlBuilder = app.getGUIModule().getNodesBuilder();
        
	// THIS HOLDS ALL THE CONTROLS IN THE WORKSPACE
        
        
        
       
	VBox toDoListPane           = tdlBuilder.buildVBox(TDLM_PANE,               null,           null,   CLASS_TDLM_BOX, HAS_KEY_HANDLER,             FOCUS_TRAVERSABLE,      ENABLED);
       
        HBox firstPane          = tdlBuilder.buildHBox( TDLM_PANE,    toDoListPane,    null,   CLASS_TDLM_FILE_TOOLBAR, HAS_KEY_HANDLER,            FOCUS_TRAVERSABLE,      ENABLED);
        HBox filePane= tdlBuilder.buildHBox( CLASS_TDLM_FILE_TOOLBAR,    firstPane,    null,   CLASS_TDLM_FILE_TOOLBAR, HAS_KEY_HANDLER,            FOCUS_TRAVERSABLE,      ENABLED);
        Button newButton = tdlBuilder.buildIconButton(FOLDER_NEW_BUTTON, filePane, null, CLASS_TDLM_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
 Button loadButton = tdlBuilder.buildIconButton(FOLDER_OPEN_BUTTON, filePane, null, CLASS_TDLM_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
       Button saveButton = tdlBuilder.buildIconButton(FLOPPY_SAVE_BUTTON, filePane, null, CLASS_TDLM_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
 Button exportButton = tdlBuilder.buildIconButton(FILE_EXPORT_BUTTON, filePane, null, CLASS_TDLM_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
 Button exitButton = tdlBuilder.buildIconButton(EXIT_BUTTON, filePane, null, CLASS_TDLM_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
 Button undoButton = tdlBuilder.buildIconButton(UNDO_BUTTON, filePane, null, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, DISABLED);
         Button redoButton = tdlBuilder.buildIconButton(REDO_BUTTON, filePane, null, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, DISABLED);

 
        HBox navigationPane= tdlBuilder.buildHBox( CLASS_TDLM_FILE_TOOLBAR,    firstPane,    null,   CLASS_TDLM_FILE_TOOLBAR, HAS_KEY_HANDLER,            FOCUS_TRAVERSABLE,      ENABLED);
       Button resetViewportButton  = tdlBuilder.buildIconButton( TDLM_VIEWPORT_BUTTON,   navigationPane,       null,   CLASS_TDLM_BUTTON,        HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  ENABLED);
        Button fitToRegionButton      = tdlBuilder.buildIconButton( TDLM_FIT_TO_REGION_BUTTON,      navigationPane,       null,   CLASS_TDLM_BUTTON,        HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  ENABLED);
       
         HBox polygonPane= tdlBuilder.buildHBox(CLASS_TDLM_FILE_TOOLBAR,    firstPane,    null,   CLASS_TDLM_FILE_TOOLBAR, HAS_KEY_HANDLER,            FOCUS_TRAVERSABLE,      ENABLED);
         Button resizeButton      = tdlBuilder.buildIconButton(TDLM_RESIZE_BUTTON,      polygonPane,       null,   CLASS_TDLM_BUTTON,        HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  ENABLED);
         Button addimageButton      = tdlBuilder.buildIconButton(TDLM_IMAGE_ADD_BUTTON,      polygonPane,       null,   CLASS_TDLM_BUTTON,        HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  ENABLED);
         Button removeimageButton      = tdlBuilder.buildIconButton(TDLM_IMAGE_REMOVE_BUTTON,      polygonPane,       null,   CLASS_TDLM_BUTTON,        HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  ENABLED);
         Button snapimagetopleftButton      = tdlBuilder.buildIconButton(TDLM_SNAP_IMAGE_TOPLEFT_BUTTON,      polygonPane,       null,   CLASS_TDLM_BUTTON,        HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  ENABLED);
         Button snapimagebottomleftButton      = tdlBuilder.buildIconButton(TDLM_SNAP_IMAGE_BOTTOMLEFT_BUTTON,      polygonPane,       null,   CLASS_TDLM_BUTTON,        HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  ENABLED);
         Button randomButton      = tdlBuilder.buildIconButton(TDLM_RANDOM_BUTTON,      polygonPane,       null,   CLASS_TDLM_NAVIGATION_BUTTON,        HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  ENABLED);
         Button frameButton      = tdlBuilder.buildIconButton(TDLM_FRAME_BUTTON,      polygonPane,       null,   CLASS_TDLM_NAVIGATION_BUTTON,        HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  ENABLED);

         
          HBox mapDealPane= tdlBuilder.buildHBox( CLASS_TDLM_FILE_TOOLBAR,    firstPane,    null,   CLASS_TDLM_FILE_TOOLBAR, HAS_KEY_HANDLER,            FOCUS_TRAVERSABLE,      ENABLED);
         Label toggle = tdlBuilder.buildLabel(TDLM_TOGGLE_LABEL,         mapDealPane,       null,   CLASS_TDLM_PROMPT, HAS_KEY_HANDLER,           FOCUS_TRAVERSABLE,      ENABLED);
         CheckBox togglebox = tdlBuilder.buildCheckBox(TDLM_TOGGLE, mapDealPane, null, CLASS_TDLM_MAP_TOOLBAR, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
         Label polygon = tdlBuilder.buildLabel(TDLM_POLYGON_LABEL,         mapDealPane,       null,   CLASS_TDLM_PROMPT, HAS_KEY_HANDLER,           FOCUS_TRAVERSABLE,      ENABLED);
         CheckBox polygonbox = tdlBuilder.buildCheckBox(TDLM_POLYGON, mapDealPane, null, CLASS_TDLM_MAP_TOOLBAR, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
         
           HBox borderPane= tdlBuilder.buildHBox( CLASS_TDLM_FILE_TOOLBAR,    firstPane,    null,   CLASS_TDLM_FILE_TOOLBAR, HAS_KEY_HANDLER,            FOCUS_TRAVERSABLE,      ENABLED);
         Label bordercolor = tdlBuilder.buildLabel(TDLM_BORDER_COLOR_LABEL,         borderPane,       null,   CLASS_TDLM_PROMPT, HAS_KEY_HANDLER,           FOCUS_TRAVERSABLE,      ENABLED);
         ColorPicker bordercolorpicker = tdlBuilder.buildColorPicker(COLOR_PICKER, borderPane, null, CLASS_TDLM_MAP_TOOLBAR, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
                 Label borderthickness = tdlBuilder.buildLabel(TDLM_BORDER_THICKNESS_LABEL,         borderPane,       null,   CLASS_TDLM_PROMPT, HAS_KEY_HANDLER,           FOCUS_TRAVERSABLE,      ENABLED);
         Slider borderthicknessslider = tdlBuilder.buildSlider(BORDER_THICKNESS, borderPane, null, CLASS_TDLM_MAP_TOOLBAR, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED, 0, 0);
       
//         firstPane.setHgrow(toggle, Priority.NEVER);
//         HBox nameOwnerPane          = tdlBuilder.buildHBox(TDLM_NAME_OWNER_PANE,    toDoListPane,    null,   CLASS_TDLM_BOX, HAS_KEY_HANDLER,            FOCUS_TRAVERSABLE,      ENABLED);
         
        HBox namePane               = tdlBuilder.buildHBox(TDLM_NAME_PANE,          null,    null,   CLASS_TDLM_BOX, HAS_KEY_HANDLER,             FOCUS_TRAVERSABLE,      ENABLED);
        Label nameLabel             = tdlBuilder.buildLabel(TDLM_NAME_LABEL,         namePane,       null,   CLASS_TDLM_PROMPT, HAS_KEY_HANDLER,           FOCUS_TRAVERSABLE,      ENABLED);
        TextField nameTextField     = tdlBuilder.buildTextField(TDLM_NAME_TEXT_FIELD,    namePane,       null,   CLASS_TDLM_TEXT_FIELD, NO_KEY_HANDLER,       FOCUS_TRAVERSABLE,      ENABLED);
        HBox ownerPane              = tdlBuilder.buildHBox(TDLM_OWNER_PANE,         null,    null,   CLASS_TDLM_BOX, HAS_KEY_HANDLER,             FOCUS_TRAVERSABLE,      ENABLED);
        Label ownerLabel            = tdlBuilder.buildLabel(TDLM_OWNER_LABEL,        ownerPane,      null,   CLASS_TDLM_PROMPT, HAS_KEY_HANDLER,           FOCUS_TRAVERSABLE,      ENABLED);
        TextField ownerTextField    = tdlBuilder.buildTextField(TDLM_OWNER_TEXT_FIELD,   ownerPane,      null,   CLASS_TDLM_TEXT_FIELD, NO_KEY_HANDLER,       FOCUS_TRAVERSABLE,      ENABLED);
      
        Pane mapPane = new Pane();

        // AND THIS WILL BE USED TO CLIP THE MAP SO WE CAN ZOOM
        BorderPane outerMapPane = new BorderPane();
        Rectangle clippingRectangle = new Rectangle();
        outerMapPane.setClip(clippingRectangle);        
        Pane clippedPane = new Pane();
        outerMapPane.setCenter(clippedPane);
        clippedPane.getChildren().add(mapPane);
        Rectangle ocean = new Rectangle();
        mapPane.getChildren().add(ocean);
        ocean.getStyleClass().add(CLASS_MV_MAP_OCEAN);
        app.getGUIModule().addGUINode(MV_MAP_PANE, mapPane);
        mapPane.minWidthProperty().bind(outerMapPane.widthProperty());
        mapPane.maxWidthProperty().bind(outerMapPane.widthProperty());
        mapPane.minHeightProperty().bind(outerMapPane.heightProperty());
        mapPane.maxHeightProperty().bind(outerMapPane.heightProperty());
        outerMapPane.layoutBoundsProperty().addListener((ov, oldValue, newValue) -> {
            clippingRectangle.setWidth(newValue.getWidth());
            clippingRectangle.setHeight(newValue.getHeight());
            ocean.setWidth(newValue.getHeight()*2);
            ocean.setHeight(newValue.getHeight());
        });
        
        // THIS HAS THE ITEMS PANE COMPONENTS
        VBox itemlistpane = tdlBuilder.buildVBox(TDLM_ITEMS_PANE,                 toDoListPane,       null,   CLASS_TDLM_BOX, HAS_KEY_HANDLER,     FOCUS_TRAVERSABLE,  ENABLED);
        HBox titlepane = tdlBuilder.buildHBox(TDLM_ITEMS_PANE,                 itemlistpane,       null,   CLASS_TDLM_BOX, HAS_KEY_HANDLER,     FOCUS_TRAVERSABLE,  ENABLED);
        Label title = tdlBuilder.buildLabel(TDLM_TITLE_LABEL,         titlepane,       null,   CLASS_TDLM_TITLE_PROMPT, HAS_KEY_HANDLER,           FOCUS_TRAVERSABLE,      ENABLED);
        title.setAlignment(Pos.CENTER);
        Button up = tdlBuilder.buildIconButton(TDLM_UP_BUTTON,      titlepane,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);
                Button down = tdlBuilder.buildIconButton(TDLM_DOWN_BUTTON,      titlepane,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);

        VBox itemsPane              = tdlBuilder.buildVBox(TDLM_ITEMS_PANE,                 itemlistpane,       null,   CLASS_TDLM_BOX, HAS_KEY_HANDLER,     FOCUS_TRAVERSABLE,  ENABLED);
//        HBox itemButtonsPane        = tdlBuilder.buildHBox(TDLM_ITEM_BUTTONS_PANE,          itemsPane,          null,   CLASS_TDLM_BOX, HAS_KEY_HANDLER,     FOCUS_TRAVERSABLE,  ENABLED);
        Button addItemButton        = tdlBuilder.buildIconButton(TDLM_ADD_ITEM_BUTTON,      null,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  ENABLED);
        Button removeItemButton     = tdlBuilder.buildIconButton(TDLM_REMOVE_ITEM_BUTTON,   null,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  DISABLED);
        Button editItemButton       = tdlBuilder.buildIconButton(TDLM_EDIT_ITEM_BUTTON,     null,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  DISABLED);
        Button moveUpItemButton     = tdlBuilder.buildIconButton(TDLM_MOVE_UP_BUTTON,      null,    null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  DISABLED);
        Button moveDownItemButton   = tdlBuilder.buildIconButton(TDLM_MOVE_DOWN_BUTTON,     null,   null,   CLASS_TDLM_BUTTON, HAS_KEY_HANDLER,   FOCUS_TRAVERSABLE,  DISABLED);
             
        GridPane gp = tdlBuilder.buildGridPane(CLASS_TDLM_FILE_TOOLBAR, itemlistpane, null, CLASS_TDLM_FILE_TOOLBAR, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
                gp.setHgap(1);
        gp.setVgap(1);
        gp.add(new Label("Background Gradient"), 0, 0);
        gp.add(new Label("Proportional"), 2, 0);
        gp.add(new CheckBox(), 3, 0);
        gp.add(new Label("Focus Angle"), -0, 1);
         gp.add(new Slider(), 1, 1);
          gp.add(new Label("Focus Distance"), 2, 1);
        gp.add(new Slider(), 3, 1);
         gp.add(new Label("Center X"), 0, 2);
         gp.add(new Slider(), 1, 2);
          gp.add(new Label("Center Y"), 2, 2);
        gp.add(new Slider(), 3, 2);
        gp.add(new Label("Radius"), 0, 3);
         gp.add(new Slider(), 1, 3);
         gp.add(new Label("Cycle Method"), 2, 3);
         gp.add(new ComboBox(), 3,3);
          gp.add(new Label("Step 0 Color"), 0, 4);
         gp.add(new ColorPicker(), 1, 4);
         gp.add(new Label("Step 1 Color"), 2, 4);
         gp.add(new ColorPicker(), 3,4);
        
        // AND NOW THE TABLE
        TableView<RegioVincoItemPrototype> itemsTable  = tdlBuilder.buildTableView(TDLM_ITEMS_TABLE_VIEW,       itemsPane,          null,   CLASS_TDLM_TABLE, HAS_KEY_HANDLER,    FOCUS_TRAVERSABLE,  true);
        TableColumn subregionColumn      = tdlBuilder.buildTableColumn(  TDLM_CATEGORY_COLUMN,    itemsTable,         CLASS_TDLM_COLUMN);
        TableColumn capitalColumn   = tdlBuilder.buildTableColumn(  TDLM_DESCRIPTION_COLUMN, itemsTable,         CLASS_TDLM_COLUMN);
        TableColumn leaderColumn     = tdlBuilder.buildTableColumn(  TDLM_LEADER_COLUMN,  itemsTable,         CLASS_TDLM_COLUMN);
//        TableColumn endDateColumn       = tdlBuilder.buildTableColumn(  TDLM_END_DATE_COLUMN,    itemsTable,         CLASS_TDLM_COLUMN);
//        TableColumn assignedToColumn    = tdlBuilder.buildTableColumn(  TDLM_ASSIGNED_TO_COLUMN, itemsTable,         CLASS_TDLM_COLUMN);
//        TableColumn completedColumn     = tdlBuilder.buildTableColumn(  TDLM_COMPLETED_COLUMN,   itemsTable,         CLASS_TDLM_COLUMN);

        // SPECIFY THE TYPES FOR THE COLUMNS
        subregionColumn.setCellValueFactory(     new PropertyValueFactory<String,    String>("Subregion"));
        capitalColumn.setCellValueFactory(  new PropertyValueFactory<String,    String>("Capital"));
        leaderColumn.setCellValueFactory(    new PropertyValueFactory<String, String>("Leader"));
//        endDateColumn.setCellValueFactory(      new PropertyValueFactory<LocalDate, String>("endDate"));
//        assignedToColumn.setCellValueFactory(   new PropertyValueFactory<String,    String>("assignedTo"));
//        completedColumn.setCellValueFactory(    new PropertyValueFactory<Boolean,   String>("completed"));
        SplitPane sp = new SplitPane();
         sp.getItems().addAll(outerMapPane, itemlistpane);
sp.setDividerPositions(0.3f, 1.5f);
	// AND PUT EVERYTHING IN THE WORKSPACE
	workspace = new BorderPane();
	((BorderPane)workspace).setTop(firstPane);
        ((BorderPane)workspace).setBottom(sp);
 

        // AND NOW SETUP ALL THE EVENT HANDLING CONTROLLERS
        nameTextField.textProperty().addListener(e->{
            app.getFileModule().markAsEdited(true);
        });
        ownerTextField.textProperty().addListener(e->{
            app.getFileModule().markAsEdited(true);
        });
        ItemsController itemsController = new ItemsController((RegioVincoListMakerApp)app);
        addItemButton.setOnAction(e->{
            itemsController.processAddItem();
        });
        loadButton.setOnAction(e->{
            AppDialogsFacade.showNewDialog(app);
        });
        resizeButton.setOnAction(e->{
            itemsController.processchange();
        });
        randomButton.setOnAction(e->{
            try {
                itemsController.processviewedit();
            } catch (IOException ex) {
                Logger.getLogger(RegioVincoWorkspace.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        removeItemButton.setOnAction(e->{
            itemsController.processRemoveItems();
        });
        editItemButton.setOnAction(e->{
            itemsController.processEditItem();
        });
        moveUpItemButton.setOnAction(e->{
            itemsController.processMoveItemUp();
        });
        moveDownItemButton.setOnAction(e->{
            itemsController.processMoveItemDown();
        });
        itemsTable.setOnMouseClicked(e -> {
            app.getFoolproofModule().updateAll();
            if (e.getClickCount() == 2) {
                itemsController.processEditItem();
            }
        });
        ItemsTableController iTC = new ItemsTableController(app);
        itemsTable.widthProperty().addListener(e->{
            iTC.processChangeTableSize();
        });
        itemsTable.setOnSort(new EventHandler<SortEvent<TableView<RegioVincoItemPrototype>>>(){
            @Override
            public void handle(SortEvent<TableView<RegioVincoItemPrototype>> event) {
                RegioVincoData data = (RegioVincoData)app.getDataComponent();
                ArrayList<RegioVincoItemPrototype> oldListOrder = data.getCurrentItemsOrder();
                TableView view = event.getSource();
                ObservableList sortOrder = view.getSortOrder();
                if ((sortOrder != null) && (sortOrder.size() == 1)) {
                    TableColumn sortColumn = event.getSource().getSortOrder().get(0);
                    String columnText = sortColumn.getText();
                    SortType sortType = sortColumn.getSortType();
                    System.out.println("Sort by " + columnText);
                    event.consume();
                    SortItems_Transaction transaction = new SortItems_Transaction(data, oldListOrder, columnText, sortType);
                    app.processTransaction(transaction);
                    app.getFoolproofModule().updateAll();
                }
            }            
        });
    }
    
    public void initFoolproofDesign() {
        AppGUIModule gui = app.getGUIModule();
        AppFoolproofModule foolproofSettings = app.getFoolproofModule();
        foolproofSettings.registerModeSettings(TDLM_FOOLPROOF_SETTINGS, 
                new RegioVincoSelectionFoolproofDesign((RegioVincoListMakerApp)app));
    }

    @Override
    public void processWorkspaceKeyEvent(KeyEvent ke) {
       // WE AREN'T USING THIS FOR THIS APPLICATION
    }

    @Override
    public void showNewDialog() {
        // WE AREN'T USING THIS FOR THIS APPLICATION
    }
}