package RV.workspace.controllers;

import java.io.IOException;
import java.util.ArrayList;
import RV.RegioVincoListMakerApp;
import RV.data.RegioVincoData;
import RV.data.RegioVincoItemPrototype;
import RV.workspace.dialogs.RegioVincoListItemDialog;
import RV.transactions.AddItem_Transaction;
import RV.transactions.EditItem_Transaction;
import RV.transactions.MoveItem_Transaction;
import RV.transactions.RemoveItems_Transaction;
import RV.workspace.dialogs.ChangeDimensionDialog;
import RV.workspace.dialogs.View_EditSubreionDialog;

/**
 *
 * @author McKillaGorilla
 */
public class ItemsController {
    RegioVincoListMakerApp app;
    RegioVincoListItemDialog itemDialog;
    ChangeDimensionDialog changedialog;
    View_EditSubreionDialog vieweditdialog;
    public ItemsController(RegioVincoListMakerApp initApp) {
        app = initApp;
        
        itemDialog = new RegioVincoListItemDialog(app);
        changedialog = new ChangeDimensionDialog(app);
        vieweditdialog = new View_EditSubreionDialog(app);
    }
    
    public void processAddItem() {
        itemDialog.showAddDialog();
        RegioVincoItemPrototype newItem = itemDialog.getNewItem();        
        if (newItem != null) {
            // IF IT HAS A UNIQUE NAME AND COLOR
            // THEN CREATE A TRANSACTION FOR IT
            // AND ADD IT
            RegioVincoData data = (RegioVincoData)app.getDataComponent();
            AddItem_Transaction transaction = new AddItem_Transaction(data, newItem);
            app.processTransaction(transaction);
        }    
        // OTHERWISE TELL THE USER WHAT THEY
        // HAVE DONE WRONG
        else {
            djf.ui.dialogs.AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), "Invalid Line", "Invalid data for a new line");
        }
    }
    
    public void processRemoveItems() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        if (data.isItemSelected() || data.areItemsSelected()) {
            ArrayList<RegioVincoItemPrototype> itemsToRemove = new ArrayList(data.getSelectedItems());
            RemoveItems_Transaction transaction = new RemoveItems_Transaction(app, itemsToRemove);
            app.processTransaction(transaction);
        }
    }

    public void processEditItem() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        if (data.isItemSelected()) {
            RegioVincoItemPrototype itemToEdit = data.getSelectedItem();
            itemDialog.showEditDialog(itemToEdit);
            RegioVincoItemPrototype editItem = itemDialog.getEditItem();
            if (editItem != null) {
                EditItem_Transaction transaction = new EditItem_Transaction(itemToEdit, editItem.getCategory(), editItem.getDescription(), editItem.getStartDate(), editItem.getEndDate(), editItem.isCompleted());
                app.processTransaction(transaction);
            }
        }
    }
    
    public void processMoveItemUp() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        if (data.isItemSelected()) {
            RegioVincoItemPrototype itemToMove = data.getSelectedItem();
            int oldIndex = data.getItemIndex(itemToMove);
            if (oldIndex > 0) {
                MoveItem_Transaction transaction = new MoveItem_Transaction(data, oldIndex, oldIndex-1);
                app.processTransaction(transaction);
                
                // DESELECT THE OLD INDEX
                data.clearSelected();
                
                // AND SELECT THE MOVED ONE
                data.selectItem(itemToMove);
                
                // UPDATE BUTTONS
                app.getFoolproofModule().updateAll();
            }
        }
    }
    
    public void processMoveItemDown() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        if (data.isItemSelected()) {
            RegioVincoItemPrototype itemToMove = data.getSelectedItem();
            int oldIndex = data.getItemIndex(itemToMove);
            if (oldIndex < (data.getNumItems()-1)) {
                MoveItem_Transaction transaction = new MoveItem_Transaction(data, oldIndex, oldIndex+1);
                app.processTransaction(transaction);
                
                // DESELECT THE OLD INDEX
                data.clearSelected();
                
                // AND SELECT THE MOVED ONE
                data.selectItem(itemToMove);
                
                // UPDATE BUTTONS
                app.getFoolproofModule().updateAll();
            }
        }
    }
    
    public void processchange() {
        changedialog.showDimensionChangeDialog();
    }
    
    public void processviewedit() throws IOException {
        vieweditdialog.showViewEditDialog();
    }
}
