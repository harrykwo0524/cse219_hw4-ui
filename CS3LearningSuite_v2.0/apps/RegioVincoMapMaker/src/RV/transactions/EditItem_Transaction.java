package RV.transactions;

import java.time.LocalDate;
import jtps.jTPS_Transaction;
import RV.data.RegioVincoItemPrototype;

/**
 *
 * @author McKillaGorilla
 */
public class EditItem_Transaction implements jTPS_Transaction {
    RegioVincoItemPrototype itemToEdit;
    String oldCategory, newCategory;
    String oldDescription, newDescription;
    LocalDate oldStartDate, newStartDate;
    LocalDate oldEndDate, newEndDate;
    boolean oldCompleted, newCompleted;
    
    public EditItem_Transaction(    RegioVincoItemPrototype initItemToEdit, 
                                    String category, 
                                    String description, 
                                    LocalDate startDate, 
                                    LocalDate endDate, 
                                    boolean completed) {
        itemToEdit = initItemToEdit;
        oldCategory = itemToEdit.getCategory();
        oldDescription = itemToEdit.getDescription();
        oldStartDate = itemToEdit.getStartDate();
        oldEndDate = itemToEdit.getEndDate();
        oldCompleted = itemToEdit.isCompleted();
        newCategory = category;
        newDescription = description;
        newStartDate = startDate;
        newEndDate = endDate;
        newCompleted = completed;
    }

    @Override
    public void doTransaction() {
        itemToEdit.setCategory(newCategory);
        itemToEdit.setDescription(newDescription);
        itemToEdit.setStartDate(newStartDate);
        itemToEdit.setEndDate(newEndDate);
        itemToEdit.setCompleted(newCompleted);
    }

    @Override
    public void undoTransaction() {
        itemToEdit.setCategory(oldCategory);
        itemToEdit.setDescription(oldDescription);
        itemToEdit.setStartDate(oldStartDate);
        itemToEdit.setEndDate(oldEndDate);
        itemToEdit.setCompleted(oldCompleted);
    }
}