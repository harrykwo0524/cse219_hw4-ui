package djf.ui.dialogs;

import static djf.AppPropertyType.APP_BANNER;
import static djf.AppPropertyType.APP_LOGO;
import static djf.AppPropertyType.APP_PATH_IMAGES;
import static djf.AppPropertyType.LOAD_ERROR_CONTENT;
import static djf.AppPropertyType.LOAD_ERROR_TITLE;
import static djf.AppPropertyType.NEW_SUCCESS_CONTENT;
import static djf.AppPropertyType.NEW_SUCCESS_TITLE;
import static djf.AppPropertyType.WELCOME_DIALOG_NEW_BUTTON_TEXT;
import static djf.AppPropertyType.WELCOME_DIALOG_NONE_LABEL;
import static djf.AppPropertyType.WELCOME_DIALOG_RECENT_WORK_LABEL;
import static djf.AppPropertyType.WELCOME_DIALOG_TITLE;
import djf.AppTemplate;
import djf.modules.AppRecentWorkModule;
import static djf.modules.AppLanguageModule.FILE_PROTOCOL;
import djf.ui.controllers.AppFileController;
import static djf.ui.style.DJFStyle.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import properties_manager.PropertiesManager;

/**
 *
 * @author McKillaGorilla
 */
public class AppNewDialog extends Stage {

    // LEFT PANE
    VBox recentlyEditedPane;
    Label recentWorkLabel;
    ArrayList<Button> recentWorkButtons;
    
    // RIGHT PANE
    VBox splashPane;
//    ImageView welcomeDialogImageView;
    HBox newPane;
    Button createNewButton;
    
    String selectedPath = null;
    String selectedWorkName = null;   
     
     
    public AppNewDialog(AppTemplate app) {
        // GET THE RECENT WORK
        AppRecentWorkModule recentWork = app.getRecentWorkModule();
                
        // WE'LL NEED THIS
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        splashPane = new VBox();
        
        HBox titlepane = new HBox();
        Label titletext = new Label("Create New Map");
        titlepane.getChildren().add(titletext);
        
        HBox regionpane = new HBox();
        Label region = new Label("Region Name");
        TextField regiontextfield = new TextField();
        regionpane.getChildren().add(region);
             regionpane.getChildren().add(regiontextfield);
     
        HBox directorypane = new HBox();
        Label directorylabel = new Label("Parent Region");
         String directorytextfield = new String("Choose Parent Region Directory");
         Button directoryButton = new Button(directorytextfield);
         directorypane.getChildren().add(directorylabel);
           directorypane.getChildren().add(directoryButton);
           
          HBox datapane = new HBox();
        Label datalabel = new Label("Data File");
         String datatextfield = new String("Choose Data File");
         Button dataButton = new Button(datatextfield);
         datapane.getChildren().add(datalabel);
           datapane.getChildren().add(dataButton);
           
           String ok = new String("Ok");
           Button okbutton = new Button(ok);
           
        
        newPane = new HBox();
        newPane.setAlignment(Pos.CENTER);
//        newPane.getChildren().add(region);
//         newPane.getChildren().add(new TextField());
        splashPane.getChildren().add(titlepane);
        splashPane.getChildren().add(regionpane);
        splashPane.getChildren().add(directorypane);
        splashPane.getChildren().add(datapane);
        splashPane.getChildren().add(newPane);
okbutton.setOnAction(e->{
               this.hide();
                 app.getGUIModule().getWindow().show();
            app.getGUIModule().getFileController().processNewRequest();
           });
       dataButton.setOnAction(e->{
            this.hide();
           AppFileController filecontroller = new AppFileController(app);
           filecontroller.processLoadRequest();
           
       });
        
        // WE ORGANIZE EVERYTHING IN HERE
        BorderPane dialogPane = new BorderPane();
        dialogPane.setLeft(recentlyEditedPane);
        dialogPane.setCenter(splashPane);

        // MAKE AND SET THE SCENE
        Scene dialogScene = new Scene(dialogPane);
        this.setScene(dialogScene);

        // PUT EVERYTHING IN THE DIALOG WINDOW
        String dialogTitle = props.getProperty(WELCOME_DIALOG_TITLE);
        this.setTitle(dialogTitle);
        
        // SET THE APP ICON
        String imagesPath = props.getProperty(APP_PATH_IMAGES);
        String appLogo = FILE_PROTOCOL + imagesPath + props.getProperty(APP_LOGO);
        Image appWindowLogo = new Image(appLogo);
        this.getIcons().add(appWindowLogo);

        // SPECIFY THE STYLE THE BANNER AND NEW BUTTON
        app.getGUIModule().initStylesheet(this);
       titlepane.getStyleClass().add(CLASS_DJF_WELCOME_HEADER);
       regionpane.getStyleClass().add(CLASS_DJF_WELCOME_NEW_PANE);
       directorypane.getStyleClass().add(CLASS_DJF_WELCOME_NEW_PANE);
       datapane.getStyleClass().add(CLASS_DJF_WELCOME_NEW_PANE);
        newPane.getStyleClass().add(CLASS_DJF_WELCOME_NEW_PANE);
       okbutton.getStyleClass().add(CLASS_DJF_WELCOME_NEW_BUTTON);
    }
}